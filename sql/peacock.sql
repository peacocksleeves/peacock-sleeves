------------
-- ORDERS
------------
CREATE Table peacock.orders (
  id int(10) unsigned NOT NULL PRIMARY KEY AUTO_INCREMENT,
  prefix varchar(5) NOT NULL DEFAULT 'PEAC_',
  receipt VARCHAR(66) NOT NULL UNIQUE,
  amount VARCHAR(10) NOT NULL,
  email VARCHAR(66) NOT NULL,
  name VARCHAR(66) NOT NULL,
  address VARCHAR(129) NOT NULL,
  city VARCHAR(66) NOT NULL,
  state VARCHAR(2) NOT NULL,
  zip VARCHAR(11) NOT NULL,
  status VARCHAR(22) DEFAULT 'CONFIRMED' NOT NULL,
  cart TEXT NOT NULL,
  note VARCHAR(350),
  createdAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY (`prefix`, `id`)
);

CREATE TRIGGER peacock.update_sales
AFTER INSERT
ON peacock.orders
FOR EACH ROW
  BEGIN
    DECLARE cart_items BLOB;
    DECLARE cart_items_quantity BLOB;
    DECLARE cart_items_size BLOB;
    DECLARE cart_items_length BLOB;
    DECLARE v_index INT(3) DEFAULT 0;
    DECLARE v_item BLOB DEFAULT NULL;
    DECLARE v_quantity INT(3) DEFAULT 0;
    DECLARE v_size BLOB DEFAULT NULL;
    DECLARE v_length BLOB DEFAULT NULL;
    SET cart_items = (SELECT JSON_EXTRACT(NEW.cart, '$[*].name'));
    SET cart_items_quantity = (SELECT JSON_EXTRACT(NEW.cart, '$[*].quantity'));
    SET cart_items_size = (SELECT JSON_EXTRACT(NEW.cart, '$[*].size'));
    SET cart_items_length = (SELECT JSON_EXTRACT(NEW.cart, '$[*].length'));
    WHILE v_index < JSON_LENGTH(cart_items) DO
      SET v_item := JSON_EXTRACT(cart_items, CONCAT('$[', v_index, ']'));
      SET v_quantity := JSON_EXTRACT(cart_items_quantity, CONCAT('$[', v_index, ']'));
      SET v_size := JSON_EXTRACT(cart_items_size, CONCAT('$[', v_index, ']'));
      SET v_length := JSON_EXTRACT(cart_items_length, CONCAT('$[', v_index, ']'));
      UPDATE peacock.products SET sales = sales + v_quantity WHERE NAME = TRIM(BOTH '"' FROM v_item);
      UPDATE peacock.inventory SET variations = JSON_SET(variations, CONCAT('$.', v_size), (SELECT JSON_EXTRACT(variations, CONCAT('$.', v_size)) FROM inventory WHERE PRODUCT LIKE CONCAT('%', TRIM(BOTH '"' FROM v_length), '%')) - v_quantity) WHERE PRODUCT LIKE CONCAT('%', TRIM(BOTH '"' FROM v_length), '%');
      SET v_index := v_index + 1;
    END WHILE;
  END

------------
-- PRODUCTS
------------
CREATE TABLE peacock.products (
  id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  imgName VARCHAR(100) NOT NULL,
  name VARCHAR(100) NOT NULL,
  category VARCHAR(66) NULL,
  price TEXT DEFAULT '{"tuxedo":"85.00","long":"70.00","short":"55.00"}' NOT NULL,
  sales INT(8) DEFAULT 0 NOT NULL,
  status VARCHAR(22) DEFAULT 'ACTIVE' NOT NULL,
  createdAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

------------
-- INVENTORY
------------
CREATE TABLE peacock.inventory (
  id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  product VARCHAR(25) NOT NULL UNIQUE,
  variations TEXT NOT NULL
);

------------
-- GROUPS
------------
CREATE TABLE peacock.groups (
  id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  email VARCHAR(66) NOT NULL UNIQUE,
  passcode VARCHAR(66) NOT NULL,
  owner VARCHAR(66) NOT NULL,
  type VARCHAR(66) NOT NULL,
  cart TEXT NOT NULL,
  price VARCHAR(10) NOT NULL,
  createdAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

------------
-- MEMOS
------------
CREATE TABLE peacock.memos (
  id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  memo VARCHAR(250) NOT NULL,
  status VARCHAR(22) DEFAULT 'ACTIVE' NOT NULL,
  createdAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

------------
-- STATS
------------
CREATE TABLE peacock.stats (
  id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  sales INT(8) DEFAULT 0 NOT NULL,
  gross INT(18) NOT NULL,
  states TEXT NOT NULL,
  sizes TEXT NOT NULL,
  lengths TEXT NOT NULL,
  createdAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);


------------
-- EXTRA
------------
-- CREATE PROCEDURE peacock.debug_msg(enabled INTEGER, msg VARCHAR(255))
-- BEGIN
--   IF enabled THEN
--     select concat('** ', msg) AS '** DEBUG:';
--   END IF;
-- END;