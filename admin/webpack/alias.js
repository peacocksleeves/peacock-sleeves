import * as path from 'path';

const root = path.resolve(__dirname, '../');

const alias = {
  './@assets': path.resolve(root, 'src/assets'),
  './@styles': path.resolve(root, 'src/global/sass'),
  './@util': path.resolve(root, 'src/global/util'),
  './@redux': path.resolve(root, 'src/global/redux'),
  '@assets': path.resolve(root, 'src/assets'),
  '@components': path.resolve(root, 'src/components'),
  '@routes': path.resolve(root, 'src/routes'),
  '@styles': path.resolve(root, 'src/global/sass'),
  '@util': path.resolve(root, 'src/global/util'),
  '@redux': path.resolve(root, 'src/global/redux'),
};

export default alias;
