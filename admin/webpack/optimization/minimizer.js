import TerserPlugin from 'terser-webpack-plugin';

const minimizer = [
  new TerserPlugin({
    cache: true,
    parallel: true,
  }),
];

export default minimizer;
