import HtmlWebpackPlugin from 'html-webpack-plugin';
import path from 'path';

import fonts from './rules/fonts';
import images from './rules/images';
import svg from './rules/svg';
import jsx from './rules/jsx';

import alias from './alias';

const config = {};

// Entry
config.entry = ['./src/index.jsx'];

// Output
config.output = {
  chunkFilename: '[name]_[hash].bundle.js',
  filename: 'main_[hash].js',
  path: path.resolve(__dirname, '../dist'),
};

// Rules
config.module = {rules: []};
config.module.rules.push(jsx);
config.module.rules.push(svg);
config.module.rules.push(images);
config.module.rules.push(fonts);

config.resolve = {
  alias,
  extensions: ['.js', '.jsx', '.scss'],
};

config.plugins = [
  new HtmlWebpackPlugin({
    template: 'src/index.html',
  }),
];

export default config;
