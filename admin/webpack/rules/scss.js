import autoprefixer from 'autoprefixer';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import * as path from 'path';

const scssCommon = [
  {
    loader: 'css-loader',
    options: {
      importLoaders: 2,
      localIdentName: '[local]__[hash:base64:5]',
      modules: true,
      sourceMap: process.env.NODE_ENV !== 'production',
    },
  },
  {
    loader: 'postcss-loader',
    options: {
      plugins: [autoprefixer({grid: 'no-autoplace'})],
    },
  },
  {
    loader: 'sass-loader',
    options: {
      prependData: '@import "global";',
      sassOptions: {
        includePaths: [path.resolve(__dirname, '../../src/global/sass/')],
      },
    },
  },
];

export const scssDev = {
  test: /\.scss$/,
  use: [{loader: 'style-loader'}, ...scssCommon],
};

export const scssProd = {
  include: path.resolve(__dirname, '../../src/'),
  test: /\.scss$/,
  use: [MiniCssExtractPlugin.loader, ...scssCommon],
};