const svg = {
  exclude: /node_modules/,
  test: /\.svg$/,
  use: [
    'babel-loader',
    {
      loader: 'react-svg-loader'
    }
  ]
};

export default svg;