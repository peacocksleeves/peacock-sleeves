import CleanWebpackPlugin from 'clean-webpack-plugin';
import CopyPlugin from 'copy-webpack-plugin';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';

import {moduleIds, runtimeChunk, splitChunks} from './optimization/cache';
import minimizer from './optimization/minimizer';
import common from './webpack.common';
import {scssProd} from './rules/scss';

const config = {...common};

// Prod specific loaders
config.module.rules.push(scssProd);

config.plugins.push(new CleanWebpackPlugin());

config.plugins.push(
  new MiniCssExtractPlugin({
    chunkFilename: '[id]_[chunkhash].css',
    filename: `[name]_[chunkhash].css`,
  })
);

config.plugins.push(
  new CopyPlugin({
    patterns: [
      {
        from: './src/robots.txt',
        to: './'
      }
    ]
  })
);

config.optimization = {
  minimizer,
  moduleIds,
  runtimeChunk,
  splitChunks,
};

// Output
config.output.publicPath = `/`;

export default config;
