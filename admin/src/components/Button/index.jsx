import React from 'react';
import {Link} from 'react-router-dom';

import Loading from '@components/Loading';

import styles from './index.scss';

const Button = props => (
  <>
    {props.to ? (
      <Link
        to={props.to}
        className={`${props.className} ${styles.button} ${
          props.type === 'primary' ? styles.primaryButton : ''
        } ${props.type === 'shop' ? styles.shopButton : ''}`}>
        {props.text}
      </Link>
    ) : props.submit ? (
      (props.loading && (
        <div
          className={`${props.className} ${styles.button} ${
            props.type === 'primary' ? styles.primaryButton : ''
          } ${props.type === 'shop' ? styles.shopButton : ''}`}>
          {(props.loading && <Loading />) || props.text}
        </div>
      )) || (
        <input
          type="submit"
          value={props.value}
          className={`${props.className} ${styles.button} ${
            props.type === 'primary' ? styles.primaryButton : ''
          } ${props.type === 'shop' ? styles.shopButton : ''}`}
        />
      )
    ) : (
      <div
        className={`${props.className} ${styles.button} ${
          props.type === 'primary' ? styles.primaryButton : ''
        } ${props.type === 'shop' ? styles.shopButton : ''}`}>
        {(props.loading && <Loading />) || props.text}
      </div>
    )}
  </>
);

export default Button;
