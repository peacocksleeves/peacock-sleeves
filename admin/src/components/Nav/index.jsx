import React, {useState} from 'react';
import {NavLink} from 'react-router-dom';

import logo from '@assets/icons/feather.png';

import styles from './index.scss';

const Nav = props => {
  const [showMobileLinks, setShowMobileLinks] = useState(false);

  const hamHandleClick = () => {
    setShowMobileLinks(!showMobileLinks);
  };

  const linkHandleClick = () => {
    setShowMobileLinks(false);
  };

  return (
    <>
      <nav
        className={styles.mainNav}
        style={props.location === 'landing' ? {} : {position: 'fixed', top: '0', zIndex: '1'}}>
        <NavLink to={'/'} className={styles.homeLink}>
          <img src={logo} className={styles.logo} alt="peacock sleeves logo" />
          <span>PEACOCK</span>
          <span>Sleeves</span>
        </NavLink>
        <span
          className={`${styles.hamburger} ${showMobileLinks ? styles.clickedHam : ''}`}
          onClick={hamHandleClick}></span>
        <div className={styles.navMenu}>
          <NavLink
            className={`${styles.linkItems} ${showMobileLinks ? styles.mobileShowLinks : ''}`}
            onClick={linkHandleClick}
            to={'/orders'}>
            Orders
          </NavLink>
          <NavLink
            className={`${styles.linkItems} ${showMobileLinks ? styles.mobileShowLinks : ''}`}
            onClick={linkHandleClick}
            to={'/products'}>
            Products
          </NavLink>
          <NavLink
            className={`${styles.linkItems} ${showMobileLinks ? styles.mobileShowLinks : ''}`}
            onClick={linkHandleClick}
            to={'/memos'}>
            Memos
          </NavLink>
          <NavLink
            className={`${styles.linkItems} ${showMobileLinks ? styles.mobileShowLinks : ''}`}
            onClick={linkHandleClick}
            to={'/coupons'}>
            Coupons
          </NavLink>
          <NavLink
            className={`${styles.linkItems} ${showMobileLinks ? styles.mobileShowLinks : ''}`}
            onClick={linkHandleClick}
            to={'/inventory'}>
            Inventory
          </NavLink>
        </div>
      </nav>
    </>
  );
};

export default Nav;
