import apiRequest from '@util/apiRequest';

/////////////////////
// PRODUCTS
////////////////////
export const getProducts = async () => {
  try {
    const data = await apiRequest('GET', {headers: {}}, 'getProducts');
    return {products: data.data, loading: false};
  } catch (err) {
    return {loading: false};
  }
};

/////////////////////
// ORDERS
////////////////////
export const getOrders = async () => {
  try {
    const data = await apiRequest('GET', {headers: {}}, 'getOrders');
    const orders = data.data
      .filter(order => order.status !== 'COMPLETE')
      .sort((a, b) => (a.createdAt < b.createdAt ? 1 : -1));
    return {orders, loading: false};
  } catch (err) {
    return {loading: false};
  }
};

/////////////////////
// MEMOS
////////////////////
export const getMemos = async () => {
  try {
    const data = await apiRequest('GET', {headers: {}}, 'getMemos');
    return {memos: data.data, loading: false};
  } catch (err) {
    return {loading: false};
  }
};

/////////////////////
// COUPONS
////////////////////
export const getCoupons = async () => {
  try {
    const data = await apiRequest('GET', {headers: {}}, 'getCoupons');
    return {coupons: data.data.data, loading: false};
  } catch (err) {
    return {loading: false};
  }
};

/////////////////////
// INVENTORY
////////////////////
export const getInventory = async () => {
  try {
    const data = await apiRequest('GET', {headers: {}}, 'getInventory');
    const inventory = data.data.map(inv => ({...inv, variations: JSON.parse(inv.variations)}));
    return {inventory, loading: false};
  } catch (err) {
    return {loading: false};
  }
};

/////////////////////
// STATS
////////////////////
export const getStats = async () => {
  try {
    const data = await apiRequest('GET', {headers: {}}, 'getStats');
    const stats = data.data.map(stat => ({
      ...stat,
      lengths: JSON.parse(stat.lengths),
      sizes: JSON.parse(stat.sizes),
      states: JSON.parse(stat.states),
    }));
    return {stats, loading: false};
  } catch (err) {
    return {loading: false};
  }
};
