import createStore from 'redux-zero';

const initialState = {
  products: [],
  orders: [],
  memos: [],
  coupons: [],
  inventory: [],
  stats: [],
  loading: false,
};

const store = createStore(initialState);

export default store;
