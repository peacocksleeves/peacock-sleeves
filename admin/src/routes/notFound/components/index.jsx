import React from 'react';
import {Link} from 'react-router-dom';

const NotFound = () => (
  <>
    <h2 style={{textAlign: 'center', marginTop: '8em', fontSize: '30px'}}>Page Not Found!</h2>
    <Link style={{textAlign: 'center', display: 'block', fontSize: '24px', color: 'teal'}} to={'/'}>
      Return Home
    </Link>
  </>
);
export default NotFound;
