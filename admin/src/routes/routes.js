import React from 'react';

const Home = React.lazy(() => import(/* webpackChunkName: "home" */ '@routes/home/components'));
const NotFound = React.lazy(() => import(/* webpackChunkName: "notfound" */ '@routes/notFound/components'));
const Products = React.lazy(() => import(/* webpackChunkName: "products" */ '@routes/products/components'));
const Orders = React.lazy(() => import(/* webpackChunkName: "orders" */ '@routes/orders/components'));
const Memos = React.lazy(() => import(/* webpackChunkName: "memos" */ '@routes/memos/components'));
const Coupons = React.lazy(() => import(/* webpackChunkName: "coupons" */ '@routes/coupons/components'));
const Inventory = React.lazy(() =>
  import(/* webpackChunkName: "inventory" */ '@routes/inventory/components')
);

const routes = [
  {
    component: Home,
    exact: true,
    path: '/',
  },
  {
    component: Products,
    exact: true,
    path: '/products',
  },
  {
    component: Memos,
    exact: true,
    path: '/memos',
  },
  {
    component: Coupons,
    exact: true,
    path: '/coupons',
  },
  {
    component: Inventory,
    exact: true,
    path: '/inventory',
  },
  {
    component: Orders,
    exact: true,
    path: '/orders',
  },
  {
    component: Orders,
    exact: true,
    path: '/orders/:id?',
  },
  {
    component: NotFound,
    exact: false,
  },
];

export default routes;
