import React, {useEffect, useState} from 'react';
import {useAction, useSelector} from 'redux-zero/react';

import {getCoupons} from '@redux/actions';
import Button from '@components/Button';
import apiRequest from '@util/apiRequest';

import styles from './index.scss';

const tableHeaders = ['Created', 'Code', 'Discount', 'Status', 'Actions'];

const Coupons = () => {
  const [loading, setLoading] = useState(false);
  const [messageSuccess, setMessageSuccess] = useState(null);
  const [messageError, setMessageError] = useState(null);
  const [newCode, setNewCode] = useState('');
  const [codeCoupon, setCodeCoupon] = useState('');
  const [couponName, setCouponName] = useState('');
  const coupons = useSelector(({coupons}) => coupons);
  const fetchCoupons = useAction(getCoupons);

  useEffect(() => {
    fetchCoupons();
  }, []);

  const couponInput = e => {
    e.preventDefault();
    e.persist();
    setNewCode(e.target.value.toUpperCase());
  };

  const submitCoupon = async () => {
    if (newCode === '') {
      setMessageError('You need to type in an actual new code in the line above ya fish.');
      setTimeout(() => {
        setMessageError(null);
      }, 3500);
      return;
    }
    setLoading(true);
    try {
      await apiRequest('POST', {headers: {}, body: {coupon: couponName, code: newCode}}, 'createCoupon');
      setLoading(false);
      setNewCode('');
      setMessageSuccess('Fuck yaaass coupon added!');
      setTimeout(() => {
        setMessageSuccess(null);
      }, 3500);
      fetchCoupons();
    } catch (error) {
      console.log(error);
      setLoading(false);
      setMessageError("Shit didn't work man. Try again ya fish.");
      setTimeout(() => {
        setMessageError(null);
      }, 3500);
    }
  };

  const deactivateCoupon = async (id, status) => {
    try {
      await apiRequest('POST', {headers: {}, body: {status, id}}, 'updateCoupon');
      fetchCoupons();
    } catch (err) {
      console.log(err);
      setMessageError("Shit, somethings up and couldn't update.");
      setTimeout(() => {
        setMessageError(null);
      }, 2500);
    }
  };

  return (
    <div className={styles.coupons}>
      <h1>Coupons</h1>
      <div className={styles.couponForm}>
        <form onSubmit={submitCoupon}>
          <input
            onChange={couponInput}
            value={newCode}
            placeholder="New Code*"
            required
            pattern="^[a-zA-Z \.,'!-]+$"
          />
          <div className={styles.discountSelect}>
            <h3>Discount</h3>
            <select
              value={codeCoupon}
              onChange={e => {
                e.target.childNodes.forEach(child => {
                  if (child.id.includes(e.target.value)) setCouponName(child.id.split('-')[0]);
                });
                setCodeCoupon(e.target.value);
              }}>
              <option value={codeCoupon}>{codeCoupon}</option>
              {coupons
                .filter(
                  (obj, index, arr) => arr.map(mapObj => mapObj.coupon.id).indexOf(obj.coupon.id) === index
                )
                .map(coupon => (
                  <option
                    key={coupon.id}
                    id={
                      coupon.coupon.amount_off
                        ? `${coupon.coupon.id}-$${(coupon.coupon.amount_off / 100).toFixed(2)} off`
                        : coupon.coupon.percent_off
                        ? `${coupon.coupon.id}-${coupon.coupon.percent_off}% off`
                        : ''
                    }
                    value={
                      coupon.coupon.amount_off
                        ? `$${(coupon.coupon.amount_off / 100).toFixed(2)} off`
                        : coupon.coupon.percent_off
                        ? `${coupon.coupon.percent_off}% off`
                        : ''
                    }>
                    {coupon.coupon.amount_off
                      ? `$${(coupon.coupon.amount_off / 100).toFixed(2)} off`
                      : coupon.coupon.percent_off
                      ? `${coupon.coupon.percent_off}% off`
                      : ''}
                  </option>
                ))}
            </select>
          </div>
          <div onClick={loading ? e => e.preventDefault() : submitCoupon}>
            <Button text="Add New Code" type="primary" className={styles.addCouponButton} loading={loading} />
          </div>
        </form>
        {messageSuccess && <p className={styles.success}>{messageSuccess}</p>}
        {messageError && <p className={styles.error}>{messageError}</p>}
      </div>
      <table className={styles.table}>
        <thead>
          <tr>
            {tableHeaders.map((key, i) => {
              return <th key={i}>{key}</th>;
            })}
          </tr>
        </thead>
        <tbody>
          {coupons.map(coupon => {
            return (
              <tr key={coupon.id}>
                <td>{new Date(coupon.created).toDateString()}</td>
                <td>{coupon.code}</td>
                <td>
                  {coupon.coupon.amount_off
                    ? `$${(coupon.coupon.amount_off / 100).toFixed(2)} off`
                    : coupon.coupon.percent_off
                    ? `${coupon.coupon.percent_off}% off`
                    : ''}
                </td>
                <td>{coupon.active ? 'Active' : 'Inactive'}</td>
                <td>
                  <div
                    onClick={
                      loading ? e => e.preventDefault() : () => deactivateCoupon(coupon.id, !coupon.active)
                    }>
                    <Button
                      text={coupon.active ? 'Deactivate' : 'Activate'}
                      type="primary"
                      className={styles.actionButton}
                      loading={loading}
                    />
                  </div>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};

export default Coupons;
