import React, {useState} from 'react';

import Button from '@components/Button';
import apiRequest from '@util/apiRequest';

import styles from './addUpdateProduct.scss';

const AddUpdateProduct = props => {
  const [loading, setLoading] = useState(false);
  const [messageSuccess, setMessageSuccess] = useState(null);
  const [messageError, setMessageError] = useState(null);
  const [status, setStatus] = useState(props.update ? props.update.status : null);
  const [formData, setFormData] = useState({
    name: props.update ? props.update.name : '',
    category: props.update ? props.update.category : '',
    price: props.update ? JSON.parse(props.update.price) : '',
    img: null,
  });

  const inputChange = e => {
    e.preventDefault();
    e.persist();
    if (e.target.name === 'tuxedo' || e.target.name === 'long' || e.target.name === 'short')
      setFormData({...formData, price: {...formData.price, [e.target.name]: e.target.value}});
    else setFormData({...formData, [e.target.name]: e.target.value});
  };

  const fileUpload = e => {
    e.preventDefault();
    e.persist();
    setLoading(true);
    const file = e.target.files[0];
    if (file.type === 'image/jpeg') {
      let reader = new FileReader();
      reader.onloadend = function() {
        var image = new Image();
        image.src = this.result;
        image.addEventListener('load', function() {
          if (this.height > 500 && this.width > 500) {
            setFormData({...formData, img: reader.result});
            setLoading(false);
          } else setMessageError('Image has to be at least 500x500 in size. Try again.');
        });
      };
      reader.readAsDataURL(file);
    } else {
      setMessageError('Image has to be a .jpg file. Try again.');
    }
  };

  const submitForm = async e => {
    e.preventDefault();
    setLoading(true);
    const requestBody = {
      imgName: formData.name.replace(/[^A-Za-z0-9]/g, '').toLowerCase(),
      ...formData,
    };
    try {
      let method = 'POST';
      let endpoint = 'createProduct';
      if (props.update) {
        method = 'PUT';
        endpoint = `updateProduct/${props.update.id}`;
        requestBody.status = status;
        delete requestBody.img;
        if (requestBody.name === props.update.name) {
          delete requestBody.name;
          delete requestBody.imgName;
        }
      }
      await apiRequest(method, {headers: {}, body: requestBody}, endpoint);
      setLoading(false);
      setFormData({name: '', category: '', price: '', img: ''});
      setMessageSuccess(`Yaaass product ${props.update ? 'updated' : 'added'}!`);
      setTimeout(() => {
        setMessageSuccess(null);
        props.setShowModal(false);
        props.setUpdate(null);
        props.fetchProducts();
      }, 1500);
    } catch (error) {
      setLoading(false);
      setMessageError("Shit didn't work man. Try again.");
      setTimeout(() => {
        setMessageError(null);
      }, 1500);
    }
  };

  return (
    <div className={styles.modal}>
      <div className={styles.formContainer}>
        <div className={styles.form}>
          <>
            <h2 className={styles.formTitle}>Product to {!props.update ? 'Add' : 'Update'}</h2>
            <p style={{textAlign: 'center'}}>*Prices must include decimal point and cents*</p>
            <span
              className={styles.close}
              onClick={() => {
                props.setShowModal(false),
                  props.setUpdate(null),
                  setFormData({name: '', category: '', price: '', img: ''});
              }}>
              x
            </span>
          </>
          <form
            onSubmit={
              loading
                ? e => e.preventDefault()
                : !props.update && !formData.img
                ? e => {
                    e.preventDefault();
                    setMessageError('Upload an image dumb-dumb.');
                  }
                : submitForm
            }>
            {props.update && <label>Change Name:</label>}
            <input
              onChange={inputChange}
              value={formData.name}
              type="text"
              name="name"
              placeholder="Product Name*"
              required
              pattern="^[a-zA-Z0-9 \.,'-]+$"
            />
            {props.update && <label>Change Category:</label>}
            <input
              onChange={inputChange}
              value={formData.category}
              type="text"
              name="category"
              placeholder="Category*"
              required
              pattern="^[a-zA-Z ]+$"
            />
            {props.update && <label>Change Tuxedo Price:</label>}
            <input
              onChange={inputChange}
              value={formData.price.tuxedo}
              type="text"
              name="tuxedo"
              placeholder="Tuxedo Price*"
              required
              pattern="^[0-9]{2,3}\.[0-9]{2}$"
            />
            {props.update && <label>Change Long Price:</label>}
            <input
              onChange={inputChange}
              value={formData.price.long}
              type="text"
              name="long"
              placeholder="Long Price*"
              required
              pattern="^[0-9]{2,3}\.[0-9]{2}$"
            />
            {props.update && <label>Change Short Price:</label>}
            <input
              onChange={inputChange}
              value={formData.price.short}
              type="text"
              name="short"
              placeholder="Short Price*"
              required
              pattern="^[0-9]{2,3}\.[0-9]{2}$"
            />
            {props.update && (
              <>
                <label>Change Status:</label>
                <select name="status" value={status} onChange={e => setStatus(e.target.value)}>
                  <option value="ACTIVE">ACTIVE</option>
                  <option value="INACTIVE">INACTIVE</option>
                </select>
              </>
            )}
            {!props.update && <input onChange={fileUpload} type="file" name="file" id="file" required />}
            <div style={{display: 'grid'}}>
              <Button
                className={styles.button}
                value="Submit"
                type="primary"
                loading={loading}
                submit={true}
              />
            </div>
          </form>
          {messageSuccess && <p className={styles.success}>{messageSuccess}</p>}
          {messageError && <p className={styles.error}>{messageError}</p>}
        </div>
      </div>
    </div>
  );
};
export default AddUpdateProduct;
