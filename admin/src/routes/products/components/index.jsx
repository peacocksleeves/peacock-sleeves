import React, {useEffect, useState} from 'react';
import {useAction, useSelector} from 'redux-zero/react';
import {Grid, _} from 'gridjs-react';

import {getProducts} from '@redux/actions';
import Button from '@components/Button';
import AddUpdateProduct from './addUpdateProduct';

import styles from './index.scss';

const Products = () => {
  const products = useSelector(({products}) => products);
  const [showModal, setShowModal] = useState(false);
  const [update, setUpdate] = useState(null);
  const fetchProducts = useAction(getProducts);
  useEffect(() => {
    fetchProducts();
  }, []);

  const tableHeaders = [
    'Id',
    {
      name: 'Img Name',
      formatter: cell =>
        _(
          <img
            style={{height: '100px', padding: '5px 0'}}
            src={`https://peacock-swatches-${process.env.ENV}.s3.amazonaws.com/${cell}small.jpg`}
            alt="pattern swatch"
          />
        ),
    },
    'Name',
    'Category',
    {
      name: 'Price',
      formatter: cell => _(<p>{cell.replace(/({|}|")/g, '').replace(/,/g, ' | ')}</p>),
    },
    'Sales',
    'Status',
    {
      name: 'Actions',
      formatter: (_cell, row) =>
        _(
          <div
            onClick={() => {
              setUpdate({
                name: row.cells[2].data,
                category: row.cells[3].data,
                price: row.cells[4].data,
                status: row.cells[6].data,
                id: row.cells[0].data,
              });
              setShowModal(true);
            }}>
            <Button text="Update" type="primary" className={styles.actionButton} />
          </div>
        ),
    },
  ];

  return (
    <>
      <div className={styles.products}>
        <h1>Products</h1>
        <div onClick={() => setShowModal(true)}>
          <Button text="Add New Product" type="primary" className={styles.addProductButton} />
        </div>
        <Grid
          data={products}
          columns={tableHeaders}
          sort={true}
          // search={true}
          // pagination={{
          //   enabled: true,
          //   limit: 1,
          // }}
          className={{
            table: `${styles.table}`,
          }}
        />
      </div>
      {showModal && (
        <AddUpdateProduct
          setShowModal={setShowModal}
          update={update}
          setUpdate={setUpdate}
          fetchProducts={fetchProducts}
        />
      )}
    </>
  );
};

export default Products;
