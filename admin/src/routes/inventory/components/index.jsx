import React, {useEffect, useState} from 'react';
import {useAction, useSelector} from 'redux-zero/react';

import {getInventory} from '@redux/actions';
import Button from '@components/Button';
import AddUpdateInventory from './addUpdateInventory';

import styles from './index.scss';

const tableHeaders = ['ID', 'Products', 'Variations', 'Actions'];

const Inventory = () => {
  const inventory = useSelector(({inventory}) => inventory);
  const [showModal, setShowModal] = useState(false);
  const [update, setUpdate] = useState(null);
  const fetchInventory = useAction(getInventory);

  useEffect(() => {
    fetchInventory();
  }, []);

  return (
    <div className={styles.inventory}>
      <h1>Inventory</h1>
      <table className={styles.table}>
        <thead>
          <tr>
            {tableHeaders.map((key, i) => {
              return <th key={i}>{key}</th>;
            })}
          </tr>
        </thead>
        <tbody>
          {inventory.map(inv => {
            return (
              <tr key={inv.id}>
                <td>{inv.id}</td>
                <td>{inv.product}</td>
                <td>
                  <br />
                  {Object.keys(inv.variations).map(key => (
                    <p key={`${key}${inv.id}`}>{`${key}: ${inv.variations[key]}`}</p>
                  ))}
                  <br />
                </td>
                <td>
                  <div
                    onClick={() => {
                      setShowModal(true);
                      setUpdate({id: inv.id, variations: inv.variations});
                    }}>
                    <Button text="Update" type="primary" className={styles.actionButton} />
                  </div>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      {showModal && (
        <AddUpdateInventory
          setShowModal={setShowModal}
          update={update}
          setUpdate={setUpdate}
          fetchInventory={fetchInventory}
        />
      )}
    </div>
  );
};

export default Inventory;
