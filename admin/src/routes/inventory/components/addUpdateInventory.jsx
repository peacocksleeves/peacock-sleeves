import React, {useState} from 'react';

import Button from '@components/Button';
import apiRequest from '@util/apiRequest';

import styles from './addUpdateInventory.scss';

const AddUpdateInventory = props => {
  const [loading, setLoading] = useState(false);
  const [messageSuccess, setMessageSuccess] = useState(null);
  const [messageError, setMessageError] = useState(null);
  const [formData, setFormData] = useState(props.update.variations);

  const inputChange = e => {
    e.preventDefault();
    e.persist();
    setFormData({...formData, [e.target.name]: parseInt(e.target.value) || 0});
  };

  const submitForm = async e => {
    e.preventDefault();
    setLoading(true);
    try {
      await apiRequest(
        'PUT',
        {headers: {}, body: {variations: JSON.stringify(formData)}},
        `updateInventory/${props.update.id}`
      );
      setLoading(false);
      setFormData(props.update.variations);
      setMessageSuccess('Yaaass inventory updated');
      setTimeout(() => {
        setMessageSuccess(null);
        props.setShowModal(false);
        props.setUpdate(null);
        props.fetchInventory();
      }, 1500);
    } catch (error) {
      console.log(error);
      setLoading(false);
      setMessageError("Shit didn't work man. Try again.");
      setTimeout(() => {
        setMessageError(null);
      }, 1500);
    }
  };

  return (
    <div className={styles.modal}>
      <div className={styles.formContainer}>
        <div className={styles.form}>
          <>
            <h2 className={styles.formTitle}>Inventory to Update</h2>
            <p style={{textAlign: 'center'}}>*Inventory must be between 0-999*</p>
            <span
              className={styles.close}
              onClick={() => {
                props.setShowModal(false);
                props.setUpdate(null);
              }}>
              x
            </span>
          </>
          <form onSubmit={loading ? e => e.preventDefault() : submitForm}>
            {Object.keys(formData).map(variation => (
              <div key={variation}>
                <label>Change {variation} Quantity:</label>
                <input
                  onChange={inputChange}
                  value={formData[variation]}
                  type="text"
                  name={variation}
                  required
                  pattern="^[0-9]{1,3}"
                />
              </div>
            ))}
            <div style={{display: 'grid'}}>
              <Button
                className={styles.button}
                value="Submit"
                type="primary"
                loading={loading}
                submit={true}
              />
            </div>
          </form>
          {messageSuccess && <p className={styles.success}>{messageSuccess}</p>}
          {messageError && <p className={styles.error}>{messageError}</p>}
        </div>
      </div>
    </div>
  );
};
export default AddUpdateInventory;
