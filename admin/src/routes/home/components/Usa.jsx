import React from 'react';
import {ComposableMap, Geographies, Geography, Marker, Annotation} from 'react-simple-maps';
import {scaleQuantile} from 'd3-scale';
import {geoCentroid} from 'd3-geo';

import styles from './usa.scss';

const geoUrl = 'https://cdn.jsdelivr.net/npm/us-atlas@3/states-10m.json';

const Usa = props => {
  const colorScale = scaleQuantile()
    .domain(props.states.map(d => d.count))
    .range(['#3AFEB5', '#16FDA7', '#03ED96', '#03C87F', '#03A368', '#037F51', '#025B3A']);

  const offsets = {
    VT: [50, -8],
    NH: [34, 2],
    MA: [30, -1],
    RI: [28, 2],
    CT: [35, 10],
    NJ: [34, 1],
    DE: [33, 0],
    MD: [47, 10],
    DC: [49, 21],
  };

  return (
    <section className={styles.map}>
      <h2 className={styles.h2}>Sales by State - HeatMap</h2>
      <ComposableMap projection="geoAlbersUsa">
        <Geographies geography={geoUrl}>
          {({geographies}) => (
            <>
              {geographies.map(geo => {
                const cur = props.states.find(s => stateAbrv[s.state] === geo.properties.name);
                return (
                  <Geography
                    key={geo.rsmKey}
                    stroke="#FFF"
                    geography={geo}
                    fill={cur ? colorScale(cur.count) : '#EEE'}
                    onClick={() => console.log(cur.count)}
                  />
                );
              })}
              {geographies.map(geo => {
                const centroid = geoCentroid(geo);
                const cur = props.states.find(s => stateAbrv[s.state] === geo.properties.name);
                return (
                  <g key={geo.rsmKey + '-name'}>
                    {cur &&
                      centroid[0] > -160 &&
                      centroid[0] < -67 &&
                      (Object.keys(offsets).indexOf(cur.state) === -1 ? (
                        <Marker coordinates={centroid}>
                          <text y="2" fontSize="10px" textAnchor="middle">
                            {cur.state}: {cur.count}
                          </text>
                        </Marker>
                      ) : (
                        <Annotation subject={centroid} dx={offsets[cur.state][0]} dy={offsets[cur.state][1]}>
                          <text x={4} fontSize="10px" alignmentBaseline="middle">
                            {cur.state}: {cur.count}
                          </text>
                        </Annotation>
                      ))}
                  </g>
                );
              })}
            </>
          )}
        </Geographies>
      </ComposableMap>
    </section>
  );
};

export default Usa;

const stateAbrv = {
  AL: 'Alabama',
  AK: 'Alaska',
  AZ: 'Arizona',
  AR: 'Arkansas',
  CA: 'California',
  CO: 'Colorado',
  CT: 'Connecticut',
  DE: 'Delaware',
  FL: 'Florida',
  GA: 'Georgia',
  HI: 'Hawaii',
  ID: 'Idaho',
  IL: 'Illinois',
  IN: 'Indiana',
  IA: 'Iowa',
  KS: 'Kansas',
  KY: 'Kentucky',
  LA: 'Louisiana',
  ME: 'Maine',
  MD: 'Maryland',
  MA: 'Massachusetts',
  MI: 'Michigan',
  MN: 'Minnesota',
  MS: 'Mississippi',
  MO: 'Missouri',
  MT: 'Montana',
  NE: 'Nebraska',
  NV: 'Nevada',
  NH: 'New Hampshire',
  NJ: 'New Jersey',
  NM: 'New Mexico',
  NY: 'New York',
  NC: 'North Carolina',
  ND: 'North Dakota',
  OH: 'Ohio',
  OK: 'Oklahoma',
  OR: 'Oregon',
  PA: 'Pennsylvania',
  RI: 'Rhode Island',
  SC: 'South Carolina',
  SD: 'South Dakota',
  TN: 'Tennessee',
  TX: 'Texas',
  UT: 'Utah',
  VT: 'Vermont',
  VA: 'Virginia',
  WA: 'Washington',
  WV: 'West Virginia',
  WI: 'Wisconsin',
  WY: 'Wyoming',
};
