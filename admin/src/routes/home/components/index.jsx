import React, {useEffect} from 'react';
import {useAction, useSelector} from 'redux-zero/react';

import {getStats} from '@redux/actions';
import Button from '@components/Button';

import Stats from './Stats.jsx';

import styles from './index.scss';

const Home = () => {
  const stats = useSelector(({stats}) => stats);
  const fetchStats = useAction(getStats);

  useEffect(() => {
    fetchStats();
  }, []);

  return (
    <div className={styles.home}>
      <h1>Peacock Admin</h1>
      <div className={styles.buttons}>
        <Button text="Orders" type="primary" to="/orders" />
      </div>
      <div className={styles.buttons}>
        <Button text="Products" type="primary" to="/products" />
      </div>
      <div className={styles.buttons}>
        <Button text="Memos" type="primary" to="/memos" />
      </div>
      <div className={styles.buttons}>
        <Button text="Coupons" type="primary" to="/coupons" />
      </div>
      <div className={styles.buttons}>
        <Button text="Inventory" type="primary" to="/inventory" />
      </div>
      {stats.length > 0 && <Stats stats={stats} />}
    </div>
  );
};

export default Home;
