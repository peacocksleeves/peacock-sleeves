import React from 'react';

import Usa from './Usa.jsx';

import styles from './stats.scss';

const Stats = props => {
  return (
    <>
      <h1 style={{marginTop: '1.5em'}}>Stats</h1>
      <p style={{textAlign: 'center'}}>*Updated Daily*</p>
      <div className={styles.webStats}>
        <h2>Financial Stats</h2>
        <p>*Use your own credentials*</p>
        <a href="https://dashboard.stripe.com/dashboard" target="_blank">
          https://dashboard.stripe.com/dashboard
        </a>
      </div>
      <div className={styles.webStats}>
        <h2>Website Analytics</h2>
        <p>*Password: peacocksleeves*</p>
        <a href="https://plausible.io/share/M_dR6xwNf1m1XHaaeZBhs" target="_blank">
          https://plausible.io/share/M_dR6xwNf1m1XHaaeZBhs
        </a>
      </div>
      <section className={styles.stats}>
        <div>
          <h2>Shirts Sold</h2>
          <p>{props.stats[0].sales}</p>
          <h2>Sales by Length</h2>
          <p>Long: {props.stats[0].lengths.long}</p>
          <p>Short: {props.stats[0].lengths.short}</p>
          <p>Tuxedo: {props.stats[0].lengths.tuxedo}</p>
        </div>
        <div>
          <h2>Sales by Size</h2>
          <p>SM: {props.stats[0].sizes.SM}</p>
          <p>MD: {props.stats[0].sizes.MD}</p>
          <p>LG: {props.stats[0].sizes.LG}</p>
          <p>XL: {props.stats[0].sizes.XL}</p>
          <p>XXL: {props.stats[0].sizes.XXL}</p>
        </div>
      </section>
      <Usa states={props.stats[0].states} />
    </>
  );
};

export default Stats;
