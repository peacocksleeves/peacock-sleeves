import React, {useEffect, useState} from 'react';
import {useAction, useSelector} from 'redux-zero/react';
import {Grid, _} from 'gridjs-react';

import {getOrders} from '@redux/actions';
import Button from '@components/Button';
import AddUpdateNote from './addUpdateNote';
import apiRequest from '@util/apiRequest';

import styles from './index.scss';

const Orders = () => {
  const orders = useSelector(({orders}) => orders);
  const [order, setOrder] = useState(null);
  const [index, setIndex] = useState(null);
  const [status, setStatus] = useState('Change Status');
  const [showModal, setShowModal] = useState(false);
  const fetchOrders = useAction(getOrders);

  const tableHeaders = [
    {
      name: 'Id',
      width: '150px',
      formatter: cell => `PEAC_${cell}`,
    },
    {
      name: 'Cart',
      width: '300px',
      formatter: cell =>
        _(
          JSON.parse(cell).map((item, i) => (
            <div key={i} style={{display: 'inline-flex', width: '100%', marginLeft: '25px'}}>
              <img
                style={{margin: 'unset'}}
                src={`https://peacock-swatches-${process.env.ENV}.s3.amazonaws.com/${item.imgName}small.jpg`}
                alt="pattern swatch"
              />
              <div style={{display: 'inline-block', paddingLeft: '10px', alignSelf: 'center'}}>
                {item.name} | {item.length} | {item.size} | {item.quantity}
              </div>
            </div>
          ))
        ),
    },
    {
      name: 'Created At',
      width: '150px',
      formatter: cell => new Date(cell).toDateString(),
    },
    {
      name: 'Status',
      width: '150px',
    },
    {
      name: 'View',
      width: '150px',
      formatter: (cell, row) =>
        _(
          <div onClick={() => setOrder(orders.filter(ord => ord.id === row.cells[0].data)[0])}>
            <Button text="Details" type="primary" />
          </div>
        ),
    },
  ];

  useEffect(() => {
    fetchOrders();
  }, []);

  useEffect(() => {
    async function updateOrder() {
      try {
        const requestBody = {status: status.toUpperCase()};
        await apiRequest('PUT', {headers: {}, body: requestBody}, `updateOrder/${order.id}`);
        fetchOrders();
        setOrder(null);
        setStatus('Change Status');
      } catch (err) {
        console.log(err);
        alert("Shit, somethings up and couldn't update status.");
        setStatus('Change Status');
      }
    }
    if (status !== 'Change Status') {
      const conf = confirm(`Change status to:\n\n   ${status}`);
      if (conf) {
        updateOrder();
      } else {
        setStatus('Change Status');
      }
    }
  }, [status]);

  return (
    <div className={styles.orders}>
      <h1>{!order ? 'Orders' : 'Order'}</h1>
      {!order && <h4>Search</h4>}
      {order && <span onClick={() => setOrder(null)}>x</span>}
      {(!order && (
        <Grid
          data={orders.sort((a, b) => (a.createdAt < b.createdAt ? 1 : -1))}
          columns={tableHeaders}
          sort={true}
          autoWidth={false}
          search={true}
          // pagination={{
          //   enabled: true,
          //   limit: 1,
          // }}
          className={{
            table: `${styles.table}`,
          }}
        />
      )) || (
        <div>
          <h2>
            <b>Order Date:</b> {new Date(order.createdAt).toDateString()}
          </h2>
          <h2>
            <b>Order Status:</b> {order.status}
          </h2>
          <div className={styles.status}>
            <div className={styles.statusValue}>{status}</div>
            <select onChange={e => setStatus(e.target.value)} value={status}>
              <option value={status}>{status}</option>
              <option value="Constructing">Constructing</option>
              <option value="Shipped">Shipped</option>
              <option value="Complete">Complete</option>
            </select>
          </div>
          <h3>
            <b>ID:</b> {`${order.prefix}${order.id}`}
          </h3>
          <h3>
            <b>Email:</b> {order.email}
          </h3>
          <h3>
            <b>Customer:</b> {order.name}
          </h3>
          <h3>
            <b>Address:</b> {order.address}
          </h3>
          <h3>
            <b>City:</b> {order.city}
          </h3>
          <h3>
            <b>State:</b> {order.state}
          </h3>
          <h3>
            <b>Zip:</b> {order.zip}
          </h3>
          <table className={styles.table}>
            <thead>
              <tr>
                <th>Sleeves</th>
                {/* <th>Pocket Square</th> */}
                <th>Style</th>
                <th>Size</th>
                <th>Quantity</th>
              </tr>
            </thead>
            <tbody>
              {JSON.parse(order.cart).map((item, i) => (
                <tr key={i}>
                  <td style={{height: '100px'}}>
                    <img
                      src={`https://peacock-swatches-${process.env.ENV}.s3.amazonaws.com/${item.imgName}small.jpg`}
                      alt="pattern swatch"
                    />
                    {item.name}
                    {item.name === 'Custom' && item.imgName === 'custom' && (
                      <div
                        onClick={() => {
                          setShowModal(true);
                          setIndex(i);
                        }}>
                        <Button text="Add Image" type="primary" className={styles.addImageButton} />
                      </div>
                    )}
                  </td>
                  {/* <td>{item.pocket ? 'Yes' : 'No'}</td> */}
                  <td>{item.length}</td>
                  <td>{item.size}</td>
                  <td>{item.quantity}</td>
                </tr>
              ))}
            </tbody>
          </table>
          <h2>Total: ${(order.amount / 100).toFixed(2)}</h2>
          <h3>
            <b>Notes:</b> {order.note}
          </h3>
          {showModal && (
            <AddUpdateNote
              setShowModal={setShowModal}
              setIndex={setIndex}
              fetchOrders={fetchOrders}
              orderId={order.id}
              note={order.note}
              cart={JSON.parse(order.cart)}
              index={index}
            />
          )}
          <div onClick={() => setShowModal(true)}>
            <Button text="Create Note" type="primary" className={styles.addNoteButton} />
          </div>
        </div>
      )}
    </div>
  );
};

export default Orders;
