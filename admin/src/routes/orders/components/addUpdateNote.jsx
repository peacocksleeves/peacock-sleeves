import React, {useState} from 'react';

import Button from '@components/Button';
import apiRequest from '@util/apiRequest';

import styles from './addUpdateNote.scss';

const AddUpdateNote = props => {
  const [loading, setLoading] = useState(false);
  const [messageSuccess, setMessageSuccess] = useState(null);
  const [messageError, setMessageError] = useState(null);
  const [note, setNote] = useState('');
  const [img, setImage] = useState('');

  const fileUpload = e => {
    e.preventDefault();
    e.persist();
    setLoading(true);
    const file = e.target.files[0];
    if (file.type === 'image/jpeg') {
      let reader = new FileReader();
      reader.onloadend = function() {
        var image = new Image();
        image.src = this.result;
        image.addEventListener('load', function() {
          if (this.height > 500 && this.width > 500) {
            setImage(reader.result);
            setLoading(false);
          } else setMessageError('Image has to be at least 500x500 in size. Try again.');
        });
      };
      reader.readAsDataURL(file);
    } else {
      setMessageError('Image has to be a .jpg file. Try again.');
    }
  };

  const submitForm = async () => {
    if (note === '' && img === '') return setMessageError('Fill out the form before submitting.');
    setLoading(true);
    try {
      if (img !== '') props.cart[props.index].imgName = `${props.index}-${props.orderId}`;
      const body =
        img !== ''
          ? {cart: JSON.stringify(props.cart), img, imgName: `${props.index}-${props.orderId}`}
          : {note};
      await apiRequest('PUT', {headers: {}, body}, `updateOrder/${props.orderId}`);
      setLoading(false);
      setNote('');
      setImage('');
      setMessageSuccess('Update Successful!');
      props.fetchOrders();
      setTimeout(() => {
        setMessageSuccess(null);
        props.setShowModal(false);
        props.setIndex(null);
      }, 1500);
    } catch (error) {
      console.log(error);
      setLoading(false);
      setMessageError("Shit didn't work. Try again.");
      setTimeout(() => {
        setMessageError(null);
      }, 1500);
    }
  };

  return (
    <div className={styles.modal}>
      <div className={styles.formContainer}>
        <div className={styles.form}>
          <>
            <h4 className={styles.formTitle}>
              {typeof props.index !== 'number' ? 'Create Note' : 'Add Image'}
            </h4>
            <div
              className={styles.close}
              onClick={() => {
                props.setShowModal(false);
                props.setIndex(null);
              }}>
              x
            </div>
          </>
          <form>
            {typeof props.index !== 'number' && (
              <>
                <label>Note:</label>
                <input
                  onChange={e => {
                    e.preventDefault();
                    e.persist();
                    setNote(e.target.value);
                  }}
                  value={note}
                  type="text"
                  name="note"
                  placeholder="Note*"
                  required
                  pattern="^[a-zA-Z0-9 \.,'-]+$"
                />
              </>
            )}
            {typeof props.index === 'number' && (
              <>
                <label>Image:</label>
                <input onChange={fileUpload} type="file" name="file" id="file" required />
              </>
            )}
            <div onClick={loading ? e => e.preventDefault() : submitForm} style={{display: 'grid'}}>
              <Button className={styles.button} text="Submit" type="primary" loading={loading} />
            </div>
          </form>
          {messageSuccess && <p className={styles.success}>{messageSuccess}</p>}
          {messageError && <p className={styles.error}>{messageError}</p>}
        </div>
      </div>
    </div>
  );
};

export default AddUpdateNote;
