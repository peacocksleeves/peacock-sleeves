import React, {useEffect, useState} from 'react';
import {useAction, useSelector} from 'redux-zero/react';

import {getMemos} from '@redux/actions';
import Button from '@components/Button';
import apiRequest from '@util/apiRequest';

import styles from './index.scss';

const tableHeaders = ['Created', 'Memo', 'Status', 'Actions'];

const Memos = () => {
  const [loading, setLoading] = useState(false);
  const [messageSuccess, setMessageSuccess] = useState(null);
  const [messageError, setMessageError] = useState(null);
  const [newMemo, setNewMemo] = useState('');
  const memos = useSelector(({memos}) => memos);
  const fetchMemos = useAction(getMemos);

  useEffect(() => {
    fetchMemos();
  }, []);

  const memoInput = e => {
    e.preventDefault();
    e.persist();
    setNewMemo(e.target.value);
  };

  const submitMemo = async () => {
    if (newMemo === '') {
      setMessageError('You need to type in an actual new memo in the line above ya fish.');
      setTimeout(() => {
        setMessageError(null);
      }, 3500);
      return;
    }
    setLoading(true);
    try {
      let memo;
      if (memos.filter(memo => memo.status === 'ACTIVE').length > 1) {
        memo = {memo: newMemo, status: 'INACTIVE'};
      } else {
        memo = {memo: newMemo};
      }
      await apiRequest('POST', {headers: {}, body: memo}, 'createMemo');
      setLoading(false);
      setNewMemo('');
      setMessageSuccess('Fuck yaaass memo added!');
      setTimeout(() => {
        setMessageSuccess(null);
        fetchMemos();
      }, 3500);
    } catch (error) {
      console.log(error);
      setLoading(false);
      setMessageError("Shit didn't work man. Try again ya fish.");
      setTimeout(() => {
        setMessageError(null);
      }, 3500);
    }
  };

  const deactivateMemo = async (id, status) => {
    if (status === 'INACTIVE' && memos.filter(memo => memo.status === 'ACTIVE').length > 1) {
      setMessageError('Only two memos can be active at any given time. Deactivate one first.');
      setTimeout(() => {
        setMessageError(null);
      }, 3500);
      return;
    }
    try {
      await apiRequest(
        'PUT',
        {headers: {}, body: {status: status === 'ACTIVE' ? 'INACTIVE' : 'ACTIVE'}},
        `updateMemo/${id}`
      );
      fetchMemos();
    } catch (err) {
      console.log(err);
      setMessageError("Shit, somethings up and couldn't deactivate.");
      setTimeout(() => {
        setMessageError(null);
      }, 2500);
    }
  };

  const deleteMemo = async id => {
    try {
      await apiRequest('DELETE', {headers: {}, body: {}}, `deleteMemo/${id}`);
      fetchMemos();
    } catch (err) {
      console.log(err);
      setMessageError("Shit, somethings up and couldn't delete.");
      setTimeout(() => {
        setMessageError(null);
      }, 2500);
    }
  };

  return (
    <div className={styles.memos}>
      <h1>Memos</h1>
      <div className={styles.memoForm}>
        <form onSubmit={submitMemo}>
          <input
            onChange={memoInput}
            value={newMemo}
            placeholder="New Memo*"
            required
            pattern="^[a-zA-Z \.,'!-]+$"
          />
          <div onClick={loading ? e => e.preventDefault() : submitMemo}>
            <Button text="Add New Memo" type="primary" className={styles.addMemoButton} loading={loading} />
          </div>
        </form>
        {messageSuccess && <p className={styles.success}>{messageSuccess}</p>}
        {messageError && <p className={styles.error}>{messageError}</p>}
      </div>
      <table className={styles.table}>
        <thead>
          <tr>
            {tableHeaders.map((key, i) => {
              return <th key={i}>{key}</th>;
            })}
          </tr>
        </thead>
        <tbody>
          {memos.map(memo => {
            return (
              <tr key={memo.id}>
                <td>{new Date(memo.createdAt).toDateString()}</td>
                <td>{memo.memo}</td>
                <td>{memo.status}</td>
                <td>
                  <div
                    onClick={loading ? e => e.preventDefault() : () => deactivateMemo(memo.id, memo.status)}>
                    <Button
                      text={memo.status === 'INACTIVE' ? 'Activate' : 'Deactivate'}
                      type="primary"
                      className={styles.actionButton}
                      loading={loading}
                    />
                  </div>
                  <div onClick={loading ? e => e.preventDefault() : () => deleteMemo(memo.id, memo.status)}>
                    <Button text="Delete" type="primary" className={styles.actionButton} loading={loading} />
                  </div>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
};

export default Memos;
