const git = require('simple-git/promise');
const util = require('util');
const exec = util.promisify(require('child_process').exec);
const uiRegex = /^admin/g;

async function runUiChecks() {
  const prettier = await exec('npm run prettier');
  console.log(prettier.stdout);
  console.log('\n----------------\nPrettier Complete\n----------------\n');
  const lintStyle = await exec('npm run lint-style');
  console.log(lintStyle.stdout);
  console.log('\n----------------\nLint-Style Complete\n----------------\n');
}

async function validateChanges() {
  try {
    const diffSummary = await git().diffSummary(['--staged']);
    const files = diffSummary.files.map(file => file.file).some(file => uiRegex.test(file));
    if (!files) return;
    await runUiChecks();
  } catch (err) {
    console.log('Error: ', err);
    process.exit(1);
  }
}

validateChanges();
