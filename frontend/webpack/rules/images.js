const images = {
  test: /(\.jpe?g|\.webp|\.png|\.ico)/,
  use: [
    {
      loader: 'file-loader',
      options: {
        name: '[name].[ext]',
        outputPath: 'images/',
      },
    },
  ],
};

export default images;