const images = {
  test: /(\.mp4)/,
  use: [
    {
      loader: 'file-loader',
      options: {
        name: '[name].[ext]',
        outputPath: 'videos/',
      },
    },
  ],
};

export default images;