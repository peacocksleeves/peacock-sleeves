import * as webpack from 'webpack';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import path from 'path';

import fonts from './rules/fonts';
import images from './rules/images';
import videos from './rules/videos';
import svg from './rules/svg';
import jsx from './rules/jsx';

import alias from './alias';

const config = {};

// Entry
config.entry = ['./src/index.jsx'];

// Output
config.output = {
  chunkFilename: '[name]_[hash].bundle.js',
  filename: 'main_[hash].js',
  path: path.resolve(__dirname, '../dist'),
  // chunkLoading: 'require',
  // wasmLoading: false
};

// Rules
config.module = {rules: []};
config.module.rules.push(jsx);
config.module.rules.push(svg);
config.module.rules.push(images);
config.module.rules.push(videos);
config.module.rules.push(fonts);

config.resolve = {
  alias,
  extensions: ['.js', '.jsx', '.scss'],
};

config.plugins = [
  new HtmlWebpackPlugin({
    template: process.env.COCKENV === 'prod' ? 'src/index_prod.html' : 'src/index_dev.html'
  }),
];

config.plugins.push(
  new webpack.DefinePlugin({
    'process.env.PEACOCK_VERSION': JSON.stringify(process.env.npm_package_version),
    'process.env.SHORT_SO': JSON.stringify(55),
    'process.env.LONG_SO': JSON.stringify(70),
    'process.env.TUXEDO_SO': JSON.stringify(85),
  })
);

export default config;
