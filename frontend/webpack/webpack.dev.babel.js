import path from 'path';

import common from './webpack.common';
import {scssDev} from './rules/scss';

const config = {...common};

config.devtool = 'inline-source-map';
config.mode = 'development';

config.devServer = {
  compress: true,
  contentBase: path.resolve(__dirname, '../dist'),
  historyApiFallback: true,
  host: 'localhost',
  hot: true,
  port: 3003,
};

// Dev specific loaders
config.module.rules.push(scssDev);

// Output
config.output.publicPath = '/';

export default config;
