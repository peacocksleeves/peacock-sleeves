export const moduleIds = 'hashed';
export const runtimeChunk = 'single';
export const splitChunks = {
  cacheGroups: {
    styles: {
      chunks: 'all',
      enforce: true,
      name: 'styles',
      test: /\.(scss|css)$/,
    },
    vendor: {
      chunks: 'all',
      enforce: true,
      name: 'vendors',
      test: /[\\/]node_modules[\\/]/,
    },
  },
};
