import React, {useState, useEffect} from 'react';
import {Helmet} from 'react-helmet';
import {useSelector} from 'redux-zero/react';

import ProductGrid from '@components/ProductGrid';

import styles from './index.scss';

const Products = props => {
  const products = useSelector(({products}) => products);
  const categories = useSelector(({categories}) => categories);
  const [filter, setFilter] = useState('All');
  const [filteredProducts, setFilteredProducts] = useState([]);

  useEffect(() => {
    if (filter === 'All') return;
    const filtered = products.filter(product => product.category === filter).map(product => product);
    setFilteredProducts(filtered);
  }, [filter]);

  return (
    <>
      <Helmet>
        <title>Products</title>
        <meta
          name="description"
          content="Search through all the products and styles Peacock Sleeves offers."
        />
      </Helmet>
      <div className={styles.productsPage}>
        <h1>
          {props.location.pathname.includes('short')
            ? 'Short Sleeves'
            : props.location.pathname.includes('long')
            ? 'Long Sleeves'
            : 'Tuxedo Sleeves'}
        </h1>
        <div className={styles.filterContainer}>
          <h2>Category</h2>
          <div className={styles.filter}>
            <div className={styles.filterValue}>{filter}</div>
            <select onChange={e => setFilter(e.target.value)} value={filter}>
              <option value="All">All</option>
              {categories.map(category => (
                <option key={category} value={category}>
                  {category}
                </option>
              ))}
            </select>
          </div>
        </div>
        <ProductGrid
          gridType="triGrid"
          products={filter === 'All' ? products : filteredProducts}
          productType={props.location.pathname.replace('/', '').split('-')[0]}
        />
      </div>
    </>
  );
};

export default Products;
