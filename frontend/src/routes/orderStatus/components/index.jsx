import React, {useState} from 'react';

import Button from '@components/Button';
import ItemAddedView from '@components/ItemAddedView';

import apiRequest from '@util/apiRequest';

import styles from './index.scss';

const OrderStatus = () => {
  const [loading, setLoading] = useState(false);
  const [orderNumber, setOrderNumber] = useState('');
  const [order, setOrder] = useState(null);
  const [error, setError] = useState(null);
  const submitForm = async e => {
    setLoading(true);
    e.preventDefault();
    try {
      const data = await apiRequest('GET', {headers: {}}, `getOrder/${orderNumber.replace('-', '')}`);
      if (data.data.message) {
        setLoading(false);
        setError(data.data.message);
        setTimeout(() => setError(null), 4500);
      } else {
        setLoading(false);
        const orderDetails = {...data.data, cart: JSON.parse(data.data.cart)};
        setOrder(orderDetails);
      }
    } catch (err) {
      console.log(err);
      setLoading(false);
      setError('Unable to process your request at this time. Please try again momentarily.');
      setTimeout(() => setError(null), 4500);
    }
    setOrderNumber('');
  };
  return (
    <>
      <h1 className={styles.h1}>Monitor Your Order</h1>
      {!order && (
        <div className={styles.orderForm}>
          <p>To check the status of your order please enter the order number found in your receipt email.</p>
          <form onSubmit={submitForm}>
            <input
              onChange={e => setOrderNumber(e.target.value)}
              placeholder="Receipt Number*"
              value={orderNumber}
              required
              pattern="^[a-zA-Z0-9\-]+$"
            />
            <div onClick={loading ? e => e.preventDefault() : submitForm}>
              <Button className={styles.button} text="Submit" type="primary" loading={loading} />
            </div>
          </form>
          {error && <p>{error}</p>}
        </div>
      )}
      {order && (
        <div className={styles.orderStatus}>
          <h2>Order Status: {order.status}</h2>
          <h3>Customer: {order.name}</h3>
          <h3>Address: {order.address}</h3>
          <h3>Zip: {order.zip}</h3>
          {order.cart.map((item, i) => (
            <ItemAddedView
              key={i}
              imgName={item.imgName}
              length={item.length}
              pocket={item.pocket}
              itemName={item.name}
              size={item.size}
              quantity={item.quantity}
              price={item.price / item.quantity}
              i={i}
              orderStatus={true}
              custom={item.imgName === 'custom' ? true : false}
            />
          ))}
          <h4>Shipping and Tax: $5.75</h4>
          <h2>Total: ${(order.amount / 100).toFixed(2)}</h2>
        </div>
      )}
    </>
  );
};

export default OrderStatus;
