import React, {useState} from 'react';
import {Helmet} from 'react-helmet';
import {useSelector, useAction} from 'redux-zero/react';

import {createGroup, getGroup, exitGroup} from '@redux/actions';
import Button from '@components/Button';
import Cart from '@routes/cart/components';

import Contact from './invite';

import styles from './index.scss';

const GroupOrder = props => {
  const makeGroup = useAction(createGroup);
  const findGroup = useAction(getGroup);
  const leaveGroup = useAction(exitGroup);
  const group = useSelector(({group}) => group);
  const [searchType, setSearchType] = useState('create');
  const [searchOrAdd, setSearchOrAdd] = useState({email: '', owner: '', passcode: '', type: 'group'});
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);

  const searchOrAddParty = async (e, type) => {
    e.preventDefault();
    setLoading(true);
    if (error !== '') setError('');
    try {
      if (type === 'search') {
        await findGroup(searchOrAdd);
      } else {
        await makeGroup(searchOrAdd);
      }
      setLoading(false);
      setSearchOrAdd({email: '', owner: '', passcode: '', type: 'group'});
    } catch (err) {
      setError(err);
    } finally {
      if (group.error === '' && error === '') {
        document.getElementById('root').scrollIntoView(true);
      }
    }
  };

  return (
    <>
      <Helmet>
        <title>Peacock Sleeves | Group Order</title>
        <meta
          name="description"
          content="Get your friends, team, and colleagues vibing with some Peacock Sleeves dress shirts!"
        />
      </Helmet>
      <h1 className={styles.h1}>Group Orders</h1>
      {group.owner.length === 0 && (
        <>
          <section>
            <div className={styles.groupContainer}>
              <img
                className={styles.groupHeroImg}
                src="https://peacock-media.s3.amazonaws.com/bannerMobile.jpg"
                alt="model in dress shirt"
              />
              <div className={styles.groupText}>
                <h4>Order Options</h4>
                <p>
                  If you and your friends will be purchasing 4 or more shirts separately, shoot us an email at
                  hello@peacocksleeves.com. We will supply you with a Coupon Code for 10% on all your orders
                  to use at checkout!
                </p>
                <br />
                <p>
                  If you're group wants all of the shirts shipped to the same location and all paid for on one
                  transaction, then we think using our Persistent Cart feature is the better option for you.
                  You will still receive 10% off your entire order. A Persistent cart will allow all of your
                  friends to choose their shirt and size and add it to the cart. Once everyone has added their
                  order, you can now go in and finalize the order. All shirts will be shipped to one location.
                </p>
              </div>
            </div>
          </section>
          <section className={styles.groupFormGrid}>
            <div className={styles.groupDisplay}>
              <h4>Be Legends</h4>
              <p>
                Let us help you and the crew get fitted with the dankiest dress shirts perfect for any event.
              </p>
              <h4>How it Works</h4>
              <ul>
                <li>Create a Persistent Cart</li>
                <li>Share Details with Crew</li>
                <li>Build Your Shirts</li>
                <li>Peacock to Glory</li>
              </ul>
            </div>
            <div className={styles.groupForm}>
              <h4>{searchType === 'create' ? 'Create Persistent Cart' : 'Restore Persistent Cart'}</h4>
              <form onSubmit={e => searchOrAddParty(e, searchType)}>
                {searchType === 'create' && (
                  <input
                    onChange={e => setSearchOrAdd({...searchOrAdd, [e.target.name]: e.target.value})}
                    placeholder="First Name*"
                    name="owner"
                    value={searchOrAdd.owner}
                    required
                    pattern="^[a-zA-Z0-9\-]+$"
                  />
                )}
                <input
                  onChange={e => setSearchOrAdd({...searchOrAdd, [e.target.name]: e.target.value})}
                  type="email"
                  name="email"
                  value={searchOrAdd.email}
                  placeholder="Email*"
                  required
                />
                <input
                  onChange={e => setSearchOrAdd({...searchOrAdd, [e.target.name]: e.target.value})}
                  type="text"
                  name="passcode"
                  value={searchOrAdd.passcode}
                  placeholder="Pass Code*"
                  pattern="^[a-zA-Z0-9 \.-_!,]+$"
                  required
                />
                <Button
                  className={styles.button}
                  value={searchType === 'create' ? 'Party On' : 'Find Cart'}
                  type="primary"
                  loading={loading}
                  submit={true}
                />
              </form>
              {(searchType === 'create' && (
                <p>
                  Have a Cart?<span onClick={() => setSearchType('search')}> Find it</span>
                </p>
              )) || (
                <p>
                  Save A Cart?<span onClick={() => setSearchType('create')}> Save it</span>
                </p>
              )}
              {error && <p className={styles.error}>{error}</p>}
              {group.error.length > 0 && <p className={styles.error}>{group.error}</p>}
            </div>
          </section>
        </>
      )}
      {group.owner.length > 0 && (
        <div className={styles.groupStatus}>
          <p className={styles.groupIntro}>
            Your <b>Persistent Cart</b> is <b>Active</b>!
            <br />
            <br />
            As you shop around any items added to cart will be saved in cart. This page and the{' '}
            <i>
              <b>Cart</b>
            </i>{' '}
            page will display your order items.
          </p>
          <h3>To Do</h3>
          <p className={styles.groupToDo}>
            1. Send PCart Details to Crew
            <br />
            2. Add Items to Group Order
            <br />
            3. Checkout and Complete Order
            <br />
            4. Receive Shirts and Party On!
          </p>
          <div className={styles.ctaContainer}>
            <div className={styles.groupCTA} onClick={leaveGroup}>
              <Button text="Exit Group" type="groupCTA" to={'/'} />
            </div>
          </div>
          <h3>{group.owner}'s Group Shirts</h3>
          <Cart match={props.match} history={props.history} location={props.location} group={true} />
        </div>
      )}
    </>
  );
};

export default GroupOrder;
