import React, {useState} from 'react';

import apiRequest from '@util/apiRequest';
import Loading from '@components/Loading';
import logo from '@assets/icons/bwfeather.png';

import styles from './invite.scss';

const Contact = props => {
  const [loading, setLoading] = useState(false);
  const [messageSuccess, setMessageSuccess] = useState(false);
  const [messageError, setMessageError] = useState(false);

  const [emails, setEmails] = useState('');

  // const inputChange = e => {
  //   e.preventDefault();
  //   e.persist();
  //   setFormData({...formData, [e.target.name]: e.target.value});
  // };

  const submitForm = async e => {
    e.preventDefault();
    setLoading(true);
    const emailArray = emails.split(',').map(email => email.trim());
    const requestBody = {
      source: process.env.ENV === 'prod' ? 'prod' : 'test',
      owner: props.owner,
      passcode: props.passcode,
      emails: emailArray,
    };
    try {
      const resp = await apiRequest('POST', {headers: {}, body: requestBody}, 'groupInvite');
      if (resp.status === 400) throw error;
      setLoading(false);
      setMessageSuccess(true);
      setTimeout(() => {
        setMessageSuccess(false);
        props.setShowContact(false);
      }, 2000);
    } catch (error) {
      setLoading(false);
      setMessageError(true);
      setTimeout(() => {
        setMessageError(false);
      }, 3500);
    }
  };

  return (
    <div className={styles.modal} onClick={() => props.setShowContact(false)}>
      <div className={styles.formContainer}>
        <div
          className={styles.form}
          onClick={e => {
            e.stopPropagation();
          }}>
          <>
            <div className={styles.logoContainer}>
              <img src={logo} className={styles.logo} alt="peacock sleeves logo" />
              <span className={styles.logoTitle}>Peacock</span>
              <span className={styles.logoTitle}>Sleeves</span>
            </div>
            <h2 className={styles.formTitle}>Invite Others to Group</h2>
            <span className={styles.close} onClick={() => props.setShowContact(false)}>
              x
            </span>
          </>
          <form onSubmit={submitForm}>
            <input
              onChange={e => setEmails(e.target.value)}
              type="text"
              name="emails"
              placeholder="john@smith.com, jane@smith.com*"
              pattern="^[a-zA-Z0-9 \.-_@,]+$"
              required
            />
            <span className={styles.commaInfo}>Use commas to separate email addresses</span>
            {/* <p className={styles.messageLabel}>Attached Message</p> */}
            {/* <textarea className={styles.messageArea} name={'message'} onChange={inputChange} /> */}
            {(loading && (
              <div id={styles.sendEmail} style={{display: 'grid'}}>
                <Loading />
              </div>
            )) || (
              <input
                id={styles.sendEmail}
                style={{display: 'grid'}}
                type="submit"
                value="Send It &#129305;"
              />
            )}
          </form>
          {messageSuccess && <p className={styles.success}>Invites sent successfully &#129305;</p>}
          {messageError && (
            <p className={styles.error}>Shit something's wrong on our end. Try again shortly.</p>
          )}
        </div>
      </div>
    </div>
  );
};

export default Contact;
