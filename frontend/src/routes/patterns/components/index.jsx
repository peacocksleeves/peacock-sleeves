import React, {useState} from 'react';
import {Helmet} from 'react-helmet';
import {useSelector} from 'redux-zero/react';
import {Link} from 'react-router-dom';

import Button from '@components/Button';
import ProductSwatch from '@components/ProductSwatch';

import styles from './index.scss';

const Patterns = () => {
  const products = useSelector(({products}) => products);
  const categories = useSelector(({categories}) => categories);
  const [filter, setFilter] = useState('All');

  return (
    <>
      <Helmet>
        <title>Patterns</title>
        <meta name="description" content="Search through all the fabric designs Peacock Sleeves offers." />
      </Helmet>
      <div className={styles.patterns}>
        <div className={styles.filterContainer}>
          <h2>Category</h2>
          <div className={styles.filter}>
            <div className={styles.filterValue}>{filter}</div>
            <select onChange={e => setFilter(e.target.value)} value={filter}>
              <option value="All">All</option>
              {categories.map(category => (
                <option key={category} value={category}>
                  {category}
                </option>
              ))}
            </select>
          </div>
        </div>
        <div className={styles.swatchSection}>
          {(filter === 'All' &&
            products.map(product => (
              <div key={product.id} className={styles.swatchContainer}>
                <Link to={`/build/sleeve/${product.imgName}`}>
                  <ProductSwatch
                    active={false}
                    imgID={styles.swatch}
                    divID={styles.swatchDiv}
                    product={product}
                    setSleeves={() => null}
                    setSelectedProduct={() => null}
                  />
                  <p>{product.name}</p>
                  <Button className={styles.buildItButton} text="Build It" type="primary" />
                </Link>
              </div>
            ))) ||
            products
              .filter(product => product.category === filter)
              .map(product => (
                <div key={product.id} className={styles.swatchContainer}>
                  <Link to={`/build/sleeve/${product.imgName}`}>
                    <ProductSwatch
                      active={false}
                      imgID={styles.swatch}
                      divID={styles.swatchDiv}
                      product={product}
                      setSleeves={() => null}
                      setSelectedProduct={() => null}
                    />
                    <p>{product.name}</p>
                    <Button className={styles.buildItButton} text="Build It" type="primary" />
                  </Link>
                </div>
              ))}
        </div>
      </div>
    </>
  );
};

export default Patterns;
