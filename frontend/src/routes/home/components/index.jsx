import React, {useState} from 'react';
import {Helmet} from 'react-helmet';
import {useSelector} from 'redux-zero/react';
import {Link} from 'react-router-dom';

import Reviews from './reviews.jsx';

import Header from '@components/Header';
import ProductGrid from '@components/ProductGrid';
import ProductSwatch from '@components/ProductSwatch';
import Button from '@components/Button';
import Contact from '@components/Footer/contact';

import shortshirt from '@assets/imgs/shortshirt.png';
import longshirt from '@assets/imgs/longshirt.png';
import tuxedoshirt from '@assets/imgs/tuxedoshirt.png';

import styles from './index.scss';

const Home = props => {
  const products = useSelector(({products}) => products);
  const categories = useSelector(({categories}) => categories);
  const [showContact, setShowContact] = useState(false);
  return (
    <>
      <Helmet>
        <title>Peacock Sleeves</title>
        <meta name="description" content="Find your next party sleeves from Peacock Sleeves." />
      </Helmet>
      <Header>
        <div className={styles.headerContainer}>
          <div
            style={{
              backgroundImage: 'url(https://peacock-media.s3.amazonaws.com/newHero.jpg)',
            }}
            className={styles.banner}
          />
          <div className={styles.bannerText}>
            <h3 className={styles.subtitle}>Classy in the Front</h3>
            <p>Party vibes on the Sides</p>
            <Button className={styles.shopButton} text="Build A Shirt" type="primary" to="/build" />
          </div>
        </div>
      </Header>
      {window.innerWidth > 767 && (
        <section>
          <div className={styles.stageContainer}>
            <div>
              <img src={'https://peacock-media.s3.amazonaws.com/stage1.jpg'} alt="stage 1 of peacocking" />
              <h3>Parents Love You</h3>
            </div>
            <div>
              <img src={'https://peacock-media.s3.amazonaws.com/stage2.jpg'} alt="stage 2 of peacocking" />
              <h3>Friends Want to Be You</h3>
            </div>
            <div>
              <img src={'https://peacock-media.s3.amazonaws.com/stage3.jpg'} alt="stage 3 of peacocking" />
              <h3>You Love You</h3>
            </div>
          </div>
        </section>
      )}
      <section>
        <div className={styles.featured}>
          <h1>Featured Sleeves</h1>
        </div>
        <ProductGrid gridType="scrollGrid" products={products.slice(0, 9)} />
        {window.innerWidth < 1025 && <div className={styles.scrollIndicator}>&#8592; Scroll &#8594;</div>}
      </section>
      <section>
        <div className={styles.featured}>
          <h1>New Arrivals</h1>
        </div>
        <ProductGrid
          gridType="scrollGrid"
          products={products
            .map(product => product)
            .sort((a, b) => {
              if (a.createdAt > b.createdAt) return -1;
              if (a.createdAt < b.createdAt) return 1;
            })
            .slice(0, 9)}
        />
        {window.innerWidth < 1025 && <div className={styles.scrollIndicator}>&#8592; Scroll &#8594;</div>}
      </section>
      {categories.slice(0, 4).map((category, i) => {
        if (i === 0) {
          return (
            <div key={i}>
              <section>
                <div>
                  <div
                    style={{
                      backgroundImage:
                        window.innerWidth < 1025
                          ? 'url(https://peacock-media.s3.amazonaws.com/model1.jpg)'
                          : 'url(https://peacock-media.s3.amazonaws.com/skater.jpg)',
                      width: '100%',
                      margin: '50px auto',
                      marginBottom: 0,
                      height: '60vh',
                      display: 'block',
                    }}
                    className={styles.catBanner}
                    onClick={() => setShowContact(!showContact)}>
                    <div className={styles.catText}>
                      <h4>Drip in Style</h4>
                      <p>Peacocking is part clothes, part style, and a state of mind.</p>
                      <Button
                        className={styles.shopButton}
                        text="Peacock Styles"
                        type="primary"
                        to="/gallery"
                      />
                    </div>
                  </div>
                  <div className={styles.swatchDisplay}>
                    <div className={styles.swatchDisplayContainer}>
                      <div className={styles.swatchText}>
                        <h2>Shirt Styles</h2>
                        <p>
                          Custom build your own shirt! Combine any available print patterns and shirt styles
                          we offer. We can also work with you on custom orders in styles not offered.
                        </p>
                      </div>
                      <div className={styles.shirtGrid}>
                        <div className={styles.shirt}>
                          <Link to={'/long-sleeves'}>
                            <img src={longshirt} alt="long sleeve shirt" />
                            <p>Long Sleeves</p>
                          </Link>
                        </div>
                        <div className={styles.shirt}>
                          <Link to={'/short-sleeves'}>
                            <img src={shortshirt} alt="short sleeve shirt" />
                            <p>Short Sleeves</p>
                          </Link>
                        </div>
                        <div className={styles.shirt}>
                          <Link to={'/tuxedo-sleeves'}>
                            <img src={tuxedoshirt} alt="tuxedo sleeve shirt" />
                            <p>Tuxedo Sleeves</p>
                          </Link>
                        </div>
                      </div>
                      <Button className={styles.shopButton} text="Make A Shirt" type="primary" to="/build" />
                    </div>
                  </div>
                </div>
              </section>
              <section className={styles.featured} style={{position: 'relative'}}>
                <h1>{category}</h1>
                <ProductGrid
                  gridType="scrollGrid"
                  products={products.filter(product => product.category === category)}
                />
                {window.innerWidth < 1025 && (
                  <div className={styles.scrollIndicator}>&#8592; Scroll &#8594;</div>
                )}
              </section>
            </div>
          );
        } else if (i === 1) {
          return (
            <div key={i}>
              <section className={styles.featured} style={{position: 'relative'}}>
                <h1>{category}</h1>
                <ProductGrid
                  gridType="scrollGrid"
                  products={products.filter(product => product.category === category)}
                />
                {window.innerWidth < 1025 && (
                  <div className={styles.scrollIndicator}>&#8592; Scroll &#8594;</div>
                )}
              </section>
              <section>
                {window.innerWidth > 1025 && (
                  <div
                    style={{
                      backgroundImage: 'url(https://peacock-media.s3.amazonaws.com/model1.jpg)',
                    }}
                    className={styles.catBanner}
                    onClick={() => props.history.push('/build')}>
                    <div className={styles.catText}>
                      <h4>Hide it or Show it</h4>
                      <p>Hide the sleeves wearing a jacket, then peacock at the after party.</p>
                      <Button className={styles.shopButton} text="Make A Shirt" type="primary" to="/build" />
                    </div>
                  </div>
                )}
                <div
                  style={{
                    backgroundImage: 'url(https://peacock-media.s3.amazonaws.com/model3.jpg)',
                    width: window.innerWidth < 1025 ? '100%' : '50%',
                  }}
                  className={styles.catBanner}
                  onClick={() => setShowContact(!showContact)}>
                  <div className={styles.catText}>
                    <h4>Endless Patterns</h4>
                    <p>We can work with you to make the exact shirt you want.</p>
                    <Button className={styles.shopButton} text="Hit Us Up" type="primary" to="/" />
                  </div>
                </div>
              </section>
              <section className={styles.swatchDisplay}>
                <div className={styles.swatchDisplayContainer}>
                  <div className={styles.swatchText}>
                    <h2>Pick A Pattern</h2>
                    <p>
                      We make every order custom. If you don't see a pattern being offered by us that you know
                      is available for purchase from fabric retailers, hit us up and we'll make the order
                      together.
                    </p>
                  </div>
                  <div className={styles.swatchSection}>
                    {products.slice(0, window.innerWidth > 476 ? 3 : 1).map(product => (
                      <ProductSwatch
                        key={product.id}
                        active={false}
                        imgID={styles.swatch}
                        divID={styles.swatchDiv}
                        product={product}
                        setSleeves={() => null}
                        setSelectedProduct={() => null}
                      />
                    ))}
                  </div>
                  <Button className={styles.shopButton} text="View Patterns" type="primary" to="/patterns" />
                </div>
              </section>
            </div>
          );
        } else {
          return (
            <section className={styles.featured} key={i} style={{position: 'relative'}}>
              <h1>{category}</h1>
              <ProductGrid
                gridType="scrollGrid"
                products={products.filter(product => product.category === category)}
              />
              {window.innerWidth < 1025 && (
                <div className={styles.scrollIndicator}>&#8592; Scroll &#8594;</div>
              )}
            </section>
          );
        }
      })}
      {/* <section className={styles.featured}>
        <h1>Customer Reviews &#128591;</h1>
        <Reviews />
      </section> */}
      {showContact && (
        <Contact onClick={() => setShowContact(!showContact)} setShowContact={setShowContact} />
      )}
    </>
  );
};

export default Home;
