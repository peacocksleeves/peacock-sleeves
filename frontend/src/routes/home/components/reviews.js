export const reviews = [
  {
    text: "Awesome work with the shirts. PS rules bro. Hells ya it's lit man.",
    img: 'https://peacock-media.s3.amazonaws.com/brian.jpg',
    name: 'poop poop',
    initials: 'PP',
    date: '10/30/2020',
  },
  {
    text: "We couldn't pass up the opportunity to wear our red, white, and blue on this patriotic weekend.",
    img: 'https://peacock-media.s3.amazonaws.com/brian.jpg',
    name: 'fashion flyer',
    initials: 'FF',
    date: '01/17/2021',
  },
  {
    text:
      'So fun working with Peacock Sleeves, who made some custom shirts for us to party in. So fun working with Peacock Sleeves, who made some custom shirts for us to party in. So fun working with Peacock Sleeves, who made some custom shirts for us to party in.',
    img: 'https://peacock-media.s3.amazonaws.com/brian.jpg',
    name: 'rhfp',
    initials: 'EF',
    date: '12/10/2020',
  },
  {
    text: 'All of our wedding guests LOVED the sleeves. It made the reception so much fun!',
    img: 'https://peacock-media.s3.amazonaws.com/brian.jpg',
    name: 'Morgan V.',
    initials: 'MV',
    date: '12/10/2020',
  },
];
