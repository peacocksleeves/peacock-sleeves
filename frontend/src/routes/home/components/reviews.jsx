import React from 'react';

import {reviews} from './reviews.js';

import styles from './reviews.scss';

const Review = () => {
  return (
    <div className={styles.reviewsContainer}>
      {reviews.map(review => (
        <div key={review.initials} className={styles.review}>
          <img src={review.img} />
          <span>{review.initials}</span>
          <div className={styles.ptext}>
            <p>{review.name}</p>
            <p>{review.date}</p>
            <p>{review.text}</p>
          </div>
          <div className={styles.stars}>
            <b>&#11089;</b>
            <b>&#11089;</b>
            <b>&#11089;</b>
            <b>&#11089;</b>
            <b>&#11089;</b>
          </div>
        </div>
      ))}
    </div>
  );
};

export default Review;
