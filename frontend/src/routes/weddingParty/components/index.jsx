import React, {useState} from 'react';
import {Helmet} from 'react-helmet';
import {useSelector, useAction} from 'redux-zero/react';

import {createGroup, getGroup, exitGroup} from '@redux/actions';
import Button from '@components/Button';
import Cart from '@routes/cart/components';

import Contact from './invite';

import styles from './index.scss';

const WeddingParty = props => {
  const createParty = useAction(createGroup);
  const getParty = useAction(getGroup);
  const exitParty = useAction(exitGroup);
  const party = useSelector(({group}) => group);
  const [searchType, setSearchType] = useState('create');
  const [searchOrAdd, setSearchOrAdd] = useState({email: '', owner: '', passcode: '', type: 'wedding'});
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const [showContact, setShowContact] = useState(false);

  const searchOrAddParty = async (e, type) => {
    e.preventDefault();
    setLoading(true);
    if (error !== '') setError('');
    try {
      if (type === 'search') {
        await getParty(searchOrAdd);
      } else {
        await createParty(searchOrAdd);
      }
      setLoading(false);
      setSearchOrAdd({email: '', owner: '', passcode: '', type: 'wedding'});
    } catch (err) {
      setError(err);
    } finally {
      if (party.error === '' && error === '') {
        document.getElementById('root').scrollIntoView(true);
      }
    }
  };

  return (
    <>
      <Helmet>
        <title>Peacock Sleeves | Wedding Party</title>
        <meta
          name="description"
          content="Have your groomsmen ready for the reception with some Peacock Sleeves dress shirts!"
        />
      </Helmet>
      <h1 className={styles.h1}>Wedding Party</h1>
      {party.owner.length === 0 && (
        <>
          <section>
            <div className={styles.weddingContainer}>
              <img
                className={styles.weddingHeroImg}
                src={'https://peacock-media.s3.amazonaws.com/weddingcrop.jpg'}
                alt="model in dress shirt"
              />
              <div className={styles.weddingDisplay}>
                <h4>Be Legends</h4>
                <p>
                  Let us help you and the boys get the dankiest wedding dress shirts for your groomsmen party.
                </p>
                <h4>How it Works</h4>
                <ul>
                  <li>Start a New Party</li>
                  <li>Invite Groomsmen</li>
                  <li>Build Your Shirts</li>
                  <li>Peacock to Glory</li>
                </ul>
              </div>
            </div>
          </section>
          <section className={styles.weddingFormGrid}>
            <p>
              When you create a party you'll be able to send invites to your groomsmen. They'll receive an
              email with instructions and credentials to add to the group order from anywhere. When all
              groomsmen have selected their shirts, you create the order like any other. We will ship all the
              shirts to a single location to then be distributed accordingly.
            </p>
            <div className={styles.partyForm}>
              <h4>{searchType === 'create' ? 'Create Party' : 'Search Party'}</h4>
              <form onSubmit={e => searchOrAddParty(e, searchType)}>
                {searchType === 'create' && (
                  <input
                    onChange={e => setSearchOrAdd({...searchOrAdd, [e.target.name]: e.target.value})}
                    placeholder="Groom First Name*"
                    name="owner"
                    value={searchOrAdd.owner}
                    required
                    pattern="^[a-zA-Z0-9\-]+$"
                  />
                )}
                <input
                  onChange={e => setSearchOrAdd({...searchOrAdd, [e.target.name]: e.target.value})}
                  type="email"
                  name="email"
                  value={searchOrAdd.email}
                  placeholder="Email*"
                  required
                />
                <input
                  onChange={e => setSearchOrAdd({...searchOrAdd, [e.target.name]: e.target.value})}
                  type="text"
                  name="passcode"
                  value={searchOrAdd.passcode}
                  placeholder="Pass Code*"
                  pattern="^[a-zA-Z0-9 \.-_!,]+$"
                  required
                />
                <Button
                  className={styles.button}
                  value={searchType === 'create' ? 'Party On' : 'Find Party'}
                  type="primary"
                  loading={loading}
                  submit={true}
                />
              </form>
              {(searchType === 'create' && (
                <p>
                  With a party?<span onClick={() => setSearchType('search')}> Join in</span>
                </p>
              )) || (
                <p>
                  Need a party?<span onClick={() => setSearchType('create')}> Start one</span>
                </p>
              )}
              {error && <p className={styles.error}>{error}</p>}
              {party.error.length > 0 && <p className={styles.error}>{party.error}</p>}
            </div>
          </section>
        </>
      )}
      {party.owner.length > 0 && (
        <div className={styles.partyStatus}>
          <p className={styles.partyIntro}>
            Your <b>Party</b> is <b>Active</b>!
            <br />
            <br />
            As you shop around any items added to cart will be saved in the wedding party group order. This
            page and the{' '}
            <i>
              <b>Cart</b>
            </i>{' '}
            page will display your wedding party items.
          </p>
          <h3>To Do</h3>
          <p className={styles.partyToDo}>
            1. Send Invites to Groomsmen
            <br />
            2. Add Items to Group Order
            <br />
            3. Checkout and Complete Order
            <br />
            4. Receive Shirts and Party On!
          </p>
          <div className={styles.ctaContainer}>
            <div className={styles.weddingCTA} onClick={exitParty}>
              <Button text="Exit Wedding" type="groupCTA" to={'/'} />
            </div>
            <div className={styles.weddingCTA} onClick={() => setShowContact(true)}>
              <Button text="Send Invites" type="groupCTA" />
            </div>
          </div>
          <h3>{party.owner}'s Groomsmen Shirts</h3>
          <Cart match={props.match} history={props.history} location={props.location} group={true} />
        </div>
      )}
      {showContact && (
        <Contact setShowContact={setShowContact} owner={party.owner} passcode={party.passcode} />
      )}
    </>
  );
};

export default WeddingParty;
