import React, {useState} from 'react';

import SizeChart from '@components/SizeChart';

import styles from './index.scss';

const FAQ = props => {
  const [open, setOpen] = useState([]);
  return (
    <div className={styles.faq}>
      <h1>FAQ</h1>
      <div>
        <h2>Orders</h2>
        <div
          className={styles.question}
          onClick={() => {
            if (open.includes('ship')) {
              setOpen(open.filter(op => op == !'ship'));
            } else {
              setOpen([...open, 'ship']);
            }
          }}>
          <i className={styles.arrow}></i>
          How long will it take for my order to be shipped?
        </div>
        {open.includes('ship') && (
          <div className={styles.answer}>
            All shirts are custom made when ordered. Because of this shipping times can differ, but we on
            average ship orders in 2 weeks. We ship via USPS First Class Mail and it takes typically 3-5 days
            for packages to arrive at your destination once the shirt leaves our shop.
            <br />
            <br />
            For custom fabric orders expect a longer timeline because we have to order the pattern fabric
            before constructing the shirts. These orders ship on average in 3 weeks.
          </div>
        )}
      </div>
      <div>
        <div
          className={styles.question}
          onClick={() => {
            if (open.includes('return')) {
              setOpen(open.filter(op => op == !'return'));
            } else {
              setOpen([...open, 'return']);
            }
          }}>
          <i className={styles.arrow}></i>
          Can I return an order?
        </div>
        {open.includes('return') && (
          <div className={styles.answer}>
            Yes, see our{' '}
            <i
              style={{textDecoration: 'underline', fontWeight: 'bold', cursor: 'pointer'}}
              onClick={() => props.history.push('/returns')}>
              return policy
            </i>{' '}
            and instructions on our return guide.
          </div>
        )}
      </div>
      <h2>Shirt Construction</h2>
      <div>
        <div
          className={styles.question}
          onClick={() => {
            if (open.includes('sizing')) {
              setOpen(open.filter(op => op == !'sizing'));
            } else {
              setOpen([...open, 'sizing']);
            }
          }}>
          <i className={styles.arrow}></i>
          Is there a size guide for dress, mess, and short sleeve shirts?
        </div>
        {open.includes('sizing') && (
          <div className={styles.answer}>
            Yes, look for the &#9432; when selecting sizes on our shopping pages. Here's another look at our
            size guide.
            <SizeChart />
          </div>
        )}
      </div>
      <div>
        <div
          className={styles.question}
          onClick={() => {
            if (open.includes('alter')) {
              setOpen(open.filter(op => op == !'alter'));
            } else {
              setOpen([...open, 'alter']);
            }
          }}>
          <i className={styles.arrow}></i>
          Can I customize/alter my shirt size?
        </div>
        {open.includes('alter') && (
          <div className={styles.answer}>
            We do not offer alterations at this time, but we're working on providing this service. Feel free
            to{' '}
            <i
              style={{textDecoration: 'underline', fontWeight: 'bold', cursor: 'pointer'}}
              onClick={() => props.setShowContact(true)}>
              contact us
            </i>{' '}
            and we can work with you on making fully custom shirts.
          </div>
        )}
      </div>
      <div>
        <div
          className={styles.question}
          onClick={() => {
            if (open.includes('tuxedo')) {
              setOpen(open.filter(op => op == !'tuxedo'));
            } else {
              setOpen([...open, 'tuxedo']);
            }
          }}>
          <i className={styles.arrow}></i>
          Do your tuxedo sleeves comply to military ball uniforms?
        </div>
        {open.includes('tuxedo') && (
          <div className={styles.answer}>
            Our Tuxedo shirts include all the touches perfect for formal outings and military mess dress
            uniforms!
            <ul style={{margin: '5px 35px'}}>
              <li>French Cuffs + Links</li>
              <li>Pleated Tuxedo Front</li>
              <li>English Spread Collar</li>
              <li>Black Stud Buttons</li>
              <li>Any Peacock Sleeves</li>
            </ul>
          </div>
        )}
      </div>
      <div>
        <div
          className={styles.question}
          onClick={() => {
            if (open.includes('material')) {
              setOpen(open.filter(op => op == !'material'));
            } else {
              setOpen([...open, 'material']);
            }
          }}>
          <i className={styles.arrow}></i>
          What material are Peacock Sleeves made out of?
        </div>
        {open.includes('material') && (
          <div className={styles.answer}>
            We use a Performance-Tech blend of cotton, nylon, and spandex that is light weight and sturdy. No
            doubt your shirt will survive a night of partying and dancining! We recommend when first receiving
            you shirt to give a once over with an iron or steamer to make sure it's looking great for your
            night out.
          </div>
        )}
      </div>
      <div>
        <div
          className={styles.question}
          onClick={() => {
            if (open.includes('wash')) {
              setOpen(open.filter(op => op == !'wash'));
            } else {
              setOpen([...open, 'wash']);
            }
          }}>
          <i className={styles.arrow}></i>
          How should I wash and care for my Peacock Sleeves shirts?
        </div>
        {open.includes('wash') && (
          <div className={styles.answer}>
            We recommend dry cleaning your shirts. All the beer, food, and other stains from a long night
            peacocking will take some heavy duty cleaning to bring your shirt back to fresh state. Dry
            cleaning your shirts is a safe and fragile method to ensure the longevity and dazzle of your
            shirts.
          </div>
        )}
      </div>
      <h2>Patterns</h2>
      <div>
        <div
          className={styles.question}
          onClick={() => {
            if (open.includes('license')) {
              setOpen(open.filter(op => op == !'license'));
            } else {
              setOpen([...open, 'license']);
            }
          }}>
          <i className={styles.arrow}></i>
          Is Peacock Sleeves an official licensed vendor for Sports and Collegiate logos?
        </div>
        {open.includes('license') && (
          <div className={styles.answer}>
            No, we purchase these fabrics from suppliers which do have the licensing rights to print these
            fabrics. The shirts we sell with these patterns are not licensed products. The materials used in
            these products use licensed fabric.
          </div>
        )}
      </div>
      <div>
        <div
          className={styles.question}
          onClick={() => {
            if (open.includes('pattern')) {
              setOpen(open.filter(op => op == !'pattern'));
            } else {
              setOpen([...open, 'pattern']);
            }
          }}>
          <i className={styles.arrow}></i>
          What can I do if I don't see a pattern available that I've seen being supplied by other vendors?
        </div>
        {open.includes('pattern') && (
          <div className={styles.answer}>
            You can{' '}
            <i
              style={{textDecoration: 'underline', fontWeight: 'bold', cursor: 'pointer'}}
              onClick={() => props.setShowContact(true)}>
              contact us
            </i>{' '}
            and we'll work with you to complete an order with any pattern that exists. We can even work with
            you to print custom patterns for your shirts. Contact us and we'll be happy to make the shirt of
            your dreams.
          </div>
        )}
      </div>
      <h2>Discounts</h2>
      <div>
        <div
          className={styles.question}
          onClick={() => {
            if (open.includes('wedding')) {
              setOpen(open.filter(op => op == !'wedding'));
            } else {
              setOpen([...open, 'wedding']);
            }
          }}>
          <i className={styles.arrow}></i>
          I'm planning my wedding, and I want my groomsmen to wear Peacock Sleeves dress shirts?
        </div>
        {open.includes('wedding') && (
          <div className={styles.answer}>
            We agree, a peacocking groomsmen party is the way! We make it easy to start a groomsmen party
            order with a persistent cart. Once you add an item to your cart you'll see in the cart page a
            small orange form at the bottom to create a persistent cart. All you need to do to create a
            persistent cart is provide your email, name, and a passcode. After that share your email and
            passcode to your wedding party and then your groomsmen can use that info to restore the persistent
            cart and start to pick their shirts and sizes. When the persistent cart is restored, any items
            added to the cart will be automatically saved. Once ready you can make the order!
          </div>
        )}
      </div>
      <div>
        <div
          className={styles.question}
          onClick={() => {
            if (open.includes('group')) {
              setOpen(open.filter(op => op == !'group'));
            } else {
              setOpen([...open, 'group']);
            }
          }}>
          <i className={styles.arrow}></i>
          If I'm making a group order is there some kind of discount?
        </div>
        {open.includes('group') && (
          <div className={styles.answer}>
            There's no better feeling than peacocking with your crew! We want more crews peacocking together.
            We are willing to provide a 10% discount on all group orders including more than 4 shirts. When
            ready{' '}
            <i
              style={{textDecoration: 'underline', fontWeight: 'bold', cursor: 'pointer'}}
              onClick={() => props.setShowContact(true)}>
              contact us
            </i>{' '}
            to receive your discount code.
          </div>
        )}
      </div>
      {/* <div>
        <div
          className={styles.question}
          onClick={() => {
            if (open.includes('military')) {
              setOpen(open.filter(op => op == !'military'));
            } else {
              setOpen([...open, 'military']);
            }
          }}>
          <i className={styles.arrow}></i>
          Do you offer military discounts?
        </div>
        {open.includes('military') && (
          <div className={styles.answer}>
            Yes of course! Most of us at Peacock Sleeves are currently serving or ex-military. You can{' '}
            <i
              style={{textDecoration: 'underline', fontWeight: 'bold', cursor: 'pointer'}}
              onClick={() => props.setShowContact(true)}>
              contact us
            </i>{' '}
            and we'll set you up with a discount code.
          </div>
        )}
      </div> */}
    </div>
  );
};
export default FAQ;
