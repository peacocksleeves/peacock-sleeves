import React, {useState, useEffect} from 'react';
import {useSelector, useAction} from 'redux-zero/react';

import {changeQuantity, resetCart, createGroup, getGroup} from '@redux/actions';
import apiRequest from '@util/apiRequest';
import ItemAddedView from '@components/ItemAddedView';
import Button from '@components/Button';

import {loadStripe} from '@stripe/stripe-js';
const stripeKey =
  process.env.ENV === 'prod'
    ? 'pk_live_HXV71tbRTyXMsAOUxc0yekV0004roBBh23'
    : 'pk_test_51GqU4JDvj8m836F9ne3yucv08BSn2Uoy2WEu3xvejCReGmg5s2GMVRKETSClUoUjEusaot6CQqka3hRG7gkQsdBw00kXuy4Wts';
const stripePromise = loadStripe(stripeKey);

import logo from '@assets/icons/feather.png';
import styles from './index.scss';

const Cart = props => {
  const emptyCart = useAction(resetCart);
  const changeItemQuantity = useAction(changeQuantity);
  const makeGroup = useAction(createGroup);
  const findGroup = useAction(getGroup);
  const [searchType, setSearchType] = useState('create');
  const [searchOrAdd, setSearchOrAdd] = useState({email: '', owner: '', passcode: '', type: 'group'});
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState(null);
  const cart = useSelector(({cart}) => cart);
  const group = useSelector(({group}) => group);
  const price = useSelector(({price}) => price);
  const [notification, setNotification] = useState({
    error: '',
    success: '',
  });

  useEffect(() => {
    if (
      props.match.params.status === 'success' &&
      sessionStorage.getItem('Sess') === props.location.search.split('=')[1]
    ) {
      sessionStorage.removeItem('Sess');
      setNotification({
        ...notification,
        success: 'payment success',
      });
      emptyCart();
      setSearchType('search');
    }
  }, [props.match.params]);

  const submit = async e => {
    e.preventDefault();
    try {
      const data = await apiRequest('POST', {headers: {}, body: cart}, 'createCheckout');
      sessionStorage.setItem('Sess', data.data.sessionId);
      const stripe = await stripePromise;
      const {error} = await stripe.redirectToCheckout({sessionId: data.data.sessionId});
      if (error) {
        setNotification({error: 'Please try again. There was an issue with Stripe.'});
        setTimeout(() => setNotification({...notification, error: ''}), 4500);
      }
    } catch (err) {
      setNotification({error: 'Please try again. There was an issue with Stripe.'});
      setTimeout(() => setNotification({...notification, error: ''}), 4500);
    }
  };

  const searchOrAddParty = async (e, type) => {
    e.preventDefault();
    setLoading(true);
    if (error !== '') setError('');
    try {
      if (type === 'search') {
        await findGroup(searchOrAdd);
      } else {
        await makeGroup(searchOrAdd);
      }
      setLoading(false);
      setSearchOrAdd({email: '', owner: '', passcode: '', type: 'group'});
      setSearchType('create');
    } catch (err) {
      setTimeout(() => setError(err), 4500);
    } finally {
      if (group.error === '' && error === '') {
        document.getElementById('root').scrollIntoView(true);
      }
    }
  };

  return (
    <div className={styles.cart}>
      {cart.length > 0 ? (
        <>
          <h2>Cart Details</h2>
          <div>
            <p style={{textAlign: 'center'}}>
              <i style={{color: '#03015C'}}>
                We custom make every order, so expect 2-3 weeks for your order to be received.
              </i>
            </p>
            <br />
            {cart.map((item, i) => (
              <ItemAddedView
                key={i}
                imgName={item.imgName}
                length={item.length}
                pocket={item.pocket}
                itemName={item.name}
                size={item.size}
                quantity={item.quantity}
                price={item.price}
                changeQuantity={changeItemQuantity}
                i={i}
                cart={true}
                custom={item.imgName === 'custom' ? true : false}
              />
            ))}
          </div>
          <div className={styles.shiptax}>
            <p>Shipping and Tax Fee:</p>
            <p>$5.75</p>
          </div>
          <div id={styles.confirmOrder} onClick={submit}>
            Checkout
            <p className={styles.price}>${price + 5.75}</p>
          </div>
          {(group.owner.length > 0 && (
            <div className={styles.shiptax}>
              <p>{group.owner}'s persistent cart is automatically being saved to as you add items!</p>
            </div>
          )) || (
            <div className={styles.groupForm}>
              <h4>Save Persistent Cart</h4>
              <p style={{width: '90%', color: '#03015C', margin: 'auto', fontStyle: 'italic'}}>
                You can save your cart to shop later as well as use for group ordering. Simply restore cart
                with the email and pass code you use to save with.
              </p>
              <form onSubmit={e => searchOrAddParty(e, 'create')}>
                <input
                  onChange={e => setSearchOrAdd({...searchOrAdd, [e.target.name]: e.target.value})}
                  placeholder="First Name*"
                  name="owner"
                  value={searchOrAdd.owner}
                  required
                  pattern="^[a-zA-Z0-9\-]+$"
                />
                <input
                  onChange={e => setSearchOrAdd({...searchOrAdd, [e.target.name]: e.target.value})}
                  type="email"
                  name="email"
                  value={searchOrAdd.email}
                  placeholder="Email*"
                  required
                />
                <input
                  onChange={e => setSearchOrAdd({...searchOrAdd, [e.target.name]: e.target.value})}
                  type="text"
                  name="passcode"
                  value={searchOrAdd.passcode}
                  placeholder="Pass Code*"
                  pattern="^[a-zA-Z0-9 \.-_!,]+$"
                  required
                />
                <Button
                  className={styles.button}
                  value={'Save Cart'}
                  type="primary"
                  loading={loading}
                  submit={true}
                />
              </form>
              {error && <p className={styles.error}>{error}</p>}
              {group.error.length > 0 && <p className={styles.error}>{group.error}</p>}
            </div>
          )}
        </>
      ) : (
        <div className={styles.emptyCart}>
          {(notification.success.length > 0 && (
            <div className={styles.orderSuccess}>
              <h1>Happy Days Ahead!</h1>
              <p>Thank you for your purchase.</p>
              <p>We've sent you an email receipt.</p>
              <p>
                Use the Receipt # to track{' '}
                <i
                  style={{
                    color: '#03015C',
                    textDecoration: 'underline',
                    fontWeight: 'bold',
                    cursor: 'pointer',
                  }}
                  onClick={() => props.history.push('/order-status')}>
                  order status
                </i>
                .
              </p>
              <p>When your order is shipped, you will be notified.</p>
              <p>
                Email{' '}
                <a
                  href="mailto:hello@peacocksleeves.com"
                  style={{color: '#03015C', textDecoration: 'underline'}}>
                  hello@peacocksleeves.com
                </a>{' '}
                with any questions.
              </p>
              <p>May you peacock to glory!</p>
              <img src={logo} onClick={() => props.history.push('/')} alt="peacock sleeves logo" />
            </div>
          )) || (
            <>
              <p>No Items in Cart</p>
              <p className={styles.continue} onClick={() => props.history.push('/')}>
                Continue Shopping
              </p>
              <img src={logo} onClick={() => props.history.push('/')} alt="peacock sleeves logo" />
              {(group.owner.length > 0 && (
                <div className={styles.shiptax}>
                  <p>{group.owner}'s persistent cart is automatically being saved to as you add items!</p>
                </div>
              )) || (
                <div className={styles.groupForm}>
                  <h4>{searchType === 'create' ? 'Create Persistent Cart' : 'Restore Persistent Cart'}</h4>
                  <form onSubmit={e => searchOrAddParty(e, 'search')}>
                    {searchType === 'create' && (
                      <input
                        onChange={e => setSearchOrAdd({...searchOrAdd, [e.target.name]: e.target.value})}
                        placeholder="First Name*"
                        name="owner"
                        value={searchOrAdd.owner}
                        required
                        pattern="^[a-zA-Z0-9\-]+$"
                      />
                    )}
                    <input
                      onChange={e => setSearchOrAdd({...searchOrAdd, [e.target.name]: e.target.value})}
                      type="email"
                      name="email"
                      value={searchOrAdd.email}
                      placeholder="Email*"
                      required
                    />
                    <input
                      onChange={e => setSearchOrAdd({...searchOrAdd, [e.target.name]: e.target.value})}
                      type="text"
                      name="passcode"
                      value={searchOrAdd.passcode}
                      placeholder="Pass Code*"
                      pattern="^[a-zA-Z0-9 \.-_!,]+$"
                      required
                    />
                    <Button
                      className={styles.button}
                      value={'Restore Cart'}
                      type="primary"
                      loading={loading}
                      submit={true}
                    />
                  </form>
                  {error && <p className={styles.error}>{error}</p>}
                  {group.error.length > 0 && <p className={styles.error}>{group.error}</p>}
                  {(searchType === 'create' && (
                    <p>
                      Have a Cart?<span onClick={() => setSearchType('search')}> Find it</span>
                    </p>
                  )) || (
                    <p>
                      Save A Cart?<span onClick={() => setSearchType('create')}> Save it</span>
                    </p>
                  )}
                </div>
              )}
            </>
          )}
        </div>
      )}
      {notification.error.length > 0 && <p className={styles.error}>{notification.error}</p>}
    </div>
  );
};

export default Cart;
