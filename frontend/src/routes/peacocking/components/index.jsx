import React from 'react';
import {Helmet} from 'react-helmet';

import styles from './index.scss';

const Peacocking = () => (
  <>
    <Helmet>
      <title>Peacocking</title>
      <meta
        name="description"
        content="Hide the sleeves, then show them. With Peacock Sleeves dress shirts your ready for the party and after party."
      />
    </Helmet>
    <h1 className={styles.h1}>Peacocking</h1>
    <div className={styles.peacocking}>
      <figure className={styles.peacocking__item}>
        <img
          src="https://peacock-media.s3.amazonaws.com/peacocking1.jpg"
          className={styles.peacocking__img1}
          alt="Image 1"
        />
        <p className={styles.peacocking__text1}>Looking Classy</p>
      </figure>
      <figure className={styles.peacocking__item}>
        <img
          src="https://peacock-media.s3.amazonaws.com/peacocking2.jpg"
          className={styles.peacocking__img2}
          alt="Image 2"
        />
        <p className={styles.peacocking__text2}>Getting Comfortable</p>
      </figure>
      <figure className={styles.peacocking__item}>
        <img
          src="https://peacock-media.s3.amazonaws.com/peacocking3.jpg"
          className={styles.peacocking__img3}
          alt="Image 3"
        />
        <p className={styles.peacocking__text3}>The Reveal</p>
      </figure>
      <figure className={styles.peacocking__item}>
        <img
          src="https://peacock-media.s3.amazonaws.com/peacocking4.jpg"
          className={styles.peacocking__img4}
          alt="Image 4"
        />
        <p className={styles.peacocking__text4}>Dripping</p>
      </figure>
    </div>
  </>
);
export default Peacocking;
