import React from 'react';

import styles from './index.scss';

const Gallery = () => (
  <>
    <h1 className={styles.h1}>Gallery</h1>
    <div className={styles.gallery}>
      <figure className={`${styles.gallery__item} ${styles.gallery__item1}`}>
        <img
          src="https://peacock-media.s3.amazonaws.com/gal7.jpg"
          className={styles.gallery__img}
          alt="Image 1"
        />
      </figure>
      <figure className={`${styles.gallery__item} ${styles.gallery__item2}`}>
        <img
          src="https://peacock-media.s3.amazonaws.com/gal8.jpg"
          className={styles.gallery__img}
          alt="Image 2"
        />
      </figure>
      <figure className={`${styles.gallery__item} ${styles.gallery__item3}`}>
        <img
          src="https://peacock-media.s3.amazonaws.com/gal9.jpg"
          className={styles.gallery__img}
          alt="Image 3"
        />
      </figure>
      <figure className={`${styles.gallery__item} ${styles.gallery__item4}`}>
        <img
          src="https://peacock-media.s3.amazonaws.com/cropshirt.png"
          className={styles.gallery__img}
          alt="Image 4"
        />
      </figure>
      <figure className={`${styles.gallery__item} ${styles.gallery__item5}`}>
        <img
          src="https://peacock-media.s3.amazonaws.com/gal11.jpg"
          className={styles.gallery__img}
          alt="Image 5"
        />
      </figure>
      <figure className={`${styles.gallery__item} ${styles.gallery__item6}`}>
        <img
          src="https://peacock-media.s3.amazonaws.com/gal12.jpg"
          className={styles.gallery__img}
          alt="Image 6"
        />
      </figure>
    </div>
    <div className={styles.gallery}>
      <figure className={`${styles.gallery__item} ${styles.gallery__item1}`}>
        <img
          src="https://peacock-media.s3.amazonaws.com/gal1.jpg"
          className={styles.gallery__img}
          alt="Image 1"
        />
      </figure>
      <figure className={`${styles.gallery__item} ${styles.gallery__item2}`}>
        <img
          src="https://peacock-media.s3.amazonaws.com/gal2.jpg"
          className={styles.gallery__img}
          alt="Image 2"
        />
      </figure>
      <figure className={`${styles.gallery__item} ${styles.gallery__item3}`}>
        <img
          src="https://peacock-media.s3.amazonaws.com/cropshirt3.png"
          className={styles.gallery__img}
          alt="Image 3"
        />
      </figure>
      <figure className={`${styles.gallery__item} ${styles.gallery__item4}`}>
        <img
          src="https://peacock-media.s3.amazonaws.com/customer1.jpg"
          className={styles.gallery__img}
          alt="Image 4"
        />
      </figure>
      <figure className={`${styles.gallery__item} ${styles.gallery__item5}`}>
        <img
          src="https://peacock-media.s3.amazonaws.com/wedding.jpg"
          className={styles.gallery__img}
          alt="Image 5"
        />
      </figure>
      <figure className={`${styles.gallery__item} ${styles.gallery__item6}`}>
        <img
          src="https://peacock-media.s3.amazonaws.com/customer2.jpg"
          className={styles.gallery__img}
          alt="Image 6"
        />
      </figure>
    </div>
    <div className={styles.gallery}>
      <figure className={`${styles.gallery__item} ${styles.gallery__item1}`}>
        <img
          src="https://peacock-media.s3.amazonaws.com/gal10.jpg"
          className={styles.gallery__img}
          alt="Image 1"
        />
      </figure>
      <figure className={`${styles.gallery__item} ${styles.gallery__item2}`}>
        <img
          src="https://peacock-media.s3.amazonaws.com/gal6.jpg"
          className={styles.gallery__img}
          alt="Image 2"
        />
      </figure>
      <figure className={`${styles.gallery__item} ${styles.gallery__item3}`}>
        <img
          src="https://peacock-media.s3.amazonaws.com/gal3.jpg"
          className={styles.gallery__img}
          alt="Image 3"
        />
      </figure>
      <figure className={`${styles.gallery__item} ${styles.gallery__item4}`}>
        <img
          src="https://peacock-media.s3.amazonaws.com/cropshirt2.png"
          className={styles.gallery__img}
          alt="Image 4"
        />
      </figure>
      <figure className={`${styles.gallery__item} ${styles.gallery__item5}`}>
        <img
          src="https://peacock-media.s3.amazonaws.com/brian.jpg"
          className={styles.gallery__img}
          alt="Image 5"
        />
      </figure>
      <figure className={`${styles.gallery__item} ${styles.gallery__item6}`}>
        <img
          src="https://peacock-media.s3.amazonaws.com/customer4.jpg"
          className={styles.gallery__img}
          alt="Image 6"
        />
      </figure>
    </div>
    <div className={styles.gallery}>
      <figure className={`${styles.gallery__item} ${styles.gallery__item1}`}>
        <img
          src="https://peacock-media.s3.amazonaws.com/customer5.jpg"
          className={styles.gallery__img}
          alt="Image 1"
        />
      </figure>
      <figure className={`${styles.gallery__item} ${styles.gallery__item2}`}>
        <img
          src="https://peacock-media.s3.amazonaws.com/customer6.jpg"
          className={styles.gallery__img}
          alt="Image 2"
        />
      </figure>
      <figure className={`${styles.gallery__item} ${styles.gallery__item3}`}>
        <img
          src="https://peacock-media.s3.amazonaws.com/customer7.jpg"
          className={styles.gallery__img}
          alt="Image 3"
        />
      </figure>
      <figure className={`${styles.gallery__item} ${styles.gallery__item4}`}>
        <img
          src="https://peacock-media.s3.amazonaws.com/customer8.jpg"
          className={styles.gallery__img}
          alt="Image 4"
        />
      </figure>
      <figure className={`${styles.gallery__item} ${styles.gallery__item5}`}>
        <img
          src="https://peacock-media.s3.amazonaws.com/customer9.jpg"
          className={styles.gallery__img}
          alt="Image 5"
        />
      </figure>
      {/* <figure className={`${styles.gallery__item} ${styles.gallery__item6}`}>
        <img
          src="https://peacock-media.s3.amazonaws.com/customer10.jpg"
          className={styles.gallery__img}
          alt="Image 6"
        />
      </figure> */}
    </div>
  </>
);

export default Gallery;
