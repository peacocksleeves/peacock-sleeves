import React, {useState, useEffect} from 'react';
import {Helmet} from 'react-helmet';
import {useSelector, useAction} from 'redux-zero/react';

import {addToCart} from '@redux/actions';
import Button from '@components/Button';
import ItemAddedView from '@components/ItemAddedView';
import ProductSwatch from '@components/ProductSwatch';
import Product from '@components/Product';
import SizeChart from '@components/SizeChart';

import TuxedoShirt from '@assets/imgs/tuxedoshirt.png';
import LongShirt from '@assets/imgs/longshirt.png';
import ShortShirt from '@assets/imgs/shortshirt.png';

import styles from './index.scss';

const top = document.getElementById('root');

const Build = props => {
  const [sleeves, setSleeves] = useState(null);
  const [size, setSize] = useState(null);
  const [price, setPrice] = useState(0);
  const [length, setLength] = useState(
    props.match.params.specifier === 'shirt' ? props.match.params.value.split('-')[0] : null
  );
  const [pocket, setPocket] = useState(false);
  const [back, setBack] = useState(false);
  const [added, setAdded] = useState(false);
  const [specifier] = useState(props.match.params.specifier ? props.match.params.specifier : null);
  const [category, setCategory] = useState('--');
  const [showSizeGuide, setShowSizeGuide] = useState(false);
  const categories = useSelector(({categories}) => categories);
  const products = useSelector(({products}) => products);
  const inventory = useSelector(({inventory}) => inventory);
  const addItemsToCart = useAction(addToCart);

  useEffect(() => {
    if (products.length > 0 && specifier === 'sleeve') {
      setSleeves(products.filter(product => product.imgName === props.match.params.value)[0].name);
    } else if (products.length > 0 && specifier === 'shirt' && props.match.params.value.includes('-')) {
      setSleeves(
        products.filter(product => product.imgName === props.match.params.value.split('-')[1])[0].name
      );
    }
  }, [products]);

  useEffect(() => {
    if (length) {
      top.scrollIntoView(true);
    }
  }, [sleeves]);

  useEffect(() => {
    if (size) {
      const tempInv = inventory.filter(inv => inv.product.includes(length));
      if (tempInv[0].variations[size] < 1) {
        setSize(null);
      }
    }
  }, [length]);

  const addItemToCart = e => {
    e.preventDefault();
    if (!sleeves || !length || !size)
      alert('You need to pick a shirt style, sleeve pattern, and size to add to cart.');
    else {
      addItemsToCart({
        name: sleeves,
        imgName: sleeves.replace(/[^A-Za-z0-9]/g, '').toLowerCase(),
        price:
          price > 0 ? price : parseInt(products.filter(product => product.name === sleeves)[0].price[length]),
        pocket: pocket,
        quantity: 1,
        size: size,
        length,
      });
      setAdded(true);
    }
  };

  return (
    <>
      <Helmet>
        <title>Build A Shirt</title>
        <meta
          name="description"
          content="Choose from our selection of patterns and styles. Building the perfect party sleeves with our custom product designer."
        />
      </Helmet>
      <header className={sleeves && length ? styles.buildingHeader : styles.landingHeader}>
        {(sleeves && length && (
          <>
            <h3>{sleeves}</h3>
            <Product
              type="design"
              img={sleeves.replace(/[^A-Za-z0-9]/g, '').toLowerCase()}
              shirt={length}
              back={back}
              pocket={pocket}
            />
          </>
        )) || (
          <>
            <div
              style={{
                backgroundImage: `url(https://peacock-media.s3.amazonaws.com/${
                  length === 'short'
                    ? 'buildshortNew.jpg'
                    : length === 'tuxedo'
                    ? 'brian.jpg'
                    : 'buildlongNew.jpg'
                })`,
              }}
              className={styles.headerImg}
            />
            <div className={styles.headerText}>
              <h3>Build Your</h3>
              <h1>Party Sleeves</h1>
            </div>
          </>
        )}
      </header>
      <div className={styles.building}>
        <section className={styles.shirtSection}>
          <h2>Shirt Style</h2>
          <div className={styles.shirt} onClick={() => setLength('long')}>
            <img className={length === 'long' ? styles.active : {}} src={LongShirt} alt="long sleeve shirt" />
            <p>Long Sleeve</p>
          </div>
          <div className={styles.shirt} onClick={() => setLength('short')}>
            <img
              className={length === 'short' ? styles.active : {}}
              src={ShortShirt}
              alt="short sleeve shirt"
            />
            <p>Short Sleeve</p>
          </div>
          <div className={styles.shirt} onClick={() => setLength('tuxedo')}>
            <img
              className={length === 'tuxedo' ? styles.active : {}}
              src={TuxedoShirt}
              alt="tuxedo sleeve shirt"
            />
            <p>Tuxedo Sleeve</p>
          </div>
          {/* {(length === 'long' || length === 'tuxedo') && (
            <label className={styles.checkSelect}>
              <input onChange={() => setPocket(!pocket)} type="checkbox" checked={pocket} />
              <span className={styles.checkmark}></span>
              Add Pocket Square? $5.00
            </label>
          )} */}
        </section>
        <section className={styles.sizeSection}>
          <h2>
            Shirt Size{' '}
            <span className={styles.tooltip} onClick={() => setShowSizeGuide(true)}>
              Size Guide &#9432;
            </span>
          </h2>
          <div
            className={
              length === 'tuxedo' && inventory[0].variations.SM < 1
                ? `${styles.sizeDiv} ${styles.outOfStock}`
                : (length === 'long' || length === 'short') && inventory[1].variations.SM < 1
                ? `${styles.sizeDiv} ${styles.outOfStock}`
                : size === 'SM'
                ? `${styles.sizeDiv} ${styles.active}`
                : styles.sizeDiv
            }
            onClick={
              length === 'tuxedo' && inventory[0].variations.SM < 1
                ? () => alert('Out Of Stock')
                : (length === 'long' || length === 'short') && inventory[1].variations.SM < 1
                ? () => alert('Out Of Stock')
                : () => setSize('SM')
            }>
            SM
          </div>
          <div
            className={
              length === 'tuxedo' && inventory[0].variations.MD < 1
                ? `${styles.sizeDiv} ${styles.outOfStock}`
                : (length === 'long' || length === 'short') && inventory[1].variations.MD < 1
                ? `${styles.sizeDiv} ${styles.outOfStock}`
                : size === 'MD'
                ? `${styles.sizeDiv} ${styles.active}`
                : styles.sizeDiv
            }
            onClick={
              length === 'tuxedo' && inventory[0].variations.MD < 1
                ? () => alert('Out Of Stock')
                : (length === 'long' || length === 'short') && inventory[1].variations.MD < 1
                ? () => alert('Out Of Stock')
                : () => setSize('MD')
            }>
            MD
          </div>
          <div
            className={
              length === 'tuxedo' && inventory[0].variations.LG < 1
                ? `${styles.sizeDiv} ${styles.outOfStock}`
                : (length === 'long' || length === 'short') && inventory[1].variations.LG < 1
                ? `${styles.sizeDiv} ${styles.outOfStock}`
                : size === 'LG'
                ? `${styles.sizeDiv} ${styles.active}`
                : styles.sizeDiv
            }
            onClick={
              length === 'tuxedo' && inventory[0].variations.LG < 1
                ? () => alert('Out Of Stock')
                : (length === 'long' || length === 'short') && inventory[1].variations.LG < 1
                ? () => alert('Out Of Stock')
                : () => setSize('LG')
            }>
            LG
          </div>
          <div
            className={
              length === 'tuxedo' && inventory[0].variations.XL < 1
                ? `${styles.sizeDiv} ${styles.outOfStock}`
                : (length === 'long' || length === 'short') && inventory[1].variations.XL < 1
                ? `${styles.sizeDiv} ${styles.outOfStock}`
                : size === 'XL'
                ? `${styles.sizeDiv} ${styles.active}`
                : styles.sizeDiv
            }
            onClick={
              length === 'tuxedo' && inventory[0].variations.XL < 1
                ? () => alert('Out Of Stock')
                : (length === 'long' || length === 'short') && inventory[1].variations.XL < 1
                ? () => alert('Out Of Stock')
                : () => setSize('XL')
            }>
            XL
          </div>
          <div
            className={
              length === 'tuxedo' && inventory[0].variations.XXL < 1
                ? `${styles.sizeDiv} ${styles.outOfStock}`
                : (length === 'long' || length === 'short') && inventory[1].variations.XXL < 1
                ? `${styles.sizeDiv} ${styles.outOfStock}`
                : size === 'XXL'
                ? `${styles.sizeDiv} ${styles.active}`
                : styles.sizeDiv
            }
            onClick={
              length === 'tuxedo' && inventory[0].variations.XXL < 1
                ? () => alert('Out Of Stock')
                : (length === 'long' || length === 'short') && inventory[1].variations.XXL < 1
                ? () => alert('Out Of Stock')
                : () => setSize('XXL')
            }>
            XXL
          </div>
        </section>
        <section className={styles.patternSection}>
          <h2>
            Sleeve Pattern
            <span style={{verticalAlign: 'sub'}}>
              <div style={{width: '120px', marginLeft: '10px', display: 'inline-block'}}>
                <h3>Categories</h3>
                <select value={category} onChange={e => setCategory(e.target.value)}>
                  <option value={category}>{category}</option>
                  <option value="All">All</option>
                  {categories.map(cat => (
                    <option key={cat} value={cat}>
                      {cat}
                    </option>
                  ))}
                </select>
              </div>
            </span>
          </h2>
          {/* {(length === 'long' || length === 'tuxedo') && (
            <label className={styles.checkSelect}>
              <input onChange={() => setBack(!back)} type="checkbox" checked={back} />
              <span className={styles.checkmark}></span>
              Pattern on Back? $15.00
            </label>
          )} */}
          <div className={styles.patternContainer}>
            {products
              .filter(product => {
                if (
                  (category != '--' && category != 'All') ||
                  (specifier === 'category' && category === '--')
                ) {
                  if (product.category === category) {
                    return product;
                  } else if (
                    category === '--' &&
                    product.category.toLowerCase().replace(' ', '') === props.match.params.value
                  ) {
                    return product;
                  }
                } else {
                  return product;
                }
              })
              .filter(product => {
                if (specifier === 'sleeve' && category === '--') {
                  if (product.imgName === props.match.params.value) {
                    return product;
                  }
                } else if (
                  specifier === 'shirt' &&
                  props.match.params.value.includes('-') &&
                  category === '--'
                ) {
                  if (product.imgName === props.match.params.value.split('-')[1]) {
                    return product;
                  }
                } else {
                  return product;
                }
              })
              .map((product, i) => (
                <div
                  key={i}
                  className={styles.swatchContainer}
                  onClick={() => {
                    setSleeves(product.name);
                    setPrice(parseInt(product.price[length]));
                  }}>
                  <ProductSwatch
                    active={false}
                    imgID={styles.swatchImgID}
                    divID={sleeves === product.name ? styles.swatchesDivActiveID : styles.swatchesDivID}
                    product={product}
                    setSleeves={() => null}
                    setSelectedProduct={() => null}
                  />
                  <p>{product.name}</p>
                  {length && <p className={styles.price}>${product.price[length]}</p>}
                </div>
              ))}
          </div>
        </section>
      </div>
      <footer className={styles.footer}>
        <div className={styles.footerContent}>
          <h4>Your Shirt</h4>
          {(sleeves && length && (
            <p>
              {sleeves} - {length} sleeves{size ? ', size ' + size : ''}
            </p>
          )) || <p>Select a shirt style to get started</p>}
        </div>
      </footer>
      <div onClick={addItemToCart}>
        <Button className={styles.button} text="Add To Cart" type="primary" />
      </div>
      {added && (
        <div className={styles.addedSuccess}>
          <ItemAddedView
            imgName={sleeves.replace(/[^A-Za-z0-9]/g, '').toLowerCase()}
            length={length}
            back={back}
            pocket={pocket}
            itemName={sleeves}
            size={size}
            price={
              price > 0
                ? price
                : parseInt(products.filter(product => product.name === sleeves)[0].price[length])
            }>
            <div onClick={() => props.history.push('/cart')} className={styles.cartAddButton}>
              See Cart
            </div>
            <div
              onClick={() => {
                setAdded(false);
                setSize(null);
                setPrice(0);
                setLength(specifier === 'shirt' ? props.match.params.value.split('-')[0] : null);
                setBack(false);
                setPocket(false);
                setSleeves(null);
                setCategory('--');
              }}
              // onClick={() => props.history.push('/')}
              className={styles.cartAddButton}>
              Keep Shopping
            </div>
          </ItemAddedView>
        </div>
      )}
      {showSizeGuide && <SizeChart modal={true} setShowSizeGuide={setShowSizeGuide} />}
    </>
  );
};

export default Build;
