import React from 'react';
import {useIndexedDB} from 'react-indexed-db';

const DesignPattern = () => {
  const indexedb = useIndexedDB('peacock');

  const fileUpload = e => {
    e.preventDefault();
    e.persist();
    const file = e.target.files[0];
    console.log(file);
    // if (file.type === 'image/jpeg') {
    let reader = new FileReader();
    reader.onloadend = function() {
      console.log(reader);
      // setFormData({...formData, img: reader.result});
    };
    reader.readAsDataURL(file);
    // } else {
    console.log('Image has to be a .jpg dumb-dumb. Try again.');
    // }
  };

  return (
    <>
      <h1 style={{textAlign: 'center', marginTop: '8em', fontSize: '30px'}}>DesignPattern</h1>
      <div>{JSON.stringify(indexedb)}</div>
      <input onChange={fileUpload} type="file" name="file" id="file" required />
    </>
  );
};
export default DesignPattern;
