import React from 'react';

import styles from './index.scss';

const Returns = props => {
  return (
    <div className={styles.returns}>
      <h1>Returns</h1>
      <p>To return a shirt these requirements must be met:</p>
      <ul>
        <li>Shirt must be postmarked within 60 days of it arriving to our facilities</li>
        <li>Shirt must be unworn, unwashed, and unaltered</li>
        <li>Shirt must have all tags, including packaging hang-tags attached</li>
        <li>The shirt you received is the wrong size, style, or pattern from that you ordered</li>
      </ul>
      <p>
        To initiate a return please use our{' '}
        <i
          style={{textDecoration: 'underline', fontWeight: 'bold', cursor: 'pointer'}}
          onClick={() => props.setShowContact(true)}>
          contact form
        </i>{' '}
        to send us an email. The email should include your order number found in the receipt email sent to you
        and the reason for return.
      </p>
    </div>
  );
};

export default Returns;
