import React from 'react';

import styles from './index.scss';

const Manifesto = () => (
  <main className={styles.manifesto}>
    <h1>Manifesto</h1>
    <p>
      We are two brothers and a best friend with a lot of party experience. Our goal is to simply bring joyful
      vibes to you and those around you!
    </p>
    <p>
      Peacock Sleeves is a menswear clothing company that creates exclusive one-of-a-kind custom short, dress,
      and tuxedo shirts, guaranteed to steal the center of attention.
    </p>
    <p>
      Traveling to weddings and events around the country comes with great responsibility! It's up to you to
      celebrate to the fullest.
    </p>
    <p>Wear your party sleeves and feel ready for any event.</p>
    <p>CLASSY IN THE FRONT PARTY ON THE SIDES TYPE OF VIBES.</p>
    <div className={styles.peacocking}>
      <figure className={styles.peacocking__item}>
        <img
          src="https://peacock-media.s3.amazonaws.com/peacocking1.jpg"
          className={styles.peacocking__img1}
          alt="Image 1"
        />
        <p className={styles.peacocking__text1}>Looking Classy</p>
      </figure>
      <figure className={styles.peacocking__item}>
        <img
          src="https://peacock-media.s3.amazonaws.com/peacocking2.jpg"
          className={styles.peacocking__img2}
          alt="Image 2"
        />
        <p className={styles.peacocking__text2}>Getting Comfortable</p>
      </figure>
      <figure className={styles.peacocking__item}>
        <img
          src="https://peacock-media.s3.amazonaws.com/peacocking3.jpg"
          className={styles.peacocking__img3}
          alt="Image 3"
        />
        <p className={styles.peacocking__text3}>The Reveal</p>
      </figure>
      <figure className={styles.peacocking__item}>
        <img
          src="https://peacock-media.s3.amazonaws.com/peacocking4.jpg"
          className={styles.peacocking__img4}
          alt="Image 4"
        />
        <p className={styles.peacocking__text4}>Dripping</p>
      </figure>
    </div>
  </main>
);
export default Manifesto;
