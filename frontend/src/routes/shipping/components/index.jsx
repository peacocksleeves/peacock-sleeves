import React from 'react';

import styles from './index.scss';

const Shipping = () => (
  <div className={styles.shipping}>
    <h1>Shipping</h1>
    <p>
      All purchases automatically receive free standard shipping on every order. Orders will be shipped
      between 14 and 21 days after we process the order. You'll be notified when we do process the order and
      ship your shirt.
    </p>
    <h2>Details</h2>
    <ul>
      <li>We process orders Monday-Friday (excluding holidays).</li>
      <li>If you place your order after the cutoff times, we’ll process the order the next business day.</li>
      <li>We do not deliver on holidays.</li>
      <li>
        We do not offer <u>international shipping</u> and are unable to ship to US territories, PO boxes,
        re-shippers, or package forwarding services.
      </li>
    </ul>
    <h2>Tracking</h2>
    <p>
      When you make an order you will receive an email receipt. You can use your <i>receipt number</i> to
      track the status of your order on the{' '}
      <a style={{textDecoration: 'underline', fontWeight: 'bold', cursor: 'pointer'}} href="/order-status">
        order status
      </a>{' '}
      page and see if it has been shipped yet. When you order has been shipped you will receive an automated
      email informing you of shipping details including links to easily track your shipment.
    </p>
  </div>
);

export default Shipping;
