import React, {useState, useEffect} from 'react';
import {NavLink, Route, Switch, useLocation} from 'react-router-dom';
import {useAction, useSelector} from 'redux-zero/react';

import {getProducts, getMemos, getInventory, exitGroup} from '@redux/actions';
// import {exitMemo} from '@redux/actions';

import Nav from '@components/Nav';
import Footer from '@components/Footer';

import CartIcon from '@assets/icons/shoppingcart.svg';

import styles from './main.scss';

import Cart from '@routes/cart/components';
import Home from '@routes/home/components';
import Build from '@routes/build/components';
const Manifesto = React.lazy(() =>
  import(/* webpackChunkName: "manifesto" */ '@routes/manifesto/components')
);
const FAQ = React.lazy(() => import(/* webpackChunkName: "faq" */ '@routes/faq/components'));
const OrderStatus = React.lazy(() =>
  import(/* webpackChunkName: "orderstatus" */ '@routes/orderStatus/components')
);
const Patterns = React.lazy(() => import(/* webpackChunkName: "patterns" */ '@routes/patterns/components'));
const Products = React.lazy(() => import(/* webpackChunkName: "products" */ '@routes/products/components'));
const Privacy = React.lazy(() => import(/* webpackChunkName: "privacy" */ '@routes/privacy/components'));
const Returns = React.lazy(() => import(/* webpackChunkName: "returns" */ '@routes/returns/components'));
const Shipping = React.lazy(() => import(/* webpackChunkName: "shipping" */ '@routes/shipping/components'));
const SingleCustom = React.lazy(() =>
  import(/* webpackChunkName: "singleCustom" */ '@routes/singleCustom/components')
);
const Gallery = React.lazy(() => import(/* webpackChunkName: "gallery" */ '@routes/gallery/components'));
const GroupOrder = React.lazy(() =>
  import(/* webpackChunkName: "grouporder" */ '@routes/groupOrder/components')
);
const NotFound = React.lazy(() => import(/* webpackChunkName: "notfound" */ '@routes/notFound/components'));

const top = document.getElementById('root');

const Main = () => {
  const location = useLocation();
  const [showContact, setShowContact] = useState(false);
  const fetchProducts = useAction(getProducts);
  const memos = useSelector(({memos}) => memos);
  const group = useSelector(({group}) => group);
  const fetchMemos = useAction(getMemos);
  const fetchInventory = useAction(getInventory);
  const leaveMemo = useAction(exitGroup);
  const leaveGroup = useAction(exitGroup);

  useEffect(() => {
    fetchProducts();
    fetchMemos();
    fetchInventory();
  }, []);
  useEffect(() => {
    top.scrollIntoView(true);
  }, [location]);

  return (
    <>
      {location.pathname === '/' && group.owner.length === 0 && (
        <div className={styles.memos}>
          {memos
            .filter(memo => memo.status != 'INACTIVE')
            .map(memo => (
              <p key={memo.id} className={styles.memo}>
                {memo.memo}
              </p>
            ))}
          <span onClick={leaveMemo}>x</span>
        </div>
      )}
      {group.owner.length > 0 && (
        <div className={styles.memos}>
          <p key={group.id} className={styles.groupMemo}>
            {group.owner}'s Persistent Cart is Active!
          </p>
          <span onClick={leaveGroup}>x</span>
        </div>
      )}
      <Nav location={location.pathname.includes('build') ? 'build' : 'landing'} />
      <React.Suspense fallback={<div> </div>}>
        <Switch>
          <Route exact={true} path="/" component={Home} />
          <Route exact={true} path="/build" component={Build} />
          <Route exact={true} path="/build/:specifier/:value" component={Build} />
          <Route exact={true} path="/build/custom-order" component={SingleCustom} />
          <Route exact={true} path="/cart" component={Cart} />
          <Route exact={false} path="/cart/:status" component={Cart} />
          <Route exact={true} path="/gallery" component={Gallery} />
          <Route exact={true} path="/manifesto" component={Manifesto} />
          <Route exact={true} path="/group-order" component={GroupOrder} />
          <Route
            exact={true}
            path="/faq"
            render={rprops => <FAQ {...rprops} setShowContact={setShowContact} />}
          />
          <Route exact={true} path="/order-status" component={OrderStatus} />
          <Route exact={true} path="/patterns" component={Patterns} />
          <Route exact={true} path="/short-sleeves" component={Products} />
          <Route exact={true} path="/long-sleeves" component={Products} />
          <Route exact={true} path="/tuxedo-sleeves" component={Products} />
          <Route exact={true} path="/privacy" component={Privacy} />
          <Route
            exact={true}
            path="/returns"
            render={rprops => <Returns {...rprops} setShowContact={setShowContact} />}
          />
          <Route exact={true} path="/shipping" component={Shipping} />
          <Route exact={false} component={NotFound} />
        </Switch>
      </React.Suspense>
      {!location.pathname.includes('build') && (
        <NavLink to={'/cart'}>
          <CartIcon className={styles.cartButton} />
        </NavLink>
      )}
      <Footer
        showContact={showContact}
        setShowContact={setShowContact}
        location={location.pathname.includes('build') ? 'build' : 'landing'}
      />
    </>
  );
};

export default Main;
