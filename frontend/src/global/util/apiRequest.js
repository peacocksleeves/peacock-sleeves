const endpoint = `https://${process.env.ENV === 'prod' ? 'api' : 'dev-api'}.peacocksleeves.com/router`;

export default async function apiRequest(method, options, path) {
  const headers = {
    'Content-Type': 'application/json',
    Accept: '*/*',
    ...options.headers,
  };

  const parsedBody = typeof options.body !== 'string' ? JSON.stringify(options.body) : options.body;

  const request = {
    body: parsedBody,
    headers,
    method,
  };

  const response = await fetch(`${endpoint}/${path}`, request);
  const data = await response.json();
  if (response.status > 399) throw new Error(data.message);
  return {data};
}
