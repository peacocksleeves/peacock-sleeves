// import apiRequest from './apiRequest';

// describe('apiRequest', () => {
//   it('Should create a simple GET request', async () => {
//     const mockResponse = {hello: 'test'};
//     fetch.once(JSON.stringify(mockResponse));

//     const response = await apiRequest('GET', 'http://localhost:3003/api', {});

//     const expectedUrl = 'http://localhost:3003/api';
//     const expectedRequest = {
//       headers: {'Content-Type': 'application/json'},
//       method: 'GET',
//     };

//     expect(fetch).toHaveBeenCalledWith(expectedUrl, expectedRequest);
//     expect(response).toEqual({data: mockResponse});
//   });

//   it('Should create a POST request with a body', async () => {
//     const mockResponse = {hello: 'test'};
//     fetch.once(JSON.stringify(mockResponse));

//     const mockBody = {test: 'one', testTwo: 2};
//     const response = await apiRequest('POST', 'http://localhost:3003/api', {body: mockBody});

//     const expectedUrl = 'http://localhost:3003/api';
//     const expectedRequest = {
//       body: JSON.stringify(mockBody),
//       headers: {'Content-Type': 'application/json'},
//       method: 'POST',
//     };

//     expect(fetch).toHaveBeenCalledWith(expectedUrl, expectedRequest);
//     expect(response).toEqual({data: mockResponse});
//   });

//   it('Should create a POST request with a pre-stringified body', async () => {
//     const mockResponse = {hello: 'test'};
//     fetch.once(JSON.stringify(mockResponse));

//     const mockBody = JSON.stringify({test: 'one', testTwo: 2});
//     const response = await apiRequest('POST', 'http://localhost:3003/api', {body: mockBody});

//     const expectedUrl = 'http://localhost:3003/api';
//     const expectedRequest = {
//       body: mockBody,
//       headers: {'Content-Type': 'application/json'},
//       method: 'POST',
//     };

//     expect(fetch).toHaveBeenCalledWith(expectedUrl, expectedRequest);
//     expect(response).toEqual({data: mockResponse});
//   });

//   it('Should handle a failed request', async () => {
//     const mockResponse = {code: 404, message: 'Not Found'};
//     fetch.once(JSON.stringify(mockResponse), {status: 404});

//     const response = await apiRequest('GET', 'http://localhost:3003/api', {});

//     const expectedUrl = 'http://localhost:3003/api';
//     const expectedRequest = {
//       headers: {'Content-Type': 'application/json'},
//       method: 'GET',
//     };

//     expect(fetch).toHaveBeenCalledWith(expectedUrl, expectedRequest);
//     expect(response).toEqual({error: mockResponse});
//   });
// });
