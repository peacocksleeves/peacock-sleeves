export const Indexedb = {
  name: 'peacock',
  version: 1,
  objectStoresMeta: [
    {
      store: 'patterns',
      storeConfig: {keyPath: 'id', autoIncrement: true},
      storeSchema: [
        {name: 'name', keypath: 'name', options: {unique: false}},
        {name: 'image', keypath: 'image', options: {unique: false}},
      ],
    },
  ],
};
