import createStore from 'redux-zero';

const initialState = {
  memos: [],
  products: [],
  inventory: [{variations: ''}, {variations: ''}],
  singleProduct: [],
  cart: JSON.parse(sessionStorage.getItem('Cart')) || [],
  categories: [],
  price: JSON.parse(sessionStorage.getItem('Price')) || 0,
  group: JSON.parse(sessionStorage.getItem('Group')) || {email: '', owner: '', id: 0, error: ''},
};

const store = createStore(initialState);

export default store;
