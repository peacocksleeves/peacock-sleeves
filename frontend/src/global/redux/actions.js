import apiRequest from '@util/apiRequest';

/////////////////////
// PRODUCTS
////////////////////
export const getProducts = async () => {
  try {
    const data = await apiRequest('GET', {headers: {}}, 'getProducts');
    const singleProduct = data.data.filter(product => product.id === (process.env.ENV === 'prod' ? 38 : 55));
    singleProduct[0].price = JSON.parse(singleProduct[0].price);
    const products = data.data
      .filter(product => product.status === 'ACTIVE')
      .sort((a, b) => {
        if (a.sales > b.sales) return -1;
        if (a.sales < b.sales) return 1;
      })
      .map(product => ({...product, price: JSON.parse(product.price)}));
    let categories = products
      .map(product => product.category)
      .map(value => ({value, sort: Math.random()}))
      .sort((a, b) => a.sort - b.sort)
      .map(({value}) => value);
    categories = [...new Set(categories)];
    return {products, categories, singleProduct};
  } catch (err) {
    return;
  }
};

/////////////////////
// GROUPS
////////////////////
export const createGroup = async (state, party) => {
  party.cart = JSON.stringify([]);
  party.price = 0;
  try {
    const data = await apiRequest('POST', {headers: {}, body: party}, 'createGroup');
    sessionStorage.setItem(
      'Group',
      JSON.stringify({
        email: party.email,
        owner: party.owner,
        id: parseInt(data.data.data),
        passcode: party.passcode,
        error: '',
      })
    );
    return {
      group: {
        email: party.email,
        owner: party.owner,
        id: parseInt(data.data.data),
        passcode: party.passcode,
        error: '',
      },
    };
  } catch (err) {
    if (err.message.includes('Duplicate')) {
      return {group: {...state.group, error: 'Email already in use'}};
    } else {
      return {group: {...state.group, error: 'An error occurred please try again'}};
    }
  }
};

export const getGroup = async (state, party) => {
  try {
    const data = await apiRequest('POST', {headers: {}, body: party}, 'getGroup');
    if (data.data.length === 0) {
      return {group: {...state.group, error: 'Group not found'}};
    } else if (data.data[0].passcode !== party.passcode) {
      return {group: {...state.group, error: 'Incorrect passcode'}};
    } else {
      data.data[0].cart = JSON.parse(data.data[0].cart);
      sessionStorage.setItem(
        'Group',
        JSON.stringify({
          email: party.email,
          owner: data.data[0].owner,
          id: data.data[0].id,
          passcode: data.data[0].passcode,
          error: '',
        })
      );
      sessionStorage.setItem('Price', JSON.stringify(parseInt(data.data[0].price)));
      sessionStorage.setItem('Cart', JSON.stringify([...data.data[0].cart]));
      return {
        group: {
          email: party.email,
          owner: data.data[0].owner,
          id: data.data[0].id,
          passcode: data.data[0].passcode,
          error: '',
        },
        cart: [...data.data[0].cart],
        price: parseInt(data.data[0].price),
      };
    }
  } catch (err) {
    return {group: {...state.group, error: 'An error occurred please try again'}};
  }
};

export const exitGroup = () => {
  sessionStorage.setItem('Cart', JSON.stringify([]));
  sessionStorage.setItem('Price', JSON.stringify(0));
  sessionStorage.removeItem('Group');
  return {cart: [], group: {email: '', owner: '', id: 0, passcode: '', error: ''}, price: 0};
};

/////////////////////
// MEMOS
////////////////////
export const getMemos = async () => {
  try {
    const data = await apiRequest('GET', {headers: {}}, 'getMemos');
    return {memos: data.data};
  } catch (err) {
    return;
  }
};

/////////////////////
// INVENTORY
////////////////////
export const getInventory = async () => {
  try {
    const data = await apiRequest('GET', {headers: {}}, 'getInventory');
    const inventory = data.data.map(inv => ({...inv, variations: JSON.parse(inv.variations)}));
    return {inventory, loading: false};
  } catch (err) {
    return {loading: false};
  }
};

/////////////////////
// CART
////////////////////
export const addToCart = async (state, item) => {
  const pocket = item.pocket ? 5 : 0;
  const price = state.price + parseInt(item.price) + pocket;
  try {
    if (state.group.id > 0)
      await apiRequest(
        'PUT',
        {headers: {}, body: {cart: JSON.stringify([...state.cart, item]), price}},
        `updateGroup/${state.group.id}`
      );
  } catch (err) {
    console.log('Error updating group cart', err);
  } finally {
    sessionStorage.setItem('Price', JSON.stringify(price));
    sessionStorage.setItem('Cart', JSON.stringify([...state.cart, item]));
    return {cart: [...state.cart, item], price};
  }
};

export const changeQuantity = async (state, index, math) => {
  let newCart;
  const pocket = state.cart[index].pocket ? 5 : 0;
  const newPrice =
    math === 'sub'
      ? state.price - parseInt(state.cart[index].price) - pocket
      : state.price + parseInt(state.cart[index].price) + pocket;
  if (math === 'sub' && state.cart[index].quantity <= 1) {
    newCart = state.cart.filter((_, i) => i !== index);
  } else {
    newCart = state.cart.map((item, i) => {
      if (i === index) {
        item.quantity = math === 'add' ? item.quantity + 1 : item.quantity - 1;
      }
      return item;
    });
  }
  try {
    if (state.group.id > 0)
      await apiRequest(
        'PUT',
        {headers: {}, body: {cart: JSON.stringify(newCart), price: newPrice}},
        `updateGroup/${state.group.id}`
      );
  } catch (err) {
    console.log('Error updating group cart', err);
  } finally {
    sessionStorage.setItem('Cart', JSON.stringify(newCart));
    sessionStorage.setItem('Price', JSON.stringify(newPrice));
    return {cart: newCart, price: newPrice};
  }
};

export const resetCart = () => {
  sessionStorage.setItem('Cart', JSON.stringify([]));
  sessionStorage.setItem('Price', JSON.stringify(0));
  return {cart: [], group: {email: '', owner: '', id: 0, error: ''}, price: 0};
};
