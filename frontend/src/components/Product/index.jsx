import React from 'react';

import LongShirt from '@assets/imgs/longshirt.png';
import LongShade from '@assets/imgs/longshade.png';
import LongSleeves from '@assets/imgs/longsleeves.png';
import ShortShirt from '@assets/imgs/shortshirt.png';
import ShortShade from '@assets/imgs/shortshade.png';
import ShortSleeves from '@assets/imgs/shortsleeves.png';
import TuxedoShirt from '@assets/imgs/tuxedoshirt.png';
import TuxedoShade from '@assets/imgs/tuxedoshade.png';
import TuxedoSleeves from '@assets/imgs/tuxedosleeves.png';
import PocketShade from '@assets/imgs/pocketshade.png';
import Pocket from '@assets/imgs/pocket.png';

import styles from './index.scss';

const Product = props => {
  const mask =
    props.shirt === 'short'
      ? `url(${ShortSleeves})`
      : props.shirt === 'long'
      ? `url(${LongSleeves})`
      : `url(${TuxedoSleeves})`;
  const pocketMask = `url(${Pocket})`;
  return (
    <div className={props.type === 'design' ? styles.productDesigner : styles.product}>
      <img
        className={styles.blankShirt}
        src={props.shirt === 'short' ? ShortShirt : props.shirt === 'long' ? LongShirt : TuxedoShirt}
        alt={`${props.shirt} sleeve shirt`}
      />
      <div
        className={styles.clippedSleeves}
        style={{
          background: `url("https://peacock-swatches-${process.env.ENV}.s3.amazonaws.com/${
            props.img
          }large.jpg") ${props.img === 'merica' ? '50%' : '0%'} 0% / ${
            props.img === 'merica' && props.shirt === 'short'
              ? '70%'
              : props.img === 'merica' && props.shirt === 'tuxedo'
              ? '63%'
              : '60%'
          }`,
          WebkitMaskImage: mask,
          maskImage: mask,
          WebkitMaskSize: 'contain',
          maskSize: 'contain',
        }}
      />
      <img
        className={styles.sleeveShade}
        src={props.shirt === 'short' ? ShortShade : props.shirt === 'long' ? LongShade : TuxedoShade}
        alt="sleeve shade"
      />
      {props.shirt === 'short' && (
        <>
          <div
            className={styles.clippedPocket}
            style={{
              background: `url("https://peacock-swatches-${process.env.ENV}.s3.amazonaws.com/${props.img}large.jpg") 0% 0% / 50%`,
              WebkitMaskImage: pocketMask,
              maskImage: pocketMask,
              WebkitMaskSize: 'contain',
              maskSize: 'contain',
            }}
          />
          <img className={styles.pocketShade} src={PocketShade} alt="pocket shade" />
        </>
      )}
    </div>
  );
};

export default Product;
