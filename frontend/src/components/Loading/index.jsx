import React from 'react';
import styles from './index.scss';

const Loading = () => (
  <div className={styles.spinner}>
    <div className={styles.bounce1}></div>
    <div className={styles.bounce2}></div>
  </div>
);

export default Loading;
