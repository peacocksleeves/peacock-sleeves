import React from 'react';

import styles from './index.scss';

const ProductSwatch = props => (
  <>
    <div className={styles.swatches} id={props.divID}>
      <div
        className={styles.swatch}
        onClick={() => {
          props.setSleeves(props.product.imgName), props.setSelectedProduct(props.product);
        }}>
        <img
          id={props.imgID}
          src={`https://peacock-swatches-${process.env.ENV}.s3.amazonaws.com/${props.product.imgName}medium.jpg`}
          alt="pattern swatch"
        />
      </div>
    </div>
  </>
);

export default ProductSwatch;
