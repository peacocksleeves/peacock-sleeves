import React from 'react';
import {Link} from 'react-router-dom';

import Product from '@components/Product';

import styles from './index.scss';

const ProductGrid = props => (
  <>
    <section className={`${styles.productGrid} ${styles[props.gridType]}`}>
      {props.products.map((product, i) => (
        <div key={i} className={props.gridType === 'scrollGrid' ? styles.scrollDiv : ''}>
          <Link
            to={`/build/shirt/${
              props.productType
                ? props.productType + '-'
                : i % 3 === 0
                ? 'long-'
                : i % 2 === 0
                ? 'short-'
                : 'tuxedo-'
            }${product.imgName}`}>
            <Product
              index={product.name.replace(' ', '-')}
              img={product.imgName}
              type="grid"
              shirt={
                props.gridType === 'triGrid' || props.gridType === 'duoGrid'
                  ? props.productType
                  : i % 3 === 0
                  ? 'long'
                  : i % 2 === 0
                  ? 'short'
                  : 'tuxedo'
              }
            />
            <p className={styles.name}>
              {product.name}
              {!props.productType
                ? ''
                : props.productType === 'short'
                ? ` - $${parseInt(product.price.short)}`
                : props.productType === 'long'
                ? ` - $${parseInt(product.price.long)}`
                : ` - $${parseInt(product.price.tuxedo)}`}
            </p>
            <button className={styles.view}>Check it Out</button>
          </Link>
        </div>
      ))}
    </section>
  </>
);

export default ProductGrid;
