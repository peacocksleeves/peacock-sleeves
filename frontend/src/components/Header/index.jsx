import React from 'react';
import PropTypes from 'prop-types';
import styles from './index.scss';

const Header = props => (
  <header className={styles.headerComponent}>
    <div>{props.children}</div>
  </header>
);

Header.propTypes = {
  children: PropTypes.element.isRequired,
};

export default Header;
