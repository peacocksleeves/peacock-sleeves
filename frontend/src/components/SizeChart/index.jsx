import React from 'react';

import sizeGuide from '@assets/imgs/sizeguide.png';

import styles from './index.scss';

const SizeChart = props => (
  <div
    className={props.modal ? styles.modal : {}}
    onClick={props.modal ? () => props.setShowSizeGuide(false) : () => ''}>
    <div className={props.modal ? `${styles.sizeChart} ${styles.sizeChartModal}` : styles.sizeChart}>
      {props.modal && (
        <span className={styles.close} onClick={() => props.setShowSizeGuide(false)}>
          x
        </span>
      )}
      <p>Size up if in-between sizes.</p>
      <p>
        All shirts have an <b>athletic fit</b>.
      </p>
      <p>
        All measurements are <b>approximate</b>.
      </p>
      <img src={sizeGuide} alt="size guide" />
      <div className={styles.tableContainer}>
        <table className={styles.table}>
          <thead>
            <tr>
              <th></th>
              <th>Small</th>
              <th>Medium</th>
              <th>Large</th>
              <th>XLarge</th>
              <th>XXLarge</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Neck &#9398;</td>
              <td>14.5"</td>
              <td>15.5"</td>
              <td>16.5"</td>
              <td>17.5"</td>
              <td>19"</td>
            </tr>
            <tr>
              <td>Shoulder &#9399;</td>
              <td>17.5"</td>
              <td>18.5"</td>
              <td>19.5"</td>
              <td>20.5"</td>
              <td>22"</td>
            </tr>
            <tr>
              <td>Chest &#9400;</td>
              <td>38"</td>
              <td>42"</td>
              <td>46"</td>
              <td>50"</td>
              <td>53"</td>
            </tr>
            <tr>
              <td>Waist &#9401;</td>
              <td>35.5"</td>
              <td>39.5"</td>
              <td>43.5"</td>
              <td>47.5"</td>
              <td>51.5"</td>
            </tr>
            <tr>
              <td>Sleeve &#9402;</td>
              <td>24"</td>
              <td>25"</td>
              <td>26"</td>
              <td>27"</td>
              <td>28.5"</td>
            </tr>
            <tr>
              <td>Length &#9403;</td>
              <td>29"</td>
              <td>30.5"</td>
              <td>31"</td>
              <td>33"</td>
              <td>34.5"</td>
            </tr>
            <tr>
              <td>HT/WT</td>
              <td>5'7/145</td>
              <td>5'9/160</td>
              <td>6'0/185</td>
              <td>6'2/210</td>
              <td>6'4/230</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
);

export default SizeChart;
