import React from 'react';

import Product from '@components/Product';

import styles from './index.scss';

const ItemAddedView = props => (
  <div className={!props.cart && !props.orderStatus && !props.wedding ? styles.cart : ''}>
    {!props && <h2>Items Added</h2>}
    <div className={styles.item} style={!props.cart ? {background: 'white'} : {}}>
      <div className={styles.itemSwatch}>
        <p>
          <b>{props.itemName}</b>
        </p>
        <Product img={props.imgName} type="grid" shirt={props.length} pocket={props.pocket} />
      </div>
      <div className={styles.itemDetails}>
        {props.cart && (
          <div className={styles.changeQuantity}>
            <div onClick={() => props.changeQuantity(props.i, 'sub')}>&#x2212;</div>
            <span>{props.quantity}</span>
            <div onClick={() => props.changeQuantity(props.i, 'add')}>&#x2B;</div>
          </div>
        )}
        <p>{props.length} Sleeve</p>
        {props.pocket && <p>With Pocket Square</p>}
        <p>Size: {props.size}</p>
        {(!props.orderStatus && (
          <p style={{marginTop: '20px'}}>
            {props.quantity || 1} x ${props.pocket ? parseInt(props.price) + 5 : parseInt(props.price)}
          </p>
        )) || (
          <p style={{marginTop: '20px'}}>
            {props.quantity || 1} x $
            {props.pocket ? parseInt(props.price) / 100 + 5 : parseInt(props.price) / 100}
          </p>
        )}
      </div>
    </div>
    {props.children}
  </div>
);

export default ItemAddedView;
