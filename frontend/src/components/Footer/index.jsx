import React, {useState} from 'react';
import {Link} from 'react-router-dom';
// import {useSelector} from 'redux-zero/react';

import Contact from './contact';

import SizeChart from '@components/SizeChart';

import logo from '@assets/icons/bwfeather.png';
// import Twitter from '@assets/icons/twitter.svg';
import Facebook from '@assets/icons/facebook.svg';
import Instagram from '@assets/icons/instagram.svg';

import styles from './index.scss';

const Footer = props => {
  // const categories = useSelector(({categories}) => categories);
  const [showSizeGuide, setShowSizeGuide] = useState(false);
  if (props.location === 'landing') {
    return (
      <footer className={styles.footer}>
        <section className={styles.contentSection}>
          <div className={styles.homeLink} onClick={() => window.scrollTo(0, 0)}>
            <img src={logo} className={styles.logo} alt="peacock sleeves logo" />
            <span>Peacock</span>
            <span>Sleeves</span>
          </div>
          <div className={styles.socialIcons}>
            <a target="_blank" rel="noreferrer" href="https://www.facebook.com/peacocksleeves">
              <Facebook className={styles.icons} />
            </a>
            <a target="_blank" rel="noreferrer" href="https://www.instagram.com/peacocksleeves">
              <Instagram className={styles.icons} />
            </a>
          </div>
        </section>
        <section className={styles.siteLinks}>
          <h4>Shop</h4>
          <Link to="/long-sleeves">Long Sleeves</Link>
          <Link to="/short-sleeves">Short Sleeves</Link>
          <Link to="/tuxedo-sleeves">Tuxedo Sleeves</Link>
          <Link to="/group-order">Group Orders</Link>
        </section>
        <section className={styles.siteLinks2}>
          <h4>Support</h4>
          <Link to="/returns">Returns</Link>
          <Link to="/shipping">Shipping</Link>
          <p onClick={() => props.setShowContact(true)} style={{cursor: 'pointer'}}>
            Contact Us
          </p>
          <Link to="/order-status">Order Status</Link>
          <Link to="/privacy">Privacy Policy</Link>
        </section>
        <section className={styles.siteLinks3}>
          <h4>About</h4>
          <Link to="/faq">FAQ</Link>
          <Link to="/gallery">Gallery</Link>
          <Link to="/manifesto">Manifesto</Link>
          <p onClick={() => setShowSizeGuide(true)} style={{cursor: 'pointer'}}>
            Size Guide
          </p>
        </section>
        {props.showContact && <Contact setShowContact={props.setShowContact} />}
        {showSizeGuide && <SizeChart modal={true} setShowSizeGuide={setShowSizeGuide} />}
      </footer>
    );
  } else {
    return '';
  }
};

export default Footer;
