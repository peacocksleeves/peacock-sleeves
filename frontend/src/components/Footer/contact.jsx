import React, {useState} from 'react';

import apiRequest from '@util/apiRequest';
import Loading from '@components/Loading';
import logo from '@assets/icons/bwfeather.png';

import styles from './contact.scss';

const Contact = props => {
  const [loading, setLoading] = useState(false);
  const [messageSuccess, setMessageSuccess] = useState(false);
  const [messageError, setMessageError] = useState(false);

  const [formData, setFormData] = useState({
    name: '',
    email: '',
    subject: '',
    message: '',
  });

  const inputChange = e => {
    e.preventDefault();
    e.persist();
    setFormData({...formData, [e.target.name]: e.target.value});
  };

  const submitForm = async e => {
    setLoading(true);
    e.preventDefault();
    const message =
      `Name: ${formData.name}\n\n` + `Email: ${formData.email}\n\n` + `Message: ${formData.message}`;
    const requestBody = {
      source: process.env.ENV === 'prod' ? 'prod' : 'test',
      message,
      subject: formData.subject,
    };
    try {
      const resp = await apiRequest('POST', {headers: {}, body: requestBody}, 'contact');
      if (resp.status === 400) throw error;
      setLoading(false);
      setMessageSuccess(true);
      setTimeout(() => {
        setMessageSuccess(false);
        props.setShowContact(false);
      }, 2000);
    } catch (error) {
      setLoading(false);
      setMessageError(true);
      setTimeout(() => {
        setMessageError(false);
      }, 3500);
    }
  };

  return (
    <div className={styles.modal} onClick={() => props.setShowContact(false)}>
      <div className={styles.formContainer}>
        <div
          className={styles.form}
          onClick={e => {
            e.stopPropagation();
          }}>
          <>
            <div className={styles.logoContainer}>
              <img src={logo} className={styles.logo} alt="peacock sleeves logo" />
              <span className={styles.logoTitle}>Peacock</span>
              <span className={styles.logoTitle}>Sleeves</span>
            </div>
            <h2 className={styles.formTitle}>How can we help</h2>
            <span className={styles.close} onClick={() => props.setShowContact(false)}>
              x
            </span>
          </>
          <form onSubmit={submitForm}>
            <input
              onChange={inputChange}
              type="text"
              name="name"
              placeholder="Full Name*"
              required
              pattern="^[a-zA-Z \.,'-]+$"
            />
            <input onChange={inputChange} type="email" name="email" placeholder="Email*" required />
            <input
              onChange={inputChange}
              type="text"
              name="subject"
              placeholder="Subject*"
              required
              pattern="^[a-zA-Z0-9 \.-]+$"
            />
            <p className={styles.messageLabel}>Message</p>
            <textarea className={styles.messageArea} name={'message'} onChange={inputChange} />
            {(loading && (
              <div id={styles.sendEmail} style={{display: 'grid'}}>
                <Loading />
              </div>
            )) || (
              <input
                id={styles.sendEmail}
                style={{display: 'grid'}}
                type="submit"
                value="Send It &#129305;"
              />
            )}
          </form>
          {messageSuccess && (
            <p className={styles.success}>Message sent! We're looking forward to connecting.</p>
          )}
          {messageError && (
            <p className={styles.error}>Shit something's wrong on our end. Try again shortly.</p>
          )}
        </div>
      </div>
    </div>
  );
};

export default Contact;
