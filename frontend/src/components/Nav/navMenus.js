const landing = [
  {
    to: '/',
  },
  {
    to: '/shop',
    text: 'Shop',
  },
  {
    to: '/media',
    text: 'Media',
  },
  {
    to: '/peacocking',
    text: 'Peacocking',
  },
];

export {landing};
