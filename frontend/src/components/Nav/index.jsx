import React, {useState} from 'react';
import {NavLink} from 'react-router-dom';
import {useSelector} from 'redux-zero/react';

import logo from '@assets/icons/feather.png';

import styles from './index.scss';

const Nav = props => {
  const [showMobileLinks, setShowMobileLinks] = useState(false);
  const [showDropdownLinks, setShowDropdownLinks] = useState(false);
  const [showCategoryLinks, setShowCategoryLinks] = useState(false);
  const [showCommLinks, setShowCommLinks] = useState(false);
  const categories = useSelector(({categories}) => categories);

  const hamHandleClick = () => {
    setShowMobileLinks(!showMobileLinks);
    setShowDropdownLinks(false);
    setShowCategoryLinks(false);
    setShowCommLinks(false);
  };

  const linkHandleClick = () => {
    setShowMobileLinks(false);
    setShowDropdownLinks(false);
    setShowCategoryLinks(false);
    setShowCommLinks(false);
  };

  return (
    <>
      <nav className={styles.mainNav}>
        <NavLink to={'/'} className={styles.homeLink}>
          <img src={logo} className={styles.logo} alt="peacock sleeves logo" />
          <span>PEACOCK</span>
          <span>Sleeves</span>
        </NavLink>
        {(props.location !== 'build' && (
          <>
            <span
              className={`${styles.hamburger} ${showMobileLinks ? styles.clickedHam : ''}`}
              onClick={hamHandleClick}></span>
            <div className={styles.navMenu}>
              <div className={styles.dropDown}>
                <p
                  className={`${styles.linkItems} ${showMobileLinks ? styles.mobileShowLinks : ''}`}
                  onClick={() => (
                    setShowDropdownLinks(!showDropdownLinks),
                    setShowCategoryLinks(false),
                    setShowCommLinks(false)
                  )}
                  style={{cursor: 'pointer'}}>
                  Shop &#x25BC;
                </p>
                {showDropdownLinks && (
                  <div className={styles.dropDownContent}>
                    <NavLink to="/long-sleeves" onClick={linkHandleClick}>
                      Long Sleeves
                    </NavLink>
                    <NavLink to="/short-sleeves" onClick={linkHandleClick}>
                      Short Sleeves
                    </NavLink>
                    <NavLink to="/tuxedo-sleeves" onClick={linkHandleClick}>
                      Tuxedo Sleeves
                    </NavLink>
                    <NavLink to="/group-order" onClick={linkHandleClick}>
                      Group Orders
                    </NavLink>
                  </div>
                )}
              </div>
              <div className={styles.dropDown}>
                <p
                  className={`${styles.linkItems} ${showMobileLinks ? styles.mobileShowLinks : ''}`}
                  onClick={() => (
                    setShowCategoryLinks(!showCategoryLinks),
                    setShowDropdownLinks(false),
                    setShowCommLinks(false)
                  )}
                  style={{cursor: 'pointer'}}>
                  Categories &#x25BC;
                </p>
                {showCategoryLinks && (
                  <div className={styles.dropDownContent}>
                    {categories.map((category, i) => (
                      <NavLink
                        key={i}
                        to={`/build/category/${category.toLowerCase().replace(' ', '')}`}
                        onClick={linkHandleClick}>
                        {category}
                      </NavLink>
                    ))}
                  </div>
                )}
              </div>
              <NavLink
                className={`${styles.linkItems} ${showMobileLinks ? styles.mobileShowLinks : ''}`}
                onClick={linkHandleClick}
                to={'/build'}>
                Build
              </NavLink>
              <div className={styles.dropDown}>
                <p
                  className={`${styles.linkItems} ${showMobileLinks ? styles.mobileShowLinks : ''}`}
                  onClick={() => (
                    setShowCommLinks(!showCommLinks), setShowCategoryLinks(false), setShowDropdownLinks(false)
                  )}
                  style={{cursor: 'pointer'}}>
                  Community &#x25BC;
                </p>
                {showCommLinks && (
                  <div className={styles.dropDownContent}>
                    <NavLink
                      // className={`${styles.linkItems} ${showMobileLinks ? styles.mobileShowLinks : ''}`}
                      onClick={linkHandleClick}
                      to={'/gallery'}>
                      Gallery
                    </NavLink>
                    <a
                      // className={`${styles.linkItems} ${showMobileLinks ? styles.mobileShowLinks : ''}`}
                      onClick={linkHandleClick}
                      target="_blank"
                      rel="noreferrer"
                      href="https://www.instagram.com/peacocksleeves">
                      Instagram
                    </a>
                  </div>
                )}
              </div>
            </div>
          </>
        )) || (
          <NavLink className={styles.buildLink} to={'/'}>
            Exit Build
          </NavLink>
        )}
      </nav>
    </>
  );
};

export default Nav;
