import React from 'react';
import {render} from 'react-dom';
import {Helmet} from 'react-helmet';
import {BrowserRouter} from 'react-router-dom';
import {Provider} from 'redux-zero/react';
// import {initDB} from 'react-indexed-db';

import store from '@redux/store';
// import {Indexedb} from '@util/indexedb';

import favicon32 from '@assets/icons/favicon32.png';
import favicon16 from '@assets/icons/favicon16.png';
import favicon from '@assets/icons/favicon.ico';

import Main from './Main.jsx';
import '@styles/main.scss';

// initDB(Indexedb);

function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Helmet>
          <meta name="viewport" content="width=device-width, initial-scale=1.0" />
          <link rel="icon" type="image/png" sizes="32x32" href={favicon32} />
          <link rel="icon" type="image/png" sizes="16x16" href={favicon16} />
          <link rel="shortcut icon" type="image/x-icon" href={favicon} />
          <meta name="theme-color" content="#ffffff" />
        </Helmet>
        <Main />
      </BrowserRouter>
    </Provider>
  );
}

render(<App />, document.getElementById('root'));
