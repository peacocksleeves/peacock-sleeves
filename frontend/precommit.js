const git = require('simple-git/promise');
const util = require('util');
const exec = util.promisify(require('child_process').exec);
// const lintStaged = require('lint-staged');
const uiRegex = /^frontend/g;

async function runUiChecks() {
  // const lint = await lintStaged();
  const prettier = await exec('npm run prettier');
  console.log(prettier.stdout);
  console.log('\n----------------\nPrettier Complete\n----------------\n');
  const lintStyle = await exec('npm run lint-style');
  console.log(lintStyle.stdout);
  console.log('\n----------------\nLint-Style Complete\n----------------\n');
  // if (lint) console.log('\n----------------\nLint Success\n----------------\n');
  // else throw new Error('Lint Failure');
  // const {stdout} = await exec('npm run test');
  // console.log(stdout);
  // console.log('\n--------------------\nUnit Test Success\n--------------------\n');
}

async function validateChanges() {
  try {
    const diffSummary = await git().diffSummary(['--staged']);
    const files = diffSummary.files.map(file => file.file).some(file => uiRegex.test(file));
    if (!files) return;
    await runUiChecks();
  } catch (err) {
    console.log('Error: ', err);
    process.exit(1);
  }
}

validateChanges();
