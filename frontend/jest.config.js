module.exports = {
  roots: ['<rootDir>/src'],
  moduleFileExtensions: ['js', 'jsx', 'json', 'node'],
  moduleNameMapper: {
    '@util/(.*)': '<rootDir>/src/global/util/$1',
    '@styles/(.*)': '<rootDir>/src/global/sass/$1',
    '@redux/(.*)': '<rootDir>/src/global/redux/$1',
    '@components/(.*)': '<rootDir>/src/components/$1',
    '@routes/(.*)': '<rootDir>/src/routes/$1',
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/__mocks__/fileMock.js',
    '\\.(scss)$': '<rootDir>/__mocks__/sassMock.js'
  },
  automock: false,
  setupFiles: ['./__mocks__/setupJest.js'],
  coverageThreshold: {
    global: {
      branches: 80,
      functions: 80,
      lines: 80,
      statements: 80,
    },
  },
};
