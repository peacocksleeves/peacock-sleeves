  
provider "aws" {
  region = "us-east-2"
}

terraform {
  backend "s3" {
    bucket = "peacock-tf-state"
    key    = "dev/rds/terraform.tfstate"
    region = "us-east-2"
  }
}

variable "env" {
  default = "dev"
}

module "rds" {
  source = "../../../modules/rds"

  env      = var.env
  name     = "peacock-db"
  username = "peacock"
  password = "iampeacocking"
}
