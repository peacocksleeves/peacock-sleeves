  
provider "aws" {
  region = "us-east-2"
}

terraform {
  backend "s3" {
    bucket = "peacock-tf-state"
    key    = "dev/az2/data-sub/terraform.tfstate"
    region = "us-east-2"
  }
}

variable "env" {
  default = "dev"
}

variable "az" {
  default = "az2"
}

module "data" {
  source = "../../../../modules/data-sub"

  env = var.env
  az = var.az
}