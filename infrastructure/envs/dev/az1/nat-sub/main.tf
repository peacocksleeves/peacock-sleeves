  
provider "aws" {
  region = "us-east-2"
}

terraform {
  backend "s3" {
    bucket = "peacock-tf-state"
    key    = "dev/az1/nat-sub/terraform.tfstate"
    region = "us-east-2"
  }
}

variable "env" {
  default = "dev"
}

variable "az" {
  default = "az1"
}

module "nat" {
  source = "../../../../modules/nat-sub"

  env = var.env
  az = var.az
}