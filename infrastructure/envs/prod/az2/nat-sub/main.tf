  
provider "aws" {
  region = "us-east-2"
}

terraform {
  backend "s3" {
    bucket = "peacock-tf-state"
    key    = "prod/az2/nat-sub/terraform.tfstate"
    region = "us-east-2"
  }
}

variable "env" {
  default = "prod"
}

variable "az" {
  default = "az2"
}

module "nat" {
  source = "../../../../modules/nat-sub"

  env = var.env
  az = var.az
}