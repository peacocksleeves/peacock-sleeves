  
provider "aws" {
  region = "us-east-2"
}

terraform {
  backend "s3" {
    bucket = "peacock-tf-state"
    key    = "prod/rds/terraform.tfstate"
    region = "us-east-2"
  }
}

variable "env" {
  default = "prod"
}

module "rds" {
  source = "../../../modules/rds"

  env      = var.env
  name     = "peacock-db"
  username = "p3ac0ck"
  password = "1amp3ac0ck!ng"
}