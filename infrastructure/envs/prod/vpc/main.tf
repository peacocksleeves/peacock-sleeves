provider "aws" {
  region = "us-east-2"
}

terraform {
  backend "s3" {
    bucket = "peacock-tf-state"
    key    = "prod/vpc/terraform.tfstate"
    region = "us-east-2"
  }
}

variable "env" {
  default = "prod"
}

module "vpc" {
  source = "../../../modules/vpc"

  env = var.env
}