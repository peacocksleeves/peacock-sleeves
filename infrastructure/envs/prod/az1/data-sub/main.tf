  
provider "aws" {
  region = "us-east-2"
}

terraform {
  backend "s3" {
    bucket = "peacock-tf-state"
    key    = "prod/az1/data-sub/terraform.tfstate"
    region = "us-east-2"
  }
}

variable "env" {
  default = "prod"
}

variable "az" {
  default = "az1"
}

module "data" {
  source = "../../../../modules/data-sub"

  env = var.env
  az = var.az
}