# Data Subnet used for data servers

module "config" {
  source = "../../config"
}

data "aws_vpc" "peacock_vpc" {
  tags = {
    Name = "peacock-${var.env}-vpc"
  }
}

data "aws_internet_gateway" "igw" {
  tags = {
    Name = "peacock-${var.env}-igw"
  }
}

resource "aws_subnet" "data" {
  vpc_id            = data.aws_vpc.peacock_vpc.id
  cidr_block        = lookup(module.config.data_cidr[var.env], var.az)
  availability_zone = "us-east-${module.config.azs[var.az]}"

  tags = {
    Name = "peacock-${var.env}-${var.az}-data-sub"
    Env  = var.env
  }
}

resource "aws_route_table" "data" {
  vpc_id = data.aws_vpc.peacock_vpc.id

  tags = {
    Name = "peacock-${var.env}-${var.az}-data-sub-routes"
    Env  = var.env
  }
}

resource "aws_route" "internet_access" {
  route_table_id         = aws_route_table.data.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = data.aws_internet_gateway.igw.id
}

resource "aws_route_table_association" "data" {
  subnet_id      = aws_subnet.data.id
  route_table_id = aws_route_table.data.id
}
