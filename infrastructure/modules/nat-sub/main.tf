module "config" {
  source = "../../config"
}

data "aws_vpc" "peacock_vpc" {
  tags = {
    Name = "peacock-${var.env}-vpc"
  }
}

data "aws_internet_gateway" "igw" {
  tags = {
    Name = "peacock-${var.env}-igw"
  }
}

resource "aws_eip" "nat_gw" {
  vpc      = true
  depends_on = [data.aws_internet_gateway.igw]

  tags = {
    Name = "peacock-${var.env}-${var.az}-nat-eip"
    Env  = var.env
  }
}

resource "aws_subnet" "nat_gw" {
  vpc_id            = data.aws_vpc.peacock_vpc.id
  cidr_block        = lookup(module.config.nat_cidr[var.env], var.az)
  availability_zone = "us-east-${module.config.azs[var.az]}"

  tags = {
    Name = "peacock-${var.env}-${var.az}-nat-sub"
    Env  = var.env
  }
}

resource "aws_nat_gateway" "nat_gw" {
  allocation_id = aws_eip.nat_gw.id
  subnet_id     = aws_subnet.nat_gw.id

  tags = {
    Name = "peacock-${var.env}-${var.az}-nat-sub-ngw"
    Env  = var.env
  }
}

resource "aws_route_table" "nat_gw" {
  vpc_id = data.aws_vpc.peacock_vpc.id

  tags = {
    Name = "peacock-${var.env}-${var.az}-nat-sub-routes"
    Env  = var.env
  }
}

resource "aws_route" "internet_access" {
  route_table_id         = aws_route_table.nat_gw.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id         = data.aws_internet_gateway.igw.id
}

resource "aws_route_table_association" "nat_gw" {
  subnet_id      = aws_subnet.nat_gw.id
  route_table_id = aws_route_table.nat_gw.id
}