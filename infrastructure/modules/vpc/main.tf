module "config" {
  source = "../../config"
}

resource "aws_vpc" "peacock_vpc" {
  cidr_block = module.config.vpc_cidr[var.env]

  enable_dns_support = true
  enable_dns_hostnames = true

  tags = {
    Name = "peacock-${var.env}-vpc"
    Env  = var.env
  }
}

resource "aws_internet_gateway" "peacock_igw" {
  vpc_id = aws_vpc.peacock_vpc.id

  tags = {
    Name = "peacock-${var.env}-igw"
    Env  = var.env
  }
}

resource "aws_security_group" "peacock-ssh" {
  name        = "peacock-${var.env}-ssh"
  description = "Allow incoming access to ssh port"
  vpc_id = aws_vpc.peacock_vpc.id

  ingress {
    from_port   = 1990
    to_port     = 1990
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "peacock-${var.env}-ssh"
    Env  = var.env
  }
}