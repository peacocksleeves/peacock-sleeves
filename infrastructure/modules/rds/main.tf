## GET VPC
data "aws_vpc" "peackock_vpc" {
  tags = {
    Name = "peacock-${var.env}-vpc"
  }
}

## GET DATA SUBNETS
data "aws_subnet" "data_all" {
  count = 2

  tags = {
    Name = "peacock-${var.env}-az${count.index + 1}-data-sub"
  }
}

resource "aws_db_subnet_group" "peackock" {
  name        = "${var.name}-${var.env}-sub-group"
  description = "${var.name}-${var.env}-sub-group"
  subnet_ids  = data.aws_subnet.data_all.*.id

  tags = {
    Name = "${var.name}-${var.env}-sub-group"
    Env  = var.env
  }
}

resource "aws_security_group" "peacock" {
  vpc_id      = data.aws_vpc.peackock_vpc.id
  name        = "${var.name}-${var.env}-sg"

  ingress {
    protocol    = "tcp"
    from_port   = 3306
    to_port     = 3306
    cidr_blocks = data.aws_subnet.data_all.*.cidr_block
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }

  tags = {
    Name = "${var.name}-${var.env}-sg"
    Env  = var.env
  }
}

resource "aws_db_instance" "peacock" {
  allocated_storage       = 20
  max_allocated_storage   = 100
  storage_type            = "gp2"
  engine                  = "mariadb"
  engine_version          = "10.3.20"
  instance_class          = var.env == "dev" ? "db.t2.micro" : "db.t3.small"
  vpc_security_group_ids  = [aws_security_group.peacock.id]
  db_subnet_group_name    = aws_db_subnet_group.peackock.name
  backup_retention_period = 0
  skip_final_snapshot     = true
  publicly_accessible     = true
  identifier              = "${var.name}-${var.env}"
  name                    = "peacock"
  username                = var.username
  password                = var.password

  tags = {
    Name = "${var.name}-${var.env}"
    Env  = var.env
  }
}
