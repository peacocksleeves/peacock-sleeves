const stripe = require('stripe')(process.env.SERVICE_LOG_DATE);
const shippo = require('shippo')(process.env.SERVICE_LOG_NAME);
const AWS = require('aws-sdk');
AWS.config.region = 'us-east-2';
const lambda = new AWS.Lambda();
const s3 = new AWS.S3();
const ses = new AWS.SES();
const whitelist = process.env.ORIGINS.split(',');

const routerFuncs = {
  createCheckout: createCheckout,
  createOrder: createOrder,
  createShippoOrder: createShippoOrder,
  notifyOfOrder: notifyOfOrder,
  getOrder: getOrder,
  getOrders: getOrders,
  updateOrder: updateOrder,
  createProduct: createProduct,
  getProduct: getProduct,
  getProducts: getProducts,
  updateProduct: updateProduct,
  deleteProduct: deleteProduct,
  createGroup: createGroup,
  getGroup: getGroup,
  updateGroup: updateGroup,
  deleteGroup: deleteGroup,
  weddingInvite: weddingInvite,
  groupInvite: groupInvite,
  createMemo: createMemo,
  getMemos: getMemos,
  updateMemo: updateMemo,
  deleteMemo: deleteMemo,
  createCoupon: createCoupon,
  getCoupons: getCoupons,
  updateCoupon: updateCoupon,
  getInventory: getInventory,
  updateInventory: updateInventory,
  getStats: getStats,
  contact: contact
}

function HTTPError (statusCode, message) {
  const error = new Error(message)
  error.statusCode = statusCode
  return error
}

async function invokeLambda(lambdaFN, payload) {
  const params = {
    FunctionName: lambdaFN,
    InvocationType: 'RequestResponse',
    LogType: 'Tail',
    Payload: payload ? JSON.stringify(payload) : ''
  };
  try {
    const response = await lambda.invoke(params).promise();
    if (response.Payload.includes("error"))
      throw new HTTPError(500, JSON.parse(response.Payload).errorMessage);
    return response;
  } catch(err) {
    throw Error(err);
  }
}

module.exports.router = async (event) => {
  try {
    if (!whitelist.includes(event.headers.origin) && !event.headers['stripe-signature'])
      throw new HTTPError(403, 'Eet Rotzooi!');
    if (event["pathParameters"]["function"] in routerFuncs) {
      return await routerFuncs[event["pathParameters"]["function"]](event);
    }
  } catch (err) {
    console.log("ERROR IN ROUTER: ", err)
    return {
      statusCode: err.statusCode || 405,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: 
        JSON.stringify({message: err.message}) ||
        JSON.stringify({message: `Invalid API Router Function: ${event["pathParameters"]["function"]}`}),
    };
  }
};

//////////////
// STRIPE
/////////////

async function createCheckout(event) {
  const body = JSON.parse(event.body);
  const successUrl = 
    process.env.ENV === 'prod' ?
      'https://www.peacocksleeves.com/cart/success?session_id={CHECKOUT_SESSION_ID}' :
      'https://dev.peacocksleeves.com/cart/success?session_id={CHECKOUT_SESSION_ID}';
  const cancelUrl =
    process.env.ENV === 'prod' ?
      'https://www.peacocksleeves.com/cart' :
      'https://dev.peacocksleeves.com/cart';
  const items =
    body.map(item => ({
      price_data: {
        currency: 'usd',
        product_data: {
          name: `${item.name}, ${item.length} sleeve, size ${item.size}${item.pocket ? ', with pocket square' : ''}`,
          images: [`https://peacock-swatches-${process.env.ENV}.s3.amazonaws.com/${item.imgName}large.jpg`]
        },
        unit_amount: (item.price + (item.pocket ? 5 : 0) ) * 100
      },
      adjustable_quantity: {
        enabled: true,
        minimum: 1,
        maximum: 10
      },
      quantity: item.quantity
    }));
  const checkout = {
    billing_address_collection: 'auto',
    shipping_address_collection: {
      allowed_countries: ['US'],
    },
    line_items: items,
    mode: 'payment',
    allow_promotion_codes: true,
    payment_method_types: ['card'],
    success_url: successUrl,
    cancel_url: cancelUrl,
    shipping_rates: process.env.ENV === 'dev' ? ['shr_1IB7jIDvj8m836F98PPamC2O'] : ['shr_1IB7ssDvj8m836F9x362PxGQ']
  }
  try {
    const session = await stripe.checkout.sessions.create(checkout);
    return {
      statusCode: 200,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify({ sessionId: session.id })
    }
  } catch (err) {
    console.log("ERROR IN CHECKOUT: ", err)
    return {
      statusCode: err.statusCode || 500,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify({message: err.message}) || JSON.stringify({message: 'Could not create the checkout session.'})
    }
  }
}

async function createOrder(event) {
  const sig = event.headers['stripe-signature'];
  const secret = process.env.SERVICE_LOG_PATH;
  try {
    const stripEvent = stripe.webhooks.constructEvent(event.body, sig, secret);
    const session = await stripe.checkout.sessions.retrieve(stripEvent.data.object.id, {expand: ['payment_intent','line_items']});
    const cart = 
      session.line_items.data.map(
        (item) => {
          const cartPieces = item.description.split(', ');
          return ({
            name: cartPieces[0],
            imgName: cartPieces[0].replace(/\s/g, '').toLowerCase(),
            price: item.amount_total,
            quantity: item.quantity,
            length: cartPieces[1].split(' ')[0],
            size: cartPieces[2].split(' ')[1],
            pocket: cartPieces.length > 3 ? true : false
          })
        }
      );
    if (process.env.ENV !== 'dev' && !session.payment_intent.charges.data[0].receipt_number) throw new HTTPError(422, 'Attempting Retry Due To Missing Receipt Number');
    const toBeId = process.env.ENV === 'dev' ? `${Math.floor(Math.random() * 100000000)}` : session.payment_intent.charges.data[0].receipt_number.replace('-', '');
    const order = {
      receipt: toBeId,
      amount: session.payment_intent.amount,
      email: session.payment_intent.charges.data[0].receipt_email,
      name: session.shipping.name,
      address: session.shipping.address.line1 + (session.shipping.address.line2 ? ` ${session.shipping.address.line2}` : ''),
      city: session.shipping.address.city,
      state: session.shipping.address.state,
      zip: session.shipping.address.postal_code,
      cart: JSON.stringify(cart)
    };
    await invokeLambda(`peacock-private-db-${process.env.ENV}-router`, {function: 'create', table: 'orders', values: order});
    return {
      statusCode: 200,
      headers: { 'Access-Control-Allow-Origin' : '*' }
    }
  }
  catch (err) {
    console.log("ERROR IN CREATE ORDER: ", err)
    return {
      statusCode: err.statusCode || 500,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify({message: `Webhook Error: ${err.message}`})
    }
  }
};

async function createShippoOrder(event) {
  const sig = event.headers['stripe-signature'];
  const secret = process.env.SERVICE_LOG_QUE;
  try {
    let totalQuantity = 0;
    let totalWeight = 0;
    const stripEvent = stripe.webhooks.constructEvent(event.body, sig, secret);
    const session = await stripe.checkout.sessions.retrieve(stripEvent.data.object.id, {expand: ['payment_intent','line_items']});
    const line_items = session.line_items.data.map((item) => {
      totalQuantity += item.quantity;
      totalWeight += 
        item.description.includes('short') ? (5 * item.quantity) :
        item.description.includes('long') ? (7 * item.quantity) :
        (12 * item.quantity);
      return (
        {
          title: item.description,
          quantity: item.quantity
        }
      );
    });
    if (process.env.ENV !== 'dev' && !session.payment_intent.charges.data[0].receipt_number) throw new HTTPError(422, 'Attempting Retry Due To Missing Receipt Number');
    const toBeId = process.env.ENV === 'dev' ? `${Math.floor(Math.random() * 100000000)}` : session.payment_intent.charges.data[0].receipt_number.replace('-', '');
    const to_address = {
      name: session.shipping.name,
      email: session.payment_intent.charges.data[0].receipt_email,
      street1: session.shipping.address.line1,
      street2: session.shipping.address.line2 || "",
      city: session.shipping.address.city,
      state: session.shipping.address.state,
      zip: session.shipping.address.postal_code,
      country: "US"
    };
    await shippo.order.create({
      to_address,
      line_items,
      placed_at: new Date().toISOString(),
      order_number: toBeId,
      weight: totalWeight,
      weight_unit: 'oz',
      async: false
    });

    return {
      statusCode: 200,
      headers: { 'Access-Control-Allow-Origin' : '*' }
    }
  }
  catch (err) {
    console.log("ERROR IN SHIPPO CREATE: ", err)
    return {
      statusCode: err.statusCode || 500,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify({message: `Shippo Webhook Error: ${err.message}`})
    }
  }
};

async function notifyOfOrder(event) {
  const sig = event.headers['stripe-signature'];
  const secret = process.env.SERVICE_LOG_TRACE
  try {
    const stripEvent = stripe.webhooks.constructEvent(event.body, sig, secret);
    const session = await stripe.checkout.sessions.retrieve(stripEvent.data.object.id, {expand: ['payment_intent']});
    const emailParams = {
      Source: 'Peacock Sleeves <hello@peacocksleeves.com>',
      Destination: { ToAddresses: ['nick@peacocksleeves.com', 'hello@peacocksleeves.com'] },
      Message: {
        Body: {
          Text: {
            Charset: 'UTF-8',
            Data: `Customer: ${session.shipping.name}\nAmount Paid: $${(session.payment_intent.amount/100).toFixed(2)}`
          }
        },
        Subject: {
          Charset: 'UTF-8',
          Data: 'Peacock Sleeves - New Order Created!'
        }
      }
    };
    await ses.sendEmail(emailParams).promise();
    return {
      statusCode: 200,
      headers: { 'Access-Control-Allow-Origin' : '*' }
    }
  }
  catch (err) {
    console.log("ERROR IN NOTIFY: ", err)
    return {
      statusCode: err.statusCode || 500,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify({message: `Webhook Error: ${err.message}`})
    }
  }
};


//////////////
// ORDERS
/////////////

async function getOrder(event) {
  const id = JSON.parse(event.pathParameters.id);
  try {
    const response = await invokeLambda(`peacock-private-db-${process.env.ENV}-router`, {function: 'getSome', table: 'orders', select:'*', clausend: ` WHERE receipt='${id}'`});
    const data = JSON.parse(response.Payload);
    let reqResponse;
    data.length != 0 ? (
      reqResponse = {
        statusCode: 200,
        headers: { 'Access-Control-Allow-Origin' : '*' },
        body: JSON.stringify(data[0])
      } ) : (
      reqResponse = {
        statusCode: 200,
        headers: { 'Access-Control-Allow-Origin' : '*' },
        body: JSON.stringify({message: `There is no order in our records with Order Number: ${id}. Please check your receipt and search again.`})
      }
    );
    return reqResponse;
  } catch (err) {
    console.log("ERROR IN GETONE ORDER: ", err)
    return {
      statusCode: err.statusCode || 500,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify({message: err.message}) || JSON.stringify({message: 'Could not get the order.'})
    }
  }
};

async function getOrders() {
  try {
    const response = await invokeLambda(`peacock-private-db-${process.env.ENV}-router`, {function: 'getAll', table: 'orders'});
    const data = JSON.parse(response.Payload);
    return {
      statusCode: 200,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify(data)
    }
  } catch (err) {
    console.log("ERROR IN GETALL ORDERS: ", err)
    return {
      statusCode: err.statusCode || 500,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify({message: err.message}) || JSON.stringify({message: 'Could not get the orders.'})
    }
  }
};

async function updateOrder(event) {
  const id = JSON.parse(event.pathParameters.id);
  const updates = JSON.parse(event.body);
  try {
    if (updates.img) {
      let image = updates.img.split(';base64,').pop();
      image = Buffer.from(image, 'base64');
      await s3.putObject({Bucket: `peacock-swatches-uploads-${process.env.ENV}`, Key: `${updates.imgName}.jpg`, Body: image, ACL: 'public-read'}).promise();
      delete updates.img;
      delete updates.imgName;
    }
    const response = await invokeLambda(`peacock-private-db-${process.env.ENV}-router`, {function: 'update', table: 'orders', id, updates});
    const data = JSON.parse(response.Payload);
    return {
      statusCode: 200,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify(data)
    }
  } catch (err) {
    console.log("ERROR IN UPDATE ORDER: ", err)
    return {
      statusCode: err.statusCode || 500,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify({message: err.message}) || JSON.stringify({message: 'Could not update the order.', data: updates})
    }
  }
};

// module.exports.deleteOrder = async (event) => {};

//////////////
// PRODUCTS
/////////////

async function createProduct(event) {
  const body = JSON.parse(event.body);
  body.price = JSON.stringify(body.price);
  try {
    let image = body.img.split(';base64,').pop();
    image = Buffer.from(image, 'base64');
    await s3.putObject({Bucket: `peacock-swatches-uploads-${process.env.ENV}`, Key: `${body.imgName}.jpg`, Body: image, ACL: 'public-read'}).promise();
    delete body.img;
    await invokeLambda(`peacock-private-db-${process.env.ENV}-router`, {function: 'create', table: 'products', values: body});
    return {
     statusCode: 200,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify({ message: 'Success', data: body })
    }
  } catch (err) {
    console.log("ERROR IN CREATE PRODUCT: ", err)
    return {
      statusCode: err.statusCode || 500,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify({message: err.message}) || JSON.stringify({message: 'Could not create the product.'})
    }
  }
};

async function getProduct(event) {
  const id = JSON.parse(event.pathParameters.id);
  try {
    const response = await invokeLambda(`peacock-private-db-${process.env.ENV}-router`, {function: 'getOne', table: 'products', id});
    const data = JSON.parse(response.Payload);
    let reqResponse;
    data.length != 0 ? (
      reqResponse = {
        statusCode: 200,
        headers: { 'Access-Control-Allow-Origin' : '*' },
        body: JSON.stringify(data[0])
      } ) : (
      reqResponse = {
        statusCode: 200,
        headers: { 'Access-Control-Allow-Origin' : '*' },
        body: JSON.stringify({message: 'Product not found'})
      }
    );
    return reqResponse;
  } catch (err) {
    console.log("ERROR IN GETONE PRODUCT: ", err)
    return {
      statusCode: err.statusCode || 500,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify({message: err.message}) || JSON.stringify({message: 'Could not get the order.'})
    }
  }
};

async function getProducts() {
  try {
    const response = await invokeLambda(`peacock-private-db-${process.env.ENV}-router`, {function: 'getAll', table: 'products'});
    const data = JSON.parse(response.Payload);
    return {
      statusCode: 200,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify(data)
    }
  } catch (err) {
    console.log("ERROR IN GETALL PRODUCTS: ", err)
    return {
      statusCode: err.statusCode || 500,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify({message: err.message}) || JSON.stringify({message: 'Could not get the orders.'})
    }
  }
};

 async function updateProduct(event) {
  const id = JSON.parse(event.pathParameters.id);
  const updates = JSON.parse(event.body);
  updates.price = JSON.stringify(updates.price);
  try {
    const response = await invokeLambda(`peacock-private-db-${process.env.ENV}-router`, {function: 'update', table: 'products', id, updates});
    const data = JSON.parse(response.Payload);
    if (updates.imgName) {
      await s3.copyObject(
        {
          Bucket: `peacock-swatches-uploads-${process.env.ENV}`,
          CopySource: `/peacock-swatches-uploads-${process.env.ENV}/${body.imgName}.jpg`,
          Key: `${body.imgName}.jpg`,
          ACL: 'public-read'
        }
      ).promise();
    }
    return {
      statusCode: 200,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify(data)
    }
  } catch (err) {
    console.log("ERROR IN UPDATE PRODUCT: ", err)
    return {
      statusCode: err.statusCode || 500,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify({message: err.message}) || JSON.stringify({message: 'Could not update product.'})
    }
  }
};

async function deleteProduct(event) {
  const id = JSON.parse(event.pathParameters.id);
  try {
    const response = await invokeLambda(`peacock-private-db-${process.env.ENV}-router`, {function: 'remove', table: 'products', id});
    const data = JSON.parse(response.Payload);
    return {
      statusCode: 200,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify(data)
    }
  } catch (err) {
    console.log("ERROR IN DELETE PRODUCT: ", err)
    return {
      statusCode: err.statusCode || 500,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify({message: err.message}) || JSON.stringify({message: 'Could not delete product.'})
    }
  }
};

//////////////
// GROUP
/////////////

async function createGroup(event) {
  const body = JSON.parse(event.body);
  try {
    const response = await invokeLambda(`peacock-private-db-${process.env.ENV}-router`, {function: 'create', table: 'groups', values: body});
    return {
     statusCode: 200,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify({ message: 'Success', data: response.Payload })
    }
  } catch (err) {
    console.log("ERROR IN CREATE GROUP: ", err)
    return {
      statusCode: err.statusCode || 500,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify({message: err.message}) || JSON.stringify({message: 'Could not create the group.'})
    }
  }
};

async function getGroup(event) {
  const body = JSON.parse(event.body);
  try {
    const response = await invokeLambda(`peacock-private-db-${process.env.ENV}-router`, {function: 'getSome', table: 'groups', select:'*', clausend: ` WHERE email='${body.email}'`});
    const data = JSON.parse(response.Payload);
    return {
      statusCode: 200,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify(data)
    }
  } catch (err) {
    console.log("ERROR IN GET GROUP: ", err)
    return {
      statusCode: err.statusCode || 500,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify({message: err.message}) || JSON.stringify({message: 'Could not get the group.'})
    }
  }
};

async function updateGroup(event) {
  const id = JSON.parse(event.pathParameters.id);
  const updates = JSON.parse(event.body);
  try {
    const response = await invokeLambda(`peacock-private-db-${process.env.ENV}-router`, {function: 'update', table: 'groups', id, updates});
    const data = JSON.parse(response.Payload);
    return {
      statusCode: 200,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify(data)
    }
  } catch (err) {
    console.log("ERROR IN UPDATE GROUP: ", err)
    return {
      statusCode: err.statusCode || 500,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify({message: err.message}) || JSON.stringify({message: 'Could not update group.'})
    }
  }
};

async function deleteGroup(event) {
  const id = JSON.parse(event.pathParameters.id);
  try {
    const response = await invokeLambda(`peacock-private-db-${process.env.ENV}-router`, {function: 'remove', table: 'groups', id});
    const data = JSON.parse(response.Payload);
    return {
      statusCode: 200,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify(data)
    }
  } catch (err) {
    console.log("ERROR IN DELETE GROUP: ", err)
    return {
      statusCode: err.statusCode || 500,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify({message: err.message}) || JSON.stringify({message: 'Could not delete group.'})
    }
  }
};

//////////////
// MEMOS
/////////////

async function createMemo(event) {
  const body = JSON.parse(event.body);
  try {
    await invokeLambda(`peacock-private-db-${process.env.ENV}-router`, {function: 'create', table: 'memos', values: body});
    return {
     statusCode: 200,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify({ message: 'Success', data: body })
    }
  } catch (err) {
    console.log("ERROR IN CREATE MEMO: ", err)
    return {
      statusCode: err.statusCode || 500,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify({message: err.message}) || JSON.stringify({message: 'Could not create the memo.'})
    }
  }
};

async function getMemos() {
  try {
    const response = await invokeLambda(`peacock-private-db-${process.env.ENV}-router`, {function: 'getAll', table: 'memos'});
    const data = JSON.parse(response.Payload);
    return {
      statusCode: 200,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify(data)
    }
  } catch (err) {
    console.log("ERROR IN GETALL MEMOS: ", err)
    return {
      statusCode: err.statusCode || 500,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify({message: err.message}) || JSON.stringify({message: 'Could not get the memos.'})
    }
  }
};

async function updateMemo(event) {
  const id = JSON.parse(event.pathParameters.id);
  const updates = JSON.parse(event.body);
  try {
    const response = await invokeLambda(`peacock-private-db-${process.env.ENV}-router`, {function: 'update', table: 'memos', id, updates});
    const data = JSON.parse(response.Payload);
    return {
      statusCode: 200,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify(data)
    }
  } catch (err) {
    console.log("ERROR IN UPDATE MEMO: ", err)
    return {
      statusCode: err.statusCode || 500,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify({message: err.message}) || JSON.stringify({message: 'Could not update memo.'})
    }
  }
};

async function deleteMemo(event) {
  const id = JSON.parse(event.pathParameters.id);
  try {
    const response = await invokeLambda(`peacock-private-db-${process.env.ENV}-router`, {function: 'remove', table: 'memos', id});
    const data = JSON.parse(response.Payload);
    return {
      statusCode: 200,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify(data)
    }
  } catch (err) {
    console.log("ERROR IN DELETE MEMO: ", err)
    return {
      statusCode: err.statusCode || 500,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify({message: err.message}) || JSON.stringify({message: 'Could not delete memo.'})
    }
  }
};

//////////////
// Coupouns
/////////////

async function createCoupon(event) {
  const body = JSON.parse(event.body);
  try {
    await stripe.promotionCodes.create({
      coupon: body.coupon,
      code: body.code
    });
    return {
      statusCode: 200,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify({ message: 'Create Code Success'})
    }
  } catch(err) {
    console.log("ERROR IN CREATE COUPON: ", err)
    return {
      statusCode: err.statusCode || 500,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify({message: err.message}) || JSON.stringify({message: 'Could not create coupon.'})
    }
  }
}

async function getCoupons() {
  try {
    const data = await stripe.promotionCodes.list();
    return {
      statusCode: 200,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify(data)
    }
  } catch(err) {
    console.log("ERROR IN GET COUPONS: ", err)
    return {
      statusCode: err.statusCode || 500,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify({message: err.message}) || JSON.stringify({message: 'Could not get coupons.'})
    }
  }
}

async function updateCoupon(event) {
  const body = JSON.parse(event.body);
  try {
    await stripe.promotionCodes.update(body.id,{active:body.status});
    return {
      statusCode: 200,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify({message: 'Great Success'})
    }
  } catch(err) {
    console.log("ERROR IN UPDATE COUPON: ", err)
    return {
      statusCode: err.statusCode || 500,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify({message: err.message}) || JSON.stringify({message: 'Could not update coupon.'})
    }
  }
}

//////////////
// INVENTORY
/////////////

async function getInventory() {
  try {
    const response = await invokeLambda(`peacock-private-db-${process.env.ENV}-router`, {function: 'getAll', table: 'inventory'});
    const data = JSON.parse(response.Payload);
    return {
      statusCode: 200,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify(data)
    }
  } catch (err) {
    console.log("ERROR IN GETALL INVENTORY: ", err)
    return {
      statusCode: err.statusCode || 500,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify({message: err.message}) || JSON.stringify({message: 'Could not get the inventory.'})
    }
  }
};

async function updateInventory(event) {
  const id = JSON.parse(event.pathParameters.id);
  const updates = JSON.parse(event.body);
  try {
    const response = await invokeLambda(`peacock-private-db-${process.env.ENV}-router`, {function: 'update', table: 'inventory', id, updates});
    const data = JSON.parse(response.Payload);
    return {
      statusCode: 200,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify(data)
    }
  } catch (err) {
    console.log("ERROR IN UPDATE INVENTORY: ", err)
    return {
      statusCode: err.statusCode || 500,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify({message: err.message}) || JSON.stringify({message: 'Could not update inventory.'})
    }
  }
};

//////////////
// STATS
/////////////

async function getStats() {
  try {
    const response = await invokeLambda(`peacock-private-db-${process.env.ENV}-router`, {function: 'getSome', table: 'stats', select:'*', clausend: ' ORDER BY ID DESC LIMIT 1'});
    const data = JSON.parse(response.Payload);
    return {
      statusCode: 200,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify(data)
    }
  } catch (err) {
    console.log("ERROR IN GET STATS: ", err)
    return {
      statusCode: err.statusCode || 500,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify({message: err.message}) || JSON.stringify({message: 'Could not get the stats.'})
    }
  }
};

//////////////
// CONTACT
/////////////

async function contact(event) {
  const body = JSON.parse(event.body);
  try {
    let subject = `Peacock Website Contact Form - ${body.subject}`;
    if (!(body.source && body.subject && body.message)) {
      throw new HTTPError(403, 'Invalid source, message, or subject');
    }
    if (body.source == 'test') {
      subject = `TESTING - ${body.subject}`;
    }
    const emailParams = {
      Source: 'Peacock Sleeves <hello@peacocksleeves.com>',
      Destination: { ToAddresses: ['nick@peacocksleeves.com', 'hello@peacocksleeves.com'] },
      Message: {
        Body: {
          Text: {
            Charset: 'UTF-8',
            Data: body.message
          }
        },
        Subject: {
          Charset: 'UTF-8',
          Data: subject
        }
      }
    };
    await ses.sendEmail(emailParams).promise();
    return {
      statusCode: 200,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify({message: "Email sent successfully."})
    }
  } catch (err) {
    console.log("ERROR IN CONTACT: ", err)
    return {
      statusCode: err.statusCode || 500,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify({message: err.message}) || JSON.stringify({message: 'Could not send email.'})
    }
  }
};

async function weddingInvite(event) {
  const body = JSON.parse(event.body);
  try {
    const emailParams = {
      Source: 'Peacock Sleeves <hello@peacocksleeves.com>',
      Template: 'Wedding_Invite',
      Destination: { ToAddresses: body.emails },
      TemplateData: JSON.stringify({groom: body.owner, passcode: body.passcode})
    };
    await ses.sendTemplatedEmail(emailParams).promise();
    return {
      statusCode: 200,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify({message: "Email sent successfully."})
    }
  } catch (err) {
    console.log("ERROR IN WEDDING INVITE: ", err)
    return {
      statusCode: err.statusCode || 500,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify({message: err.message}) || JSON.stringify({message: 'Could not send email.'})
    }
  }
};

async function groupInvite(event) {
  const body = JSON.parse(event.body);
  try {
    const emailParams = {
      Source: 'Peacock Sleeves <hello@peacocksleeves.com>',
      Template: 'Group_Invite',
      Destination: { ToAddresses: body.emails },
      TemplateData: JSON.stringify({owner: body.owner, passcode: body.passcode})
    };
    await ses.sendTemplatedEmail(emailParams).promise();
    return {
      statusCode: 200,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify({message: "Email sent successfully."})
    }
  } catch (err) {
    console.log("ERROR IN GROUP INVITE: ", err)
    return {
      statusCode: err.statusCode || 500,
      headers: { 'Access-Control-Allow-Origin' : '*' },
      body: JSON.stringify({message: err.message}) || JSON.stringify({message: 'Could not send email.'})
    }
  }
};
