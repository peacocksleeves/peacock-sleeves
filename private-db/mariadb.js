const mysql = require('mysql2/promise');
const pool = mysql.createPool({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0
});

module.exports = async (query, values) => {
  const conn = await pool.getConnection();
  try {
    const [data, fields] = await conn.query(query, values);
    return data;
  } catch(err) {
    console.log('MARIA ERR: ', err)
    throw new Error(err.sqlMessage);
  } finally {
    conn.release();
  }
};