const connectToDatabase = require('./mariadb.js');

module.exports.etl = async () => {
  try {
    const sales = await connectToDatabase('SELECT SUM(sales) AS sales FROM products', null);
    const gross = await connectToDatabase('SELECT SUM(amount) AS gross FROM orders', null);
    const states = await connectToDatabase('SELECT state, COUNT(state) AS count FROM orders GROUP BY state', null);
    const sizeLength = await connectToDatabase(`SELECT JSON_EXTRACT(cart, '$[*].length') AS length, JSON_EXTRACT(cart, '$[*].size') AS size FROM orders`, null);
    let lengths = { short: 0, long: 0, tuxedo: 0};
    let sizes = { SM: 0, MD: 0, LG: 0, XL: 0, XXL: 0};
    sizeLength.forEach(sl => {
      JSON.parse(sl.length).forEach(len => {
        lengths[len] += 1;
      });
      JSON.parse(sl.size).forEach(siz => {
        sizes[siz] += 1;
      });
    });
    const stats = {
      sales: sales[0].sales,
      gross: gross[0].gross,
      states: JSON.stringify(states),
      sizes: JSON.stringify(sizes),
      lengths: JSON.stringify(lengths)
    };
    await connectToDatabase(`INSERT INTO peacock.stats SET ?`, stats);
  } catch (err) {
    console.log("ERROR IN ETL: ", err)
  }
};

// module.exports.etl = async () => {
//   try {}
//   catch(err){}
// };
