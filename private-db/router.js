const connectToDatabase = require('./mariadb.js');

const routerFuncs = {
  healthCheck: healthCheck,
  create: create,
  getOne: getOne,
  getSome: getSome,
  getAll: getAll,
  update: update,
  remove: remove
}

module.exports.router = async (event, context) => {
  try {
    if (event.function in routerFuncs) {
      return await routerFuncs[event.function](event, context);
    }
  } catch (err) {
    console.log("ERROR IN ROUTER: ", err)
    context.fail(err);
  }
};

async function healthCheck() {
  try {
    await connectToDatabase('select 1')
    console.log('Connection successful.')
  } catch (err) {
    console.log("ERROR IN HEALTH: ", err)
    context.fail(err);
  }
}

async function create(event, context) {
  try {
    const data = await connectToDatabase(`INSERT INTO peacock.${event.table} SET ?`, event.values);
    context.succeed(data.insertId);
  } catch (err) {
    console.log("ERROR IN CREATE: ", err)
    context.fail(err);
  }
}

async function getOne(event, context) {
  try {
    const data = await connectToDatabase(`SELECT * FROM peacock.${event.table} WHERE id = ?`, event.id)
    context.succeed(data);
  } catch (err) {
    console.log("ERROR IN GETONE: ", err)
    context.fail(err);
  }
}

async function getSome(event, context) {
  try {
    const data = await connectToDatabase(`SELECT ${event.select} FROM peacock.${event.table}${event.clausend}`, null)
    context.succeed(data);
  } catch (err) {
    console.log("ERROR IN GETONE: ", err)
    context.fail(err);
  }
}

async function getAll(event, context) {
  try {
    const data = await connectToDatabase(`SELECT * FROM peacock.${event.table}`, null)
    context.succeed(data);
  } catch (err) {
    console.log("ERROR IN GETALL: ", err)
    context.fail(err);
  }
}

async function update(event, context) {
  try {
    const values = [];
    const sets = 
      Object.keys(event.updates).map((update, i) => {
        values.push(event.updates[update]);
        return `${update} = ? `;
      }).join();
    const data = await connectToDatabase(`UPDATE peacock.${event.table} SET ${sets}WHERE id = ?`, [...values, event.id])
    context.succeed(data);
  } catch (err) {
    console.log("ERROR IN UPDATE: ", err)
    context.fail(err);
  }
}

async function remove(event, context) {
  try {
    const data = await connectToDatabase(`DELETE FROM peacock.${event.table} WHERE id = ?`, [event.id])
    context.succeed(data);
  } catch (err) {
    console.log("ERROR IN DESTROY: ", err)
    context.fail(err);
  }
}
